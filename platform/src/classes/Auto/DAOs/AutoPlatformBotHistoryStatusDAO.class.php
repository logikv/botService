<?php
/*****************************************************************************
 *   Copyright (C) 2006-2009, onPHP's MetaConfiguration Builder.             *
 *   Generated by onPHP-1.1.master at 2017-03-30 09:37:58                    *
 *   This file is autogenerated - do not edit.                               *
 *****************************************************************************/

	abstract class AutoPlatformBotHistoryStatusDAO extends StorableDAO
	{
		public function getTable()
		{
			return 'bots.history_status';
		}
		
		public function getObjectName()
		{
			return 'PlatformBotHistoryStatus';
		}
		
		public function getSequence()
		{
			return 'bots.history_status_id';
		}
	}
?>