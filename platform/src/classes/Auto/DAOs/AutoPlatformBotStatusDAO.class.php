<?php
/*****************************************************************************
 *   Copyright (C) 2006-2009, onPHP's MetaConfiguration Builder.             *
 *   Generated by onPHP-1.1.master at 2017-03-30 09:37:58                    *
 *   This file is autogenerated - do not edit.                               *
 *****************************************************************************/

	abstract class AutoPlatformBotStatusDAO extends StorableDAO
	{
		public function getTable()
		{
			return 'bots.status';
		}
		
		public function getObjectName()
		{
			return 'PlatformBotStatus';
		}
		
		public function getSequence()
		{
			return 'bots.status_id';
		}
	}
?>