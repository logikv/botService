<?php
/*****************************************************************************
 *   Copyright (C) 2006-2009, onPHP's MetaConfiguration Builder.             *
 *   Generated by onPHP-1.1.master at 2016-10-10 09:56:11                    *
 *   This file is autogenerated - do not edit.                               *
 *****************************************************************************/

	abstract class AutoSocialPublishedVideoDAO extends StorableDAO
	{
		public function getTable()
		{
			return 'social.published_video';
		}
		
		public function getObjectName()
		{
			return 'SocialPublishedVideo';
		}
		
		public function getSequence()
		{
			return 'social.published_video_id';
		}
	}
?>