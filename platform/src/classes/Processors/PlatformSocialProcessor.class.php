<?php

/**
 * Created by PhpStorm.
 * User: root
 * Date: 19.05.16
 * Time: 12:22
 * Class PlatformSocialProcessor
 */
class PlatformSocialProcessor
{
    /**
     * Публикация в группу в facebook
     *
     * @param PlatformSocialPublishedLink $publishedLink
     * @param bool $isConsole
     * @return bool|\Facebook\FacebookResponse
     * @throws ServiceException
     */
    public function groupPublicationLinkFacebook(PlatformSocialPublishedLink $publishedLink, $isConsole = false)
    {
        $group = $publishedLink->getAppAdminGroup();

        $admin = $group->getAppAdmin();
        $app = $admin->getApp();

        $connect = [
            'app_id' => '' . $app->getAppId() . '',
            'app_secret' => '' . $app->getAppSecretKey() . '',
            'default_graph_version' => 'v2.5'
        ];

        $facebook = new \Facebook\Facebook($connect);

        $feed = '/' . $group->getGroupId() . '/feed';

        $option = [
            'link' => '' . $publishedLink->getLinkUrl() . '',
            'group_id' => '' . $publishedLink->getAppAdminGroup()->getGroupId() . ''
        ];

        if (!is_null($publishedLink->getDescription()))
            $option['message'] = $publishedLink->getDescription();


        $accessToken = '' . $admin->getAppAccessToken() . '';

        try {

            $response = $facebook->post($feed, $option, $accessToken);

            (new PlatformSocialPublishedLinkData())
                ->dao()
                ->add(
                    (new PlatformSocialPublishedLinkData())
                        ->setAppAdminGroup($group)
                        ->setSocialNetwork($app->getSocialNetwork())
                        ->setPublishedLink($publishedLink)
                        ->setPostId($response->getGraphPage()->getId())
                );

            $publishedLink->dao()->save(
                $publishedLink
                    ->setPublished(true)
                    ->setPublishedAt(TimestampTZ::makeNow())
                    ->setStatus(LinkStatusEnum::published())
            );

            if ($isConsole)
                return $response;

            return true;

        } catch (Exception $e) {

            $publishedLink->dao()->save(
                $publishedLink
                    ->setPublished(true)
                    ->setPublishedAt(TimestampTZ::makeNow())
                    ->setStatus(LinkStatusEnum::publishedError())
            );

            (new PlatformSocialPublishedLinkData())
                ->dao()
                ->add(
                    (new PlatformSocialPublishedLinkData())
                        ->setAppAdminGroup($group)
                        ->setSocialNetwork($app->getSocialNetwork())
                        ->setPublishedLink($publishedLink)
                        ->setMessage($e->getMessage())
                );

            throw new ServiceException($e->getMessage());
        }
    }

    /**
     * Публикация на официальную страницу в facebook
     *
     * @param PlatformSocialPublishedLink $publishedLink
     * @param bool $isConsole
     * @return bool|\Facebook\FacebookResponse
     * @throws ServiceException
     */
    public function pagePublicationLinkFacebook(PlatformSocialPublishedLink $publishedLink, $isConsole = false)
    {
        $page = $publishedLink->getAppAdminPage();
        $admin = $page->getAppAdmin();
        $app = $admin->getApp();

        if ((int)$app->getSocialNetwork()->getId() == PlatformSocialNameEnum::FACEBOOK) {
            $connect = [
                'app_id' => '' . $app->getAppId() . '',
                'app_secret' => '' . $app->getAppSecretKey() . '',
                'default_graph_version' => 'v2.5'
            ];

            $facebook = new \Facebook\Facebook($connect);

            $feed = '/' . $page->getPageId() . '/feed';

            $option = [
                'link' => '' . $publishedLink->getLinkUrl() . '',
                'place' => '' . $page->getPageId() . ''
            ];

            if (!is_null($publishedLink->getDescription()))
                $option['message'] = $publishedLink->getDescription();

            $accessToken = '' . $page->getAccessToken() . '';
            try {
                $response = $facebook->post($feed, $option, $accessToken);

                (new PlatformSocialPublishedLinkData())
                    ->dao()
                    ->add(
                        (new PlatformSocialPublishedLinkData())
                            ->setAppAdminPages($page)
                            ->setSocialNetwork($app->getSocialNetwork())
                            ->setPublishedLink($publishedLink)
                            ->setPostId($response->getGraphPage()->getId())
                    );

                $publishedLink->dao()->save(
                    $publishedLink
                        ->setPublished(true)
                        ->setPublishedAt(TimestampTZ::makeNow())
                        ->setStatus(LinkStatusEnum::published())
                );

                if ($isConsole)
                    return $response;

                return true;
            } catch (Exception $e) {
                $publishedLink->dao()->save(
                    $publishedLink
                        ->setPublished(true)
                        ->setPublishedAt(TimestampTZ::makeNow())
                        ->setStatus(LinkStatusEnum::publishedError())
                );

                (new PlatformSocialPublishedLinkData())
                    ->dao()
                    ->add(
                        (new PlatformSocialPublishedLinkData())
                            ->setAppAdminPages($page)
                            ->setSocialNetwork($app->getSocialNetwork())
                            ->setPublishedLink($publishedLink)
                            ->setMessage($e->getMessage())
                    );

                throw new ServiceException($e->getMessage());
            }
        }
    }

    /**
     * Публикация в группу однокласников
     *
     * При создании приложение приложение должно включать в себя группу
     *
     * @param PlatformSocialPublishedLink $publishedLink
     * @param bool $isConsole
     * @return mixed
     * @throws ServiceException
     */
    public function groupPublicationLinkOk(PlatformSocialPublishedLink $publishedLink, $isConsole = false)
    {
        $group = $publishedLink->getAppAdminGroup();
        $admin = $group->getAppAdmin();
        $app = $admin->getApp();

        try {
            $accessToken = $admin->getAppAccessToken();
            $privateKey = $app->getAppSecretKey();
            $publicKey = $admin->getName();

            $description = '';

            if (!is_null($publishedLink->getDescription())) {
                $description = '{"type": "text","text": "' . $publishedLink->getDescription() . '"},';
            }

            $params = [
                "application_key" => $publicKey,
                "method" => "mediatopic.post",
                "gid" => "{$group->getGroupId()}",
                "type" => "GROUP_THEME",
                "attachment" =>
                    '{"media": [' . $description . '{"type": "link","url": "' . $publishedLink->getLinkUrl() . '"}]}',
                "format" => "json"
            ];

            $sig = md5(\Odnoklassniki\Executor::preparedRequest($params) . md5("{$accessToken}{$privateKey}"));

            $params["access_token"] = $accessToken;
            $params["sig"] = $sig;

            $result =
                json_decode(
                    (new \Odnoklassniki\Executor())->perform("https://api.ok.ru/fb.do", "POST", $params),
                    true
                );

            $publishedLink->dao()->save(
                $publishedLink
                    ->setPublished(true)
                    ->setPublishedAt(TimestampTZ::makeNow())
                    ->setStatus(LinkStatusEnum::published())
            );

            if ($isConsole)
                return $result;

            if (isset($result['error_code'])) {
                throw new Exception('code:' . $result['error_code'] . ' msg:' . $result['error_msg']);
            }

        } catch (Exception $e) {
            $publishedLink->dao()->save(
                $publishedLink
                    ->setPublished(true)
                    ->setPublishedAt(TimestampTZ::makeNow())
                    ->setStatus(LinkStatusEnum::publishedError())
            );

            (new PlatformSocialPublishedLinkData())
                ->dao()
                ->add(
                    (new PlatformSocialPublishedLinkData())
                        ->setAppAdminGroup($group)
                        ->setSocialNetwork($app->getSocialNetwork())
                        ->setPublishedLink($publishedLink)
                        ->setMessage($e->getMessage())
                );

            throw new ServiceException($e->getMessage());
        }
    }

    /**
     * Публикация в группу в вконтакте
     *
     * @param PlatformSocialPublishedLink $publishedLink
     * @param bool $isConsole
     * @return $this|bool
     * @throws ServiceException
     */
    public function groupPublicationLinkVkontakte(PlatformSocialPublishedLink $publishedLink, $isConsole = false)
    {
        $group = $publishedLink->getAppAdminGroup();
        $admin = $group->getAppAdmin();
        $app = $admin->getApp();

        try {
            $response = (new VkRequest())
                ->setAccessToken($admin->getAppAccessToken())
                ->setGroupId($group->getGroupId())
                ->setAttachments(urldecode($publishedLink->getLinkUrl()));

            if (!is_null($publishedLink->getDescription()))
                $response->setMessage($publishedLink->getDescription());

            if (!is_null($publishedLink->getImgUrl()))
                $response->setImgUrl($publishedLink->getImgUrl());

            $response->execute();

            (new PlatformSocialPublishedLinkData())
                ->dao()
                ->add(
                    (new PlatformSocialPublishedLinkData())
                        ->setAppAdminGroup($group)
                        ->setSocialNetwork($app->getSocialNetwork())
                        ->setPublishedLink($publishedLink)
                        ->setPostId($response->response->getPostId())
                );

            $publishedLink->dao()->save(
                $publishedLink
                    ->setPublished(true)
                    ->setPublishedAt(TimestampTZ::makeNow())
                    ->setStatus(LinkStatusEnum::published())
            );

            if ($isConsole)
                return $response;

            return true;

        } catch (Exception $e) {
            $publishedLink->dao()->save(
                $publishedLink
                    ->setPublished(true)
                    ->setPublishedAt(TimestampTZ::makeNow())
                    ->setStatus(LinkStatusEnum::publishedError())
            );

            (new PlatformSocialPublishedLinkData())
                ->dao()
                ->add(
                    (new PlatformSocialPublishedLinkData())
                        ->setAppAdminGroup($group)
                        ->setSocialNetwork($app->getSocialNetwork())
                        ->setPublishedLink($publishedLink)
                        ->setMessage($e->getMessage())
                );

            throw new ServiceException($e->getMessage());
        }
    }
}