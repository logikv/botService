<?php

/**
 * Created by PhpStorm.
 * User: root
 * Date: 18.01.16
 * Time: 14:35
 */
class  PlatformAddAppAdminErrorEnum extends Enum
{
    const
        ERROR_REQUIRED_NAME = 1,
        ERROR_NAME = 2,
        ERROR_REQUIRED_ACCESS_TOKEN = 3,
        ERROR_ACCESS_TOKEN = 4,
        ERROR_REQUIRED_SOCIAL_ADMIN = 5,
        ERROR_SOCIAL_ADMIN = 6,
        ERROR_PUBLIC_KEY = 7,
        ERROR_REQUIRED_PUBLIC_KEY = 8,
        ERROR_GROUP_ID = 9,
        ERROR_REQUIRED_GROUP_ID = 10,
        ERROR_GROUP_NAME = 11,
        ERROR_REQUIRED_GROUP_NAME = 12;

    protected static $names = [
        self::ERROR_REQUIRED_NAME => 'Имя администратора обязательно для заполнения',
        self::ERROR_NAME => 'Не правильно введено имя администратора',
        self::ERROR_REQUIRED_ACCESS_TOKEN => '`Access token` обязательно для заполнения',
        self::ERROR_ACCESS_TOKEN => 'Не правильно введно поле `Access token`',
        self::ERROR_REQUIRED_SOCIAL_ADMIN => 'ID пользователя (администратора приложения) обязательно для заполнения',
        self::ERROR_SOCIAL_ADMIN => 'ID пользоветеля не правильно',
        self::ERROR_PUBLIC_KEY => 'Не правильно заполненно поле `Публичный ключ`',
        self::ERROR_REQUIRED_PUBLIC_KEY => 'Поле `Публичный ключ` обязательно для заполнения',
        self::ERROR_GROUP_ID => 'Не правильно введено поле `ID группы`',
        self::ERROR_REQUIRED_GROUP_ID => 'Поле `ID группы` обязательно для заполнения',
        self::ERROR_GROUP_NAME => 'Не правильно введено поле `Название группы`',
        self::ERROR_REQUIRED_GROUP_NAME => 'Поле `Название групы` обязательно для заполенения'
    ];

    public static function getErrorRequiredName()
    {
        return new self(self::ERROR_REQUIRED_NAME);
    }

    public static function getErrorName()
    {
        return new self(self::ERROR_NAME);
    }

    public static function getErrorPublicKey()
    {
        return new self(self::ERROR_PUBLIC_KEY);
    }

    public static function getErrorRequiredPublicKey()
    {
        return new self(self::ERROR_REQUIRED_PUBLIC_KEY);
    }

    public static function getErrorRequiredAccessToken()
    {
        return new self(self::ERROR_REQUIRED_ACCESS_TOKEN);
    }

    public static function getErrorAccessToken()
    {
        return new self(self::ERROR_ACCESS_TOKEN);
    }

    public static function getErrorRequiredSocialAdmin()
    {
        return new self(self::ERROR_REQUIRED_SOCIAL_ADMIN);
    }

    public static function getErrorSocialAdmin()
    {
        return new self(self::ERROR_SOCIAL_ADMIN);
    }

    public static function getErrorGroupId()
    {
        return new self(self::ERROR_GROUP_ID);
    }

    public static function getErrorRequiredGroupId()
    {
        return new self(self::ERROR_REQUIRED_GROUP_ID);
    }

    public static function getErrorGroupName()
    {
        return new self(self::ERROR_GROUP_NAME);
    }

    public static function getErrorRequiredGroupName()
    {
        return new self(self::ERROR_REQUIRED_GROUP_NAME);
    }

    public function getError()
    {
        return parent::getName();
    }
}