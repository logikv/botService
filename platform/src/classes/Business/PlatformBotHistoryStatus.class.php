<?php
/*****************************************************************************
 *   Copyright (C) 2006-2009, onPHP's MetaConfiguration Builder.             *
 *   Generated by onPHP-1.1.master at 2017-03-30 09:37:58                    *
 *   This file will never be generated again - feel free to edit.            *
 *****************************************************************************/

	class PlatformBotHistoryStatus extends AutoPlatformBotHistoryStatus implements Prototyped, DAOConnected
	{
		/**
		 * @return PlatformBotHistoryStatus
		**/
		public static function create()
		{
			return new self;
		}
		
		/**
		 * @return PlatformBotHistoryStatusDAO
		**/
		public static function dao()
		{
			return Singleton::getInstance('PlatformBotHistoryStatusDAO');
		}
		
		/**
		 * @return ProtoPlatformBotHistoryStatus
		**/
		public static function proto()
		{
			return Singleton::getInstance('ProtoPlatformBotHistoryStatus');
		}
		
		// your brilliant stuff goes here
	}
?>