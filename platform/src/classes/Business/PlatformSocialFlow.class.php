<?php

/*****************************************************************************
 *   Copyright (C) 2006-2009, onPHP's MetaConfiguration Builder.             *
 *   Generated by onPHP-1.1.master at 2016-01-27 11:18:06                    *
 *   This file will never be generated again - feel free to edit.            *
 *****************************************************************************/
class PlatformSocialFlow extends AutoPlatformSocialFlow implements Prototyped, DAOConnected
{
    /**
     * @return PlatformSocialFlow
     **/
    public static function create()
    {
        return new self;
    }

    /**
     * @return PlatformSocialFlowDAO
     **/
    public static function dao()
    {
        return Singleton::getInstance('PlatformSocialFlowDAO');
    }

    /**
     * @return ProtoPlatformSocialFlow
     **/
    public static function proto()
    {
        return Singleton::getInstance('ProtoPlatformSocialFlow');
    }

    /**
     * @return PlatformSocialFlowGroups[]
     */
    public function getGroups()
    {
        return (new PlatformSocialFlowGroups)
            ->dao()->getByFlow($this);
    }

    /**
     * @return array
     */
    public function getGroupsArray()
    {
        $result = [];
        $i = 0;
        $data = $this->getGroups();

        foreach($data as $flow)
        {
            $result[$i]['id'] = $flow->getGroup()->getId();
            $result[$i]['name'] = $flow->getGroup()->getName();
            $result[$i]['socialNetwork'] = $flow->getGroup()->getAppAdmin()->getApp()->getSocialNetwork()->getName();
            $i = ++$i;
        }
        return $result;
    }

    /**
     * @return array
     */
    public function getGroupsIds()
    {
        $result = [];

        foreach($this->getGroups() as $flow)
            $result[] = $flow->getGroup()->getId();

        return $result;
    }

    /**
     * @return PlatformSocialFlowPages[]
     */
    public function getPages()
    {
        return (new PlatformSocialFlowPages())
            ->dao()->getByFlow($this);
    }

    /**
     * @return array
     */
    public function getPagesArray()
    {
        $result = [];
        $i = 0;
        $data = $this->getPages();

        foreach ($data as $flow)
        {
            $result[$i]['id'] = $flow->getPage()->getId();
            $result[$i]['name'] = $flow->getPage()->getName();
            $result[$i]['socialNetwork'] = $flow->getPage()->getAppAdmin()->getApp()->getSocialNetwork()->getName();
            $i = ++$i;
        }

        return $result;

    }

    public function getPagesIds()
    {
        $result = [];

        foreach ($this->getPages() as $flow)
            $result[] = $flow->getPage()->getId();

        return $result;
    }

    public function updateAccessToken()
    {
        $accessToken = $this->accessToken;

        $this->setAccessToken(md5($accessToken));

        $this->dao()->save($this);
    }

    /**
     * @param $link
     * @return PlatformSocialPublishedLink[]
     */
    public function getDataLinkByLink($link, $materialId = null)
    {
            return (new PlatformSocialPublishedLink())->dao()->getPublishedLinkByLinkAndFlow($link, $this, $materialId);
    }
    // your brilliant stuff goes here
}

?>