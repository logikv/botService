<?php

class Mailer
{
    /** @var  Model */
    private $model;
    /** @var  string */
    private $view;
    /** @var MimeMail */
    private $mimeMail;
    /** @var  string */
    private $subject;
    /** @var  string */
    private $to;

    private $body = null;

    /**
     * Mailer constructor.
     */
    function __construct()
    {
        $this->mimeMail = (new MimeMail());
    }

    /**
     * @param $body
     * @return Mailer
     */
    public function setBody($body)
    {
        $this->body = $body;
        return $this;
    }

    /**
     * @param string $subject
     * @return $this
     */
    public function setSubject($subject)
    {
        $this->subject = $subject;
        return $this;
    }

    /**
     * @param string $to
     * @return $this
     */
    public function setTo($to)
    {
        $this->to = $to;
        return $this;
    }

    /**
     * @return Model
     */
    public function getModel()
    {
        return $this->model;
    }

    /**
     * @param Model $model
     * @return $this
     */
    public function setModel(Model $model)
    {
        $this->model = $model;
        return $this;
    }

    /**
     * @return string
     */
    public function getView()
    {
        return $this->view;
    }

    /**
     * @param string $view
     * @return $this
     */
    public function setView($view)
    {
        $this->view = $view;
        return $this;
    }

    /**
     * send mail
     */
    public function send()
    {
        $this->build();

        (new Mail())
            ->setFrom(
                (new MailAddress())->setAddress('bot@social.newsteam.ru')
                    ->setPerson('BOT')
                    ->toString()
            )
            ->setTo($this->to)
            ->setSubject($this->subject)
            ->setHeaders($this->mimeMail->getHeaders())
            ->setText($this->mimeMail->getEncodedBody())
            ->send();
    }

    /**
     *
     * @throws Exception
     */
    private function build()
    {
        if (is_null($this->body))
            throw new Exception('No body mail');

        $this
            ->mimeMail
            ->setContentType('multipart/alternative')
            ->addPart(
                (new MimePart())
                    ->setBody($this->body)
                    ->setContentType('text/html')
                    ->setCharset("UTF-8")
                    ->setEncoding(MailEncoding::base64())
            );
        $this->mimeMail->build();
    }
}