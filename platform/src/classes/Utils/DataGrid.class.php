<?php

/**
 * Виджет для генерации таблиц.
 *
 * <code>
 *   $grid =
 *      DataGrid::bootstrapTable()
 *      ->addColumn('id')
 *      ->addGroupColumns(array('last_name' => 'LastName', 'middle_name' => 'MiddleName'), 'LabelFio')
 *      ->addColumn('fio', 'LabelFio')
 *      ->addColumn('email')
 *      ->addColumn('first_name', 'FirstName')
 *      ->addColumn('mobile_phone')
 *      ->addColumn('created_at')
 *      ->addCallable('fio', function($row){
 *      if ($row['last_name'] && $row['first_name'] && $row['middle_name'])
 *      {
 *      $fio = $row['last_name'] . ' ' . $row['first_name'] .' ' . $row['middle_name'];
 *      } else {
 *      $fio = '???';
 *      }
 *      return $fio;
 *      })
 *      ->addCallable('id', function($row){
 *      return '<a href="/userdata/'.$row['id'].'">'.$row['id'].'</a>';
 *      })
 *      ->addRows($userList['listData'])
 *
 *      echo $grid;
 * <code>
 *
 * Class DataGrid
 */
class DataGrid implements IWidget {

    /**
     * @var bool
     */
    public $showHeader = true;

    /**
     * @var bool
     */
    public $showFooter = false;

    /**
     * @var bool
     */
    public $showOnEmpty = true;

    /**
     * @var string
     */
    public $emptyCell = '&nbsp;';

    /**
     * @var array
     */
    public $rowOptions = array();

    /**
     * @var array
     */
    protected $columns = array();

    /**
     * @var array
     */
    protected $rows = array();

    /**
     * @var array
     */
    protected $subColumns = array();

    /**
     * @var array
     */
    protected $tableOptions = array();

    /**
     * @var array
     */
    protected $columnHtmlOptions = array();

    /**
     * @var array
     */
    protected $callable = array();

    /**
     * @var int
     */
    protected $countHeaderRows = 1;

    /**
     * @return DataGrid
     */
    public static function plainTable() {
        return new self;
    }

    /**
     * @return DataGrid
     */
    public static function bootstrapTable() {
        $self = new self;
        $self->setTableOption('class', 'table table-striped');
        return $self;
    }

    /**
     * @param $name
     * @param $option
     * @return $this
     */
    public function setTableOption($name, $option) {
        $this->tableOptions[$name] = $option;
        return $this;
    }

    /**
     * @param $name
     * @param $option
     * @return $this
     */
    public function setColumnHtmlOption($name, $option) {
        $this->columnHtmlOptions[$name] = $option;

        return $this;
    }

    /**
     * @return array
     */
    public function getColumnHtmlOptions() {
        return $this->columnHtmlOptions;
    }

    /**
     * @param array $options
     * @return $this
     */
    public function setColumnHtmlOptions(array $options) {
        $this->columnHtmlOptions = $options;

        return $this;
    }

    /**
     * @return array
     */
    public function getTableOptions() {
        return $this->tableOptions;
    }

    /**
     * @param array $options
     * @return $this
     */
    public function setTableOptions(array $options) {
        $this->tableOptions = $options;

        return $this;
    }

    /**
     * @param $name
     * @param callable $function
     * @return $this
     */
    public function addCallable($name, Closure $function) {
        $this->callable[$name] = $function;

        return $this;
    }

    /**
     * @return array
     */
    public function getColumns() {
        return $this->columns;
    }

    /**
     * @param array $columns
     * @return $this
     */
    public function setColumns(array $columns) {
        $this->columns = $columns;

        return $this;
    }

    /**
     * @param $name
     * @param null $label
     * @return $this
     */
    public function addColumn($name, $label = null) {
        if($name instanceof IGridHeaderWidget) {
            $this->columns[$name->getField()] = $name;
        } else {
            $label = is_null($label) ? ucfirst($name) : $label;

            $this->columns[$name] = $label;
        }

        return $this;
    }

    /**
     * @param array $data
     * @return $this
     */
    public function addRows(array $data) {
        foreach ($data as $row) {
            $this->addRow($row);
        }

        return $this;
    }

    /**
     * @param array $data
     * @return $this
     */
    public function addRow(array $data) {
        $rowId = count($this->rows);

        foreach ($data as $fieldId => $value) {
            $this->setField($rowId, $fieldId, $value);
        }

        return $this;
    }

    /**
     * @param $rowId
     * @param $fieldId
     * @param $value
     * @return $this
     */
    public function setField($rowId, $fieldId, $value) {
        $this->rows[$rowId][$fieldId] = $value;

        return $this;
    }

    /**
     * @param array $fields
     * @param $label
     * @return $this
     */
    public function addGroupColumns(array $fields, $label) {
        foreach ($fields as $field => $fieldLabel) {
            $this->columns[$label][$field] = $fieldLabel;
        }

        $this->countHeaderRows = $this->countHeaderRows + 1;

        return $this;
    }

    /**
     * @return string
     */
    public function __toString() {
        return $this->render();
    }

    /**
     * @return string
     */
    public function render() {
        /*
            Тут эксепшен нам совсем не нужен, так-как нам нужно нарисовать хотя бы шапку стаблицы.
            @TODO обязательно рассмотреть это место и принять единое решение.
        */
        //Assert::isNotEmpty($this->rows);
        Assert::isNotEmpty($this->columns);

        $content = array_filter(array(
            $this->showHeader ? $this->renderTableHeader() : false,
            $this->showFooter ? $this->renderTableFooter() : false,
            $this->renderTableBody(),
        ));

        return "<table " . $this->getTableOptionsHtml() . ">\n" . implode("\n", $content) . "\n</table>";
    }

    /**
     * @return string
     */
    protected function renderTableHeader() {
        $content = '';

        $content .= '<tr>';
        foreach ($this->columns as $name => $label) {
            if (is_array($label)) {
                $content .= '<th colspan="' . count($label) . '">' . $name . '</th>';
            }
            else {
                if($label instanceof IGridHeaderWidget) {
                    $label = $label->render();
                }

                $content .= '<th rowspan="' . $this->countHeaderRows . '">' . $label . '</th>';
            }
        }
        $content .= '</tr>';

        $content .= '<tr>';
        foreach ($this->columns as $name => $label) {
            if (is_array($label)) {
                foreach ($label as $k => $v) {
                    $content .= '<th>' . $v . '</th>';
                }
            }
        }
        $content .= '</tr>';

        return "<thead>\n" . $content . "\n</thead>";
    }

    /**
     * @return string
     */
    protected function renderTableFooter() {
        $content = '';

        return "<tfoot>\n" . $content . "\n</tfoot>";
    }

    /**
     * @return string
     */
    protected function renderTableBody() {
        $content = '';

        foreach ($this->rows as $name => $row) {
            $content .= '<tr>';

            foreach ($this->columns as $columnName => $columnValue) {
                if (is_array($columnValue)) {
                    foreach ($columnValue as $name => $column) {
                        $content .= '<td>' . $this->getRowData($name, $row) . '</td>';
                    }
                }
                else {
                    $content .= '<td>' . $this->getRowData($columnName, $row) . '</td>';
                }
            }

            $content .= '</tr>';
        }

        return "<tbody>\n" . $content . "\n</tbody>";
    }

    /**
     * @param $name
     * @param $row
     * @return mixed
     */
    protected function getRowData($name, $row) {
        if (array_key_exists($name, $this->callable)) {
            $value = call_user_func($this->callable[$name], $row);
        }
        elseif (array_key_exists($name, $row)) {
            $value = $row[$name];
        }
        else {
            $value = $name;
        }

        return $value;
    }

    /**
     * @return string
     */
    public function getTableOptionsHtml() {
        $tableOptions = array();
        if (!empty($this->tableOptions)) {
            foreach ($this->tableOptions as $name => $value) {
                $tableOptions[] = $name . '="' . $value . '"';
            }
        }

        return implode(" ", $tableOptions);
    }
}