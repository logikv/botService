<?php
/*****************************************************************************
 *   Copyright (C) 2006-2009, onPHP's MetaConfiguration Builder.             *
 *   Generated by onPHP-1.1.master at 2017-01-17 11:39:20                    *
 *   This file will never be generated again - feel free to edit.            *
 *****************************************************************************/

	class PlatformInviteGroupsDAO extends AutoPlatformInviteGroupsDAO
	{
		// your brilliant stuff goes here
        /**
         * @return string
         */
        public function getSequence()
        {
            return parent::getSequence().'_seq'; // TODO: Change the autogenerated stub
        }

        /**
         * @param $id
         * @param int $expires
         * @return PlatformInviteGroups|mixed
         */
        public function getById($id, $expires = Cache::EXPIRES_MEDIUM)
        {
            return parent::getById($id, $expires); // TODO: Change the autogenerated stub
        }

        public function getRandomGroupByGroup(PlatformBotGroup $botGroup, $limit = 3)
        {
            $criteria = new Criteria($this);

            $criteria
                ->add(Expression::eq('botGroup', $botGroup))
                ->addOrder(OrderBy::create(SQLFunction::create('random')))
                ->setLimit($limit);

            return $criteria->getList();
        }
    }
?>