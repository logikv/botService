<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 02.06.16
 * Time: 11:52
 */
namespace ACL {
    class AccessToResources
    {
        private $resources = [];

        /**
         * @param array $resources
         */
        public function setResources($resources)
        {
            $this->resources[] = $resources;
        }
    }
}