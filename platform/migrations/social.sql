
-- object: social | type: SCHEMA --
DROP SCHEMA social CASCADE ;
CREATE SCHEMA social;
-- ddl-end --

-- object: social.app | type: TABLE --
-- DROP TABLE social.app;
CREATE TABLE social.app(
	id bigserial NOT NULL,
	name varchar(32) NOT NULL,
	social_network smallint NOT NULL,
	app_id bigint NOT NULL,
	app_secret_key text NOT NULL,
	created_at timestamptz,
	created_admin_id bigint,
	picture boolean,
	deleted boolean NOT NULL,
	CONSTRAINT pk__app PRIMARY KEY (id)

);
-- ddl-end --
-- NOTE: below all the code for some key referrer objects are attached
-- as a convinience in order to permit you to test the whole object's
-- SQL definition at once. When exporting or generating the SQL for
-- the whole database model all objects will be placed at their
-- original positions.

-- object: fk_admin_id | type: CONSTRAINT --
-- ALTER TABLE social.app DROP CONSTRAINT fk_admin_id;
ALTER TABLE social.app ADD CONSTRAINT fk_admin_id FOREIGN KEY (created_admin_id)
REFERENCES users.admin (id) MATCH FULL
ON DELETE NO ACTION ON UPDATE NO ACTION;
-- ddl-end --


-- object: social.app_admin | type: TABLE --
-- DROP TABLE social.app_admin;
CREATE TABLE social.app_admin(
	id bigserial,
	app_id smallint,
	app_access_token text,
	name text,
	social_admin_id bigint,
	CONSTRAINT pk_app_admin PRIMARY KEY (id)

);
-- ddl-end --
-- NOTE: below all the code for some key referrer objects are attached
-- as a convinience in order to permit you to test the whole object's
-- SQL definition at once. When exporting or generating the SQL for
-- the whole database model all objects will be placed at their
-- original positions.

-- object: fk__app_id | type: CONSTRAINT --
-- ALTER TABLE social.app_admin DROP CONSTRAINT fk__app_id;
ALTER TABLE social.app_admin ADD CONSTRAINT fk__app_id FOREIGN KEY (app_id)
REFERENCES social.app (id) MATCH FULL
ON DELETE NO ACTION ON UPDATE NO ACTION;
-- ddl-end --


-- object: social.app_admin_group | type: TABLE --
-- DROP TABLE social.app_admin_group;
CREATE TABLE social.app_admin_group(
	id bigserial,
	app_admin_id smallint,
	name varchar(256),
	privacy varchar(128),
	group_id bigint,
	CONSTRAINT pk__app_admin_group PRIMARY KEY (id)

);
-- ddl-end --
-- NOTE: below all the code for some key referrer objects are attached
-- as a convinience in order to permit you to test the whole object's
-- SQL definition at once. When exporting or generating the SQL for
-- the whole database model all objects will be placed at their
-- original positions.

-- object: fk__app_admin | type: CONSTRAINT --
-- ALTER TABLE social.app_admin_group DROP CONSTRAINT fk__app_admin;
ALTER TABLE social.app_admin_group ADD CONSTRAINT fk__app_admin FOREIGN KEY (app_admin_id)
REFERENCES social.app_admin (id) MATCH FULL
ON DELETE NO ACTION ON UPDATE NO ACTION;
-- ddl-end --


-- object: social.app_admin_pages | type: TABLE --
-- DROP TABLE social.app_admin_pages;
CREATE TABLE social.app_admin_pages(
	id bigserial,
	app_admin_id smallint,
	name varchar(256),
	category varchar(256),
	page_id bigint,
	access_token text,
	permissions hstore,
	CONSTRAINT pk__admin_pages PRIMARY KEY (id)

);
-- ddl-end --
-- NOTE: below all the code for some key referrer objects are attached
-- as a convinience in order to permit you to test the whole object's
-- SQL definition at once. When exporting or generating the SQL for
-- the whole database model all objects will be placed at their
-- original positions.

-- object: fk__app_admin | type: CONSTRAINT --
-- ALTER TABLE social.app_admin_pages DROP CONSTRAINT fk__app_admin;
ALTER TABLE social.app_admin_pages ADD CONSTRAINT fk__app_admin FOREIGN KEY (app_admin_id)
REFERENCES social.app_admin (id) MATCH FULL
ON DELETE NO ACTION ON UPDATE NO ACTION;
-- ddl-end --


-- object: social.flow | type: TABLE --
-- DROP TABLE social.flow;
CREATE TABLE social.flow(
	id bigserial,
	name varchar(256),
	access_token varchar(256),
	secret_key varchar(256),
	deleted boolean NOT NULL,
	CONSTRAINT pk__flow PRIMARY KEY (id)

);
-- ddl-end --
-- object: social.flow_dimension_page | type: TABLE --
-- DROP TABLE social.flow_dimension_page;
CREATE TABLE social.flow_dimension_page(
	id bigserial,
	flow_id bigint,
	page_id bigint,
	deleted boolean NOT NULL,
	CONSTRAINT pk__flow_dimension_page PRIMARY KEY (id)

);
-- ddl-end --
-- NOTE: below all the code for some key referrer objects are attached
-- as a convinience in order to permit you to test the whole object's
-- SQL definition at once. When exporting or generating the SQL for
-- the whole database model all objects will be placed at their
-- original positions.

-- object: fk__flow | type: CONSTRAINT --
-- ALTER TABLE social.flow_dimension_page DROP CONSTRAINT fk__flow;
ALTER TABLE social.flow_dimension_page ADD CONSTRAINT fk__flow FOREIGN KEY (flow_id)
REFERENCES social.flow (id) MATCH FULL
ON DELETE NO ACTION ON UPDATE NO ACTION;
-- ddl-end --


-- object: index__page_id | type: INDEX --
-- DROP INDEX social.index__page_id;
CREATE INDEX index__page_id ON social.flow_dimension_page
	USING btree
	(
	  page_id ASC NULLS LAST
	);
-- ddl-end --

-- object: social.flow_dimension_group | type: TABLE --
-- DROP TABLE social.flow_dimension_group;
CREATE TABLE social.flow_dimension_group(
	id bigserial,
	flow_id bigint,
	group_id bigint,
	deleted boolean NOT NULL,
	CONSTRAINT pk__flow_dimension_group PRIMARY KEY (id)

);
-- ddl-end --
-- NOTE: below all the code for some key referrer objects are attached
-- as a convinience in order to permit you to test the whole object's
-- SQL definition at once. When exporting or generating the SQL for
-- the whole database model all objects will be placed at their
-- original positions.

-- object: fk__flow | type: CONSTRAINT --
-- ALTER TABLE social.flow_dimension_group DROP CONSTRAINT fk__flow;
ALTER TABLE social.flow_dimension_group ADD CONSTRAINT fk__flow FOREIGN KEY (flow_id)
REFERENCES social.flow (id) MATCH FULL
ON DELETE NO ACTION ON UPDATE NO ACTION;
-- ddl-end --


-- object: index__group_id | type: INDEX --
-- DROP INDEX social.index__group_id;
CREATE INDEX index__group_id ON social.flow_dimension_group
	USING btree
	(
	  group_id ASC NULLS LAST
	);
-- ddl-end --

-- object: social.published_link | type: TABLE --
-- DROP TABLE social.published_link;
CREATE TABLE social.published_link(

);
-- ddl-end --

-- object: id | type: COLUMN --
ALTER TABLE social.published_link ADD COLUMN id bigserial;
-- ddl-end --

-- object: flow_id | type: COLUMN --
ALTER TABLE social.published_link ADD COLUMN flow_id smallint;
-- ddl-end --

-- object: description | type: COLUMN --
ALTER TABLE social.published_link ADD COLUMN description varchar(256);
-- ddl-end --

-- object: link_url | type: COLUMN --
ALTER TABLE social.published_link ADD COLUMN link_url varchar(512);
-- ddl-end --

-- object: img_url | type: COLUMN --
ALTER TABLE social.published_link ADD COLUMN img_url varchar(512);
-- ddl-end --

-- object: published | type: COLUMN --
ALTER TABLE social.published_link ADD COLUMN published boolean;
-- ddl-end --

-- object: everywhere | type: COLUMN --
ALTER TABLE social.published_link ADD COLUMN everywhere boolean;
-- ddl-end --

-- object: published_at | type: COLUMN --
ALTER TABLE social.published_link ADD COLUMN published_at timestamptz;
-- ddl-end --

-- object: created_at | type: COLUMN --
ALTER TABLE social.published_link ADD COLUMN created_at timestamptz;
-- ddl-end --

-- object: deferred_time | type: COLUMN --
ALTER TABLE social.published_link ADD COLUMN deferred_time timestamptz;
-- ddl-end --

-- object: app_admin_pages_id | type: COLUMN --
ALTER TABLE social.published_link ADD COLUMN app_admin_pages_id bigint;
-- ddl-end --

-- object: app_admin_group_id | type: COLUMN --
ALTER TABLE social.published_link ADD COLUMN app_admin_group_id bigint;
-- ddl-end --

-- object: material_type | type: COLUMN --
ALTER TABLE social.published_link ADD COLUMN material_type smallint;
-- ddl-end --

-- object: material_id | type: COLUMN --
ALTER TABLE social.published_link ADD COLUMN material_id bigint;
-- ddl-end --

-- object: postponed | type: COLUMN --
ALTER TABLE social.published_link ADD COLUMN postponed boolean;
-- ddl-end --

-- object: status | type: COLUMN --
ALTER TABLE social.published_link ADD COLUMN status smallint;
-- ddl-end --
-- object: pk__published_link | type: CONSTRAINT --
-- ALTER TABLE social.published_link DROP CONSTRAINT pk__published_link;
ALTER TABLE social.published_link ADD CONSTRAINT pk__published_link PRIMARY KEY (id);
-- ddl-end --


-- NOTE: below all the code for some key referrer objects are attached
-- as a convinience in order to permit you to test the whole object's
-- SQL definition at once. When exporting or generating the SQL for
-- the whole database model all objects will be placed at their
-- original positions.

-- object: index__link_url | type: INDEX --
-- DROP INDEX social.index__link_url;
CREATE INDEX index__link_url ON social.published_link
	USING btree
	(
	  link_url ASC NULLS LAST
	);
-- ddl-end --

-- object: index__flow_id | type: INDEX --
-- DROP INDEX social.index__flow_id;
CREATE INDEX index__flow_id ON social.published_link
	USING btree
	(
	  flow_id ASC NULLS LAST
	);
-- ddl-end --

-- object: index__app_admin_pages_id | type: INDEX --
-- DROP INDEX social.index__app_admin_pages_id;
CREATE INDEX index__app_admin_pages_id ON social.published_link
	USING btree
	(
	  app_admin_pages_id ASC NULLS LAST
	);
-- ddl-end --

-- object: index__app_admin_group_id | type: INDEX --
-- DROP INDEX social.index__app_admin_group_id;
CREATE INDEX index__app_admin_group_id ON social.published_link
	USING btree
	(
	  app_admin_group_id ASC NULLS LAST
	);
-- ddl-end --

-- object: index__material_type | type: INDEX --
-- DROP INDEX social.index__material_type;
CREATE INDEX index__material_type ON social.published_link
	USING btree
	(
	  material_type ASC NULLS LAST
	);
-- ddl-end --

-- object: index__material_id | type: INDEX --
-- DROP INDEX social.index__material_id;
CREATE INDEX index__material_id ON social.published_link
	USING btree
	(
	  material_id ASC NULLS LAST
	);
-- ddl-end --

-- object: social.published_data | type: TABLE --
-- DROP TABLE social.published_data;
CREATE TABLE social.published_data(
	id bigserial,
	published_link_id bigint,
	social_network smallint,
	app_admin_pages_id bigint,
	app_admin_group_id bigint,
	post_id varchar,
	message varchar,
	CONSTRAINT pk__published_data PRIMARY KEY (id)

);
-- ddl-end --
-- NOTE: below all the code for some key referrer objects are attached
-- as a convinience in order to permit you to test the whole object's
-- SQL definition at once. When exporting or generating the SQL for
-- the whole database model all objects will be placed at their
-- original positions.

-- object: fk__published_link | type: CONSTRAINT --
-- ALTER TABLE social.published_data DROP CONSTRAINT fk__published_link;
ALTER TABLE social.published_data ADD CONSTRAINT fk__published_link FOREIGN KEY (published_link_id)
REFERENCES social.published_link (id) MATCH FULL
ON DELETE NO ACTION ON UPDATE NO ACTION;
-- ddl-end --


