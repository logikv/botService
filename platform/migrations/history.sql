
CREATE TABLE bots.history_invited (
  id            SERIAL,
  user_page_url VARCHAR(512),
  group_id      INTEGER,
  CONSTRAINT pk__history_invited PRIMARY KEY (id)

);

CREATE INDEX index__user_page_url
  ON bots.history_invited
  USING BTREE
  (
    user_page_url ASC NULLS LAST
  );


CREATE TABLE bots.history_visit_page (
  id       SERIAL,
  page_url VARCHAR(512),
  bot_id   INTEGER,
  CONSTRAINT pk__history_visit_page PRIMARY KEY (id)

);

CREATE INDEX index__history_visit_page_page_url
  ON bots.history_visit_page
  USING BTREE
  (
    page_url ASC NULLS LAST
  );


