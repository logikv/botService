-- object: social.published_video | type: TABLE --
-- DROP TABLE social.published_video;
CREATE TABLE social.published_video(
	id serial,
	title varchar(256),
	description varchar(512),
	app_admin_page_id bigint,
	app_admin_group_id bigint,
	published_at timestamptz,
	published_id varchar(128),
	created_at timestamp,
	status smallint,
	path_to_the_file varchar(512),
	CONSTRAINT pk__social_published_video PRIMARY KEY (id)

);
-- ddl-end --

-- Appended SQL commands --
CREATE INDEX index_fti_published_video_title ON social.published_video USING GIN (to_tsvector('russian', title));
CREATE INDEX index_fti_article_published_anons ON social.published_video USING GIN (to_tsvector('russian', description));
-- ddl-end --


-- object: social.published_video_data | type: TABLE --
-- DROP TABLE social.published_video_data;
CREATE TABLE social.published_video_data(
	id serial,
	status smallint,
	published_video_id bigint,
	created_at timestamptz,
	data varchar(512),
	CONSTRAINT pk__published_video PRIMARY KEY (id)

);
-- ddl-end --

-- Appended SQL commands --
CREATE INDEX index__published_video_id ON social.published_video_data USING BTREE (id);
-- ddl-end --


--Appended SQL commands --
CREATE INDEX index_fti_app_admin_group ON social.app_admin_group USING GIN (to_tsvector('russian', name));
CREATE INDEX index_fti_app_admin_pages ON social.app_admin_pages USING GIN (to_tsvector('russian', name));

