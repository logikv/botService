<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 28.03.17
 * Time: 16:16
 */

include('bin_config.inc.php');

$text = file_get_contents('botNames/last_name.txt');
$arrayText = explode(',', $text);

foreach ($arrayText as $k => $value) {
    PlatformBotLastName::dao()
        ->add(
            PlatformBotLastName::create()->setName(trim($value))
        );
}
