<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 28.03.17
 * Time: 16:32
 */

include('bin_config.inc.php');


function setFirstNames($file, $floor)
{
    $text = file_get_contents($file);
    $arrayText = explode(',', $text);

    foreach ($arrayText as $k => $value) {
        PlatformBotFirstName::dao()
            ->add(
                PlatformBotFirstName::create()
                    ->setName(trim($value))
                    ->setFloor($floor)
            );
    }
}

setFirstNames('botNames/first_name_famale.txt', 0);
setFirstNames('botNames/first_name_male.txt', 1);
