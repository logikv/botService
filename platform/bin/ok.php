<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 05.07.16
 * Time: 12:00
 */
include('bin_config.inc.php');

function groupPublicationLinkOk(PlatformSocialPublishedLink $publishedLink, $isConsole = false)
{
    $group = $publishedLink->getAppAdminGroup();
    $admin = $group->getAppAdmin();
    $app = $admin->getApp();

    try {
        $accessToken = $admin->getAppAccessToken();//Наш вечный токен
        $privateKey = $app->getAppSecretKey();//Секретный ключ приложения
        $publicKey = $admin->getName();//Публичный ключ приложения

        $params = array(
            "application_key" => $publicKey,
            "method" => "mediatopic.post",
            "gid" => "54859547082765",//ID нашей группы
            "type" => "GROUP_THEME",
            "attachment" => '{
  "media": [
    {
      "type": "link",
      "url": "' . $publishedLink->getLinkUrl() . '"
    }]
}',//Вместо https://www.google.com естественно надо подставить нашу ссылку
            "format" => "json"
        );

        $sig = md5(\Odnoklassniki\Executor::preparedRequest($params) . md5("{$accessToken}{$privateKey}"));

        $params["access_token"] = $accessToken;
        $params["sig"] = $sig;

        $result =
            json_decode(
                (new \Odnoklassniki\Executor())->perform("https://api.ok.ru/fb.do", "POST", $params),
                true
            );

        print_r($result);

        if (isset($result['error_code']) && $result['error_code'] == 5000) {
            throw new Exception($result['error_code']);
        }


    } catch (Exception $e) {
        $publishedLink->dao()->save(
            $publishedLink
                ->setPublished(true)
                ->setPublishedAt(TimestampTZ::makeNow())
                ->setStatus(LinkStatusEnum::publishedError())
        );

        (new PlatformSocialPublishedLinkData())
            ->dao()
            ->add(
                (new PlatformSocialPublishedLinkData())
                    ->setAppAdminGroup($group)
                    ->setSocialNetwork($app->getSocialNetwork())
                    ->setPublishedLink($publishedLink)
                    ->setMessage($e->getMessage())
            );

        throw new ServiceException($e->getMessage());
    }
}

print_r(PlatformSocialPublishedLink::dao());

groupPublicationLinkOk(PlatformSocialPublishedLink::dao()->getById(16)->setLinkUrl('pravda.ru/'));