<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 13.01.17
 * Time: 13:04
 */
include('bin_config.inc.php');

class AssembleRobots {
    public $fileData;
    private $log;
    function __construct($file)
    {
        $this->log = new TextOutput();
        $this->fileData = file($file);
    }

    /**
     * @return mixed
     */
    public function getFileData()
    {
        return $this->fileData;
    }

    public function assembled()
    {
        foreach ($this->fileData as $key => $value) {
            $bootData = explode('|', $value);

            try {
                PlatformBot::dao()
                    ->add(
                        PlatformBot::create()
                            ->setLogin($bootData[0])
                            ->setPassword($bootData[1])
                            ->setProxy($bootData[2])
                            ->setFloor($bootData[3])
                    );
            }catch (Exception $e)
            {
                $this->log->writeErrLine(
                    'Error: file -'. $e->getFile() .', code - ' .$e->getCode().','.
                    'lime - '.$e->getLine(). ', message -'.$e->getMessage()
                );
            }
            catch (Error $e)
            {
                $this->log->writeErrLine(
                    'Error: file -'. $e->getFile() .', code - ' .$e->getCode().','.
                    'lime - '.$e->getLine(). ', message -'.$e->getMessage()
                );
            }
        }
    }

}

(new AssembleRobots('botFile/bot.txt'))->assembled();