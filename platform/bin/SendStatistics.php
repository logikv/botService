<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 30.01.17
 * Time: 18:48
 */
include('bin_config.inc.php');

$setToMail = 'bot@newsteam.ru';

$timestamp = TimestampTZ::makeNow();
$from = $timestamp->getDateTime()->modify('-1 day')->format('Y-m-d H:i:s');
$to = $timestamp->getDateTime()->modify('+1 day')->format('Y-m-d H:i:s');

$visitSql = <<<SQL
SELECT 
	"bhvp"."page_url" AS "url",
	 count("bhvp"."page_url") AS "visit_count"
FROM 
	"bots"."history_visit_page" AS "bhvp" 
WHERE 
	"bhvp"."visited_at" > TIMESTAMP WITH TIME ZONE '$from' AND
	"bhvp"."visited_at" <  TIMESTAMP WITH TIME ZONE '$to'
GROUP BY "bhvp"."page_url"
ORDER BY "visit_count" DESC;
SQL;

$invitedSql = <<<SQL
SELECT 
	count("bhi"."group_description") AS "invitee",
	"bhi"."group_description" AS "group_title"	
FROM 
	"bots"."history_invited" AS "bhi"
WHERE 
	"bhi"."invited_at" > TIMESTAMP WITH TIME ZONE '$from' AND 
	"bhi"."invited_at" < TIMESTAMP WITH TIME ZONE '$to'
GROUP BY
	"bhi"."group_description"
ORDER BY 
	"invitee" DESC
SQL;

$link = DBPool::me()->getLink();
$resourceVisit = $link->queryRaw($visitSql);
$visitTable = DataGrid::plainTable();
$totalVisitCount = 0;

$visitTable
    ->addColumn('url', 'Сcылка на материал')
    ->addCallable(
        'url',
        function ($row) {
            return '<a href="'.$row['url'].'">'.$row['url'].'</a>';
        }
    )
    ->addColumn('visit_count', 'Кол-во переходов');


while ($row = pg_fetch_assoc($resourceVisit)) {
    $totalVisitCount = $totalVisitCount + $row['visit_count'];
    $visitTable->addRow($row);
}

$bodyVisible = <<<HTML
<p>
Общее кол-во переходов за период <strong> $totalVisitCount </strong> за период от <strong> $from </strong> до <strong> $to</strong> 
</p>
$visitTable
HTML;

$resourceInvite = $link->queryRaw($invitedSql);
$invitedTable = DataGrid::plainTable();
$totalInvitee = 0;

$invitedTable
    ->addColumn('group_title', 'Название группы')
    ->addColumn('invitee', 'Кол-во приглашенных в группу');

while ($row = pg_fetch_assoc($resourceInvite)) {
    $totalInvitee = $totalInvitee + $row['invitee'];
    $invitedTable->addRow($row);
}

//<p>
//Общее кол-во приглашенных <strong> $totalInvitee </strong>  за период от <strong> $from </strong> до <strong> $to</strong>
//</p>
//$invitedTable
//<br/>

$body = <<<HTML
$bodyVisible
HTML;

(new Mailer())
    ->setBody($body)
    ->setTo($setToMail)
    ->setSubject('Статистика')
    ->send();
