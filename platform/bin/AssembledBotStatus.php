<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 30.03.17
 * Time: 12:50
 */
include('bin_config.inc.php');
$text = file_get_contents('botStatus/status.txt');
$textArray = explode("\n\n", $text);
foreach ($textArray as $text)
{
    PlatformBotStatus::dao()->add(
        PlatformBotStatus::create()->setText($text)
    );
}
