<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 19.10.16
 * Time: 18:00
 */

include('bin_config.inc.php');


class PublishedVideo
{

    public function published()
    {
        $videos =
            (new SocialPublishedVideo())->dao()->getByStatus(LinkStatusEnum::excepts(), 10);

        /** @var  SocialPublishedVideo $video */
        foreach ($videos as $video) {
            if (!is_null($video->getAppAdminGroupId())) {
                $this->publishedInGroup($video);
                continue;
            }

            if (!is_null($video->getAppAdminPageId())) {
                $this->publishedInPage($video);
                continue;
            }
        }
    }

    /**
     * @return TextOutput
     */
    public function log()
    {
        return new TextOutput();
    }

    /**
     * @param SocialPublishedVideo $video
     * @return bool
     */
    public function publishedInGroup(SocialPublishedVideo $video)
    {
        if (
            $video->getAppAdminGroup()->getAppAdmin()->getApp()->getSocialNetwork()->getId()
            == PlatformSocialNameEnum::FACEBOOK
        ) {
            $facebook = new \Facebook\Facebook([
                    'app_id' => $video->getAppAdminGroup()->getAppAdmin()->getApp()->getAppId(),
                    'app_secret' => $video->getAppAdminGroup()->getAppAdmin()->getApp()->getAppSecretKey(),
                    'default_graph_version' => 'v2.5'
                ]
            );

            $accessToken = '' . $video->getAppAdminGroup()->getAppAdmin()->getAppAccessToken() . '';

            $data = [
                'title' => $video->getTitle(),
                'description' => $video->getDescription(),
                'source' => $facebook->videoToUpload($video->getPathToTheFile())
            ];

            try {
                $response = $facebook->post(
                    '/' . $video->getAppAdminGroup()->getGroupId() . '/videos',
                    $data, $accessToken
                );
            } catch (Facebook\Exceptions\FacebookResponseException $e) {
                (new SocialPublishedVideo())->dao()->save(
                    $video->setStatusId($status = LinkStatusEnum::PUBLISHING_ERROR)
                );

                $this->addPublishedVideoData($video->getId(), $status, $e->getMessage());
                $this->log()->writeErrLine('Graph returned an error: ' . $e->getMessage());
                return false;
            } catch (Facebook\Exceptions\FacebookSDKException $e) {
                (new SocialPublishedVideo())->dao()->save(
                    $video->setStatusId($status = LinkStatusEnum::PUBLISHING_ERROR)
                );
                $this->addPublishedVideoData(
                    $video->getId(),
                    $status,
                    'Facebook SDK returned an error: ' . $e->getMessage()
                );
                return false;
            }

            (new SocialPublishedVideo())->dao()->save(
                $video->setStatusId($status = LinkStatusEnum::PUBLISHED)
            );

            $node = $response->getGraphNode();

            $this->log()->writeErrLine('publication video id:' . $video->getId());
            $this->log()->writeErrLine('published video id:' . $node['id']);

            $this->addPublishedVideoData($video->getId(), $status, $node['id']);

            return true;
        }
    }

    public function publishedInPage(SocialPublishedVideo $video)
    {
        if ($video->getAppAdminPage()->getAppAdmin()->getApp()->getSocialNetwork()->getId() == PlatformSocialNameEnum::FACEBOOK) {
            $facebook = new \Facebook\Facebook([
                    'app_id' => $video->getAppAdminPage()->getAppAdmin()->getApp()->getAppId(),
                    'app_secret' => $video->getAppAdminPage()->getAppAdmin()->getApp()->getAppSecretKey(),
                    'default_graph_version' => 'v2.5'
                ]
            );

            $accessToken = '' . $video->getAppAdminPage()->getAccessToken() . '';

            $data = [
                'title' => $video->getTitle(),
                'description' => $video->getDescription(),
                'source' => $facebook->videoToUpload($video->getPathToTheFile()),
            ];

            try {
                $response = $facebook->post(
                    '/' . $video->getAppAdminPage()->getPageId() . '/videos',
                    $data, $accessToken
                );

                print_r(  '/' . $video->getAppAdminPage()->getPageId() . '/videos');

            } catch (Facebook\Exceptions\FacebookResponseException $e) {
                (new SocialPublishedVideo())->dao()->save(
                    $video->setStatusId($status = LinkStatusEnum::PUBLISHING_ERROR)
                );

                $this->addPublishedVideoData($video->getId(), $status, $e->getMessage());
                $this->log()->writeErrLine('Graph returned an error: ' . $e->getMessage());
                return false;
            } catch (Facebook\Exceptions\FacebookSDKException $e) {
                (new SocialPublishedVideo())->dao()->save(
                    $video->setStatusId($status = LinkStatusEnum::PUBLISHING_ERROR)
                );
                $this->addPublishedVideoData(
                    $video->getId(),
                    $status,
                    'Facebook SDK returned an error: ' . $e->getMessage()
                );
                return false;
            }

            (new SocialPublishedVideo())->dao()->save(
                $video->setStatusId($status = LinkStatusEnum::PUBLISHED)
            );

            $node = $response->getGraphNode();

            $this->log()->writeErrLine('publication video id:' . $video->getId());
            $this->log()->writeErrLine('published video id:' . $node['id']);

            $this->addPublishedVideoData($video->getId(), $status, $node['id']);

            return true;
        }

    }

    private function addPublishedVideoData($videoId, $status, $data)
    {
        (new SocialPublishedVideoData())->dao()->add(
            (new SocialPublishedVideoData())
                ->setPublishedVideoId($videoId)
                ->setStatusId($status)
                ->setData($data)
        );
    }

}

(new PublishedVideo())->published();