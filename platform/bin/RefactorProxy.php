<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 10.02.17
 * Time: 13:44
 */

include('bin_config.inc.php');

/** @var PlatformBot[] $platformBots */
$platformBots = Criteria::create(PlatformBot::dao())
    ->add(
        PosixExpression::sensitive('proxy', '::')
    )
    ->getList();

foreach ($platformBots as $bot){
    $proxy = $bot->getProxy();
    $proxy = str_replace('::', ':', $proxy);
    PlatformBot::dao()->save($bot->setProxy($proxy));
}
