<?php
/**
 * Created by PhpStorm.
 * User: bva
 * Date: 16.06.2017
 * Time: 15:00
 */


function fwrite_stream($profileNickname, $imgName, $imgAlt)
{
    $name = fopen("/var/www/html/downloads/$profileNickname/$imgName.txt", "w");
    fwrite($name, "$imgAlt");
    fclose($name);
    return true;
}


function returnJSON($stdObject) {
    $jsonBefore = json_encode($stdObject, JSON_UNESCAPED_UNICODE);
    $jsonAfter = preg_replace("/\\\\\//", "/", $jsonBefore);
    header("Content-Type: application/json; charset=utf-8;");
    return $jsonAfter;
}