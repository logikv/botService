<?php
/**
 * Created by PhpStorm.
 * User: bva
 * Date: 31.07.17
 * Time: 18:37
 */

$text = $_GET['text'];
// Create a blank image and add some text
$im = imagecreatetruecolor(120, 20);
$text_color = imagecolorallocate($im, 233, 14, 91);
imagestring($im, 2, 5, 5,  $text, $text_color);

// Set the content type header - in this case image/jpeg

// Output the image
imagejpeg($im);

// Free up memory
imagedestroy($im);
?>