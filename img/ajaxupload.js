/**
 * Created by bva on 02.08.17.
 */
$(document).ready(function () {

    function readImage ( input ) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#preview').attr('src', e.target.result);
            }

            reader.readAsDataURL(input.files[0]);
        }
    }

    function printMessage(destination, msg) {

        $(destination).removeClass();



        if (msg == 'error') {
            $(destination).addClass('alert alert-danger').text('Произошла ошибка при загрузке файла.');
        } else {
            var img = document.createElement('img');
            img.src = '/img'+msg.slice(1);
            document.body.appendChild(img);
        }

    }

    $('#image').change(function(){
        readImage(this);
    });

    var response = '';

    $('#upload-image').on('submit',(function(e) {
        e.preventDefault();

        var formData = new FormData(this);

        $.ajax({
            type:'POST', // Тип запроса
            url: 'handler.php', // Скрипт обработчика
            data: formData, // Данные которые мы передаем
            cache:false, // В запросах POST отключено по умолчанию, но перестрахуемся
            contentType: false, // Тип кодирования данных мы задали в форме, это отключим
            processData: false, // Отключаем, так как передаем файл
            success:function(data){
                printMessage('#result', data);
                response = data;
            },
            error:function(data){
                console.log(data);
            }
        });

    }));

     $('#include-text').on('submit',(function(e) {
         e.preventDefault();

         var formData = new FormData(this);

         $.ajax({
             type:'POST', // Тип запроса
             url: 'createImage.php', // Скрипт обработчика
             data: { text: $(text).value, src: response }, // Данные которые мы передаем
             cache:false, // В запросах POST отключено по умолчанию, но перестрахуемся
             contentType: false, // Тип кодирования данных мы задали в форме, это отключим
             processData: false, // Отключаем, так как передаем файл
             success:function(data){
                 printMessage('#result', data);
                 console.log(data);
             },
             error:function(data){
                 console.log(data);
             }
         });

     }));

});