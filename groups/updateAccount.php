<?php
/**
 * Created by PhpStorm.
 * User: bva
 * Date: 03.07.2017
 * Time: 14:56
 */
require_once '../sql-query/function.qb.php';
require_once '../methods/func.inc.php';


function updateAccount($botId, $group = '', $image = '')
{
    $query = [];

    if (isset($group) && $group !== '') {
        $query['groups_join'] = $group;
    }

    if (isset($image) && $image !== '') {
        $query['images_used'] = $image;
    }

    if ($query)
        $result = qb()->table('accountsData')->where(['id' => $botId])->update($query);

    $result;

    //echo json_encode($query);
}


updateAccount($_GET['botId'], $_GET['group'], $_GET['image']);

