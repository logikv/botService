<?php

/**
 * Created by PhpStorm.
 * User: root
 * Date: 13.01.17
 * Time: 12:19
 */
class BotServiceController extends ProjectBotBaseController
{
    /**
     * Получить не занятого бота
     *
     * @param HttpRequest $httpRequest
     * @return ModelAndView
     */
    public function getFreeBotAction(HttpRequest $httpRequest)
    {
        if ($this->checkAccess($httpRequest) == false)
            return (new ModelAndView())
                ->setModel(
                    (new Model())
                        ->set('success', false)
                        ->set('error', 'Access denied!!!')
                )
                ->setView(new JsonView());

        if (!array_key_exists('sid', $httpRequest->getGet()))
            $sid = PlatformSocialNameEnum::OK;
        else
            $sid = $httpRequest->getGetVar('sid');

        return
            (new ModelAndView())
                ->setModel(
                    (new Model())
                        ->set(
                            'bot', PlatformBot::dao()->getRandom($sid)
                        )
                )
                ->setView(new JsonView());
    }

    /**
     * Занять бота
     *
     * @param HttpRequest $httpRequest
     * @return ModelAndView
     */
    public function takeBotAction(HttpRequest $httpRequest)
    {
        if (
            $this->checkAccess($httpRequest) == false ||
            !array_key_exists('id', $httpRequest->getGet())
        )
            return (new ModelAndView())
                ->setModel(
                    (new Model())
                        ->set('success', false)
                        ->set('error', 'Access denied!!!')
                )
                ->setView(new JsonView());

        PlatformBot::dao()->save(
            PlatformBot::dao()
                ->getById($httpRequest->getGetVar('id'))
                ->setBusy(true)
        );

        return
            (new ModelAndView())
                ->setModel(
                    (new Model())
                        ->set('success', true)
                )
                ->setView(new JsonView());
    }

    /**
     * Освободить бота
     *
     * @param HttpRequest $httpRequest
     * @return ModelAndView
     */
    public function ridBotAction(HttpRequest $httpRequest)
    {
        if (
            $this->checkAccess($httpRequest) == false ||
            !array_key_exists('id', $httpRequest->getGet())
        )
            return (new ModelAndView())
                ->setModel(
                    (new Model())
                        ->set('success', false)
                        ->set('error', 'Access denied!!!')
                )
                ->setView(new JsonView());

        PlatformBot::dao()->save(
            PlatformBot::dao()
                ->getById($httpRequest->getGetVar('id'))
                ->setFreedAt(TimestampTZ::makeNow())
                ->setBusy(false)
        );

        return
            (new ModelAndView())
                ->setModel(
                    (new Model())
                        ->set('success', true)
                )
                ->setView(new JsonView());
    }

    /**
     * Смена пароля у бота
     *
     * @param HttpRequest $httpRequest
     * @return ModelAndView
     */
    public function updateBotAction(HttpRequest $httpRequest)
    {
        $form = new Form();
        $model = new Model();

        $form
            ->add(Primitive::integer('id')->required())
            ->add(Primitive::string('password'))
            ->add(Primitive::string('proxy'))
            ->add(Primitive::integer('sid'))
            ->add(Primitive::integer('uid'))
            ->add(Primitive::string('uAgent'))
            ->add(Primitive::string('email')->setAllowedPattern(PrimitiveString::MAIL_PATTERN))
            ->addMissingLabel('id', 'The field "id" is obligatory for filling!')
            ->addWrongLabel('id', 'The field "count" is not the correct value!')
            ->addMissingLabel('password', 'The field "password" is obligatory for filling!')
            ->addWrongLabel('password', 'The field "password" is not the correct value!')
            ->addMissingLabel('proxy', 'The field "proxy" is obligatory for filling!')
            ->addWrongLabel('proxy', 'The field "proxy" is not the correct value!')
            ->addMissingLabel('sid', 'The field "sid" is obligatory for filling!')
            ->addWrongLabel('sid', 'The field "sid" is not the correct value!')
            ->addMissingLabel('uid', 'The field "uid" is obligatory for filling!')
            ->addWrongLabel('uid', 'The field "uid" is not the correct value!')
            ->addMissingLabel('email', 'The field "email" is obligatory for filling!')
            ->addWrongLabel('email', 'The field "email" is not the correct value!');

        if ($this->checkAccess($httpRequest) == false)
            return (new ModelAndView())
                ->setModel(
                    (new Model())
                        ->set('success', false)
                        ->set('error', 'Access denied!!!')
                )
                ->setView(new JsonView());

        $form->import($httpRequest->getGet());

        if ($form->getErrors()) {
            foreach ($form->getPrimitiveNames() as $name) {
                if (!is_null($error = $form->getTextualErrorFor($name)))
                    $model->set($name, $error);
            }

            return (new ModelAndView())
                ->setModel($model->set('success', false))
                ->setView(new JsonView());
        }

        $bot = PlatformBot::dao()->getById($form->get('id')->getValue());

        if (!is_null($password = $form->get('password')->getValue()))
            $bot->setPassword($password);

        if (!is_null($proxy = $form->get('proxy')->getValue()))
            $bot->setProxy($proxy);

        if (!is_null($sid = $form->get('sid')->getValue()))
            $bot->setProxy($sid);

        if (!is_null($email = $form->get('email')->getValue()))
            $bot->setEmail($email);

        if (!is_null($uid = $form->get('uid')->getValue()))
            $bot->setUserId($uid);

        if(!is_null($uAgent = $form->get('uAgent')->getValue()))
            $bot->setUserAgent($uAgent);

        PlatformBot::dao()->save($bot);

        return
            (new ModelAndView())
                ->setModel(
                    (new Model())
                        ->set('success', true)
                )
                ->setView(new JsonView());
    }

    /**
     * Список выполенных действия бота
     *
     * @param HttpRequest $httpRequest
     * @return ModelAndView
     */
    public function getRecentActivityActions(HttpRequest $httpRequest)
    {
        $form = (new Form());
        $model = (new Model());
        $id = [];

        if (!$this->checkAccess($httpRequest))
            return (new ModelAndView())
                ->setModel(
                    $model
                        ->set('success', false)
                        ->set('error', 'Access denied!!!')
                )
                ->setView(new JsonView());

        $form
            ->add(Primitive::integer('botId')->required())
            ->add(Primitive::integer('count'))
            ->addWrongLabel('count', 'The field "count" is not the correct value!')
            ->addMissingLabel('botId', 'The field "botId" is obligatory for filling!')
            ->addWrongLabel('botId', 'The field "botId" is not the correct value!');

        $form->import($httpRequest->getGet());

        if ($form->getErrors()) {
            foreach ($form->getPrimitiveNames() as $name) {
                if (!is_null($error = $form->getTextualErrorFor($name)))
                    $model->set($name, $error);
            }

            return (new ModelAndView())
                ->setModel($model->set('success', false))
                ->setView(new JsonView());
        }

        try {
            $bot = PlatformBot::dao()->getById($form->get('botId')->getValue());

            array_push($id, $form->get('botId')->getValue());

            if (!$count = $form->get('count')->getValue())
                $count = 2;

            return (new ModelAndView())
                ->setModel(
                    $model
                        ->set('success', true)
                        ->set(
                            'list',
                            PlatformBotActivity::dao()->getListByBot($bot, $count)
                        )
                )
                ->setView(new JsonView());

        } catch (Exception $e) {
            return (new ModelAndView())
                ->setModel(
                    (new Model())
                        ->set('success', false)
                )
                ->setView(
                    new JsonView()
                );
        }
    }

    /**
     * Создать экш бота
     *
     * @param HttpRequest $httpRequest
     * @return ModelAndView
     */
    public function createBotAction(HttpRequest $httpRequest)
    {
        $model = (new Model());
        $form = (new Form());

        if (!$this->checkAccess($httpRequest))
            return (new ModelAndView())
                ->setModel(
                    $model
                        ->set('success', false)
                        ->set('error', 'Access denied!!!')
                )
                ->setView(new JsonView());

        $form
            ->add(
                Primitive::integer('botId')
                    ->required()
            )
            ->add(
                Primitive::integer('activity')
                    ->setMax(32768)
                    ->required()
            )
            ->addMissingLabel('activity', 'The field "activity" is obligatory for filling!')
            ->addWrongLabel('activity', 'The field "activity" is not the correct value!')
            ->addMissingLabel('botId', 'The field "botId" is obligatory for filling!')
            ->addWrongLabel('botId', 'The field "botId" is not the correct value!');

        $form->import($httpRequest->getGet());

        if ($form->getErrors()) {
            foreach ($form->getPrimitiveNames() as $name) {
                if (!is_null($error = $form->getTextualErrorFor($name)))
                    $model->set($name, $error);
            }

            return (new ModelAndView())
                ->setModel($model->set('success', false))
                ->setView(new JsonView());
        }

        try {
            $activity = (new PlatformBotActivity())
                ->dao()
                ->add(
                    (new PlatformBotActivity())
                        ->setBotId($form->get('botId')->getValue())
                        ->setActivity($form->get('activity')->getValue())
                        ->setCreatedAt(TimestampTZ::makeNow())
                );
        } catch (Exception $e) {
            return (new ModelAndView())
                ->setModel((new Model())->set('success', false))
                ->setView(new JsonView());
        }

        return (new ModelAndView())
            ->setModel(
                (new Model())
                    ->set('success', true)
                    ->set('activityId', $activity->getId())
            )
            ->setView(new JsonView());
    }

    /**
     * Остановить действие бота
     *
     * @param HttpRequest $httpRequest
     * @return ModelAndView
     */
    public function stoppedBotAction(HttpRequest $httpRequest)
    {
        $model = new Model();
        $form = new Form();

        if (!$this->checkAccess($httpRequest))
            return (new ModelAndView())
                ->setModel(
                    $model
                        ->set('success', false)
                        ->set('error', 'Access denied!!!')
                )
                ->setView(new JsonView());

        $form
            ->set(Primitive::integer('activityId')->required())
            ->addWrongLabel('activityId', 'The field "activityId" is not the correct value!')
            ->addMissingLabel('activityId', 'The field "activityId" is obligatory for filling!');

        $form->import($httpRequest->getGet());

        if ($form->getErrors()) {
            foreach ($form->getPrimitiveNames() as $name) {
                if (!is_null($error = $form->getTextualErrorFor($name)))
                    $model->set($name, $error);
            }

            return (new ModelAndView())
                ->setModel($model->set('success', false))
                ->setView(new JsonView());
        }

        try {
            $activity = PlatformBotActivity::dao()
                ->getById(
                    $form->get('activityId')->getValue()
                );

            $activity->setStopped(TimestampTZ::makeNow());

            PlatformBotActivity::dao()->save($activity);

            $model->set('success', true);

        } catch (Exception $e) {
            return (new ModelAndView())
                ->setModel((new Model())->set('success', false))
                ->setView(new JsonView());
        }

        return (new ModelAndView())
            ->setModel($model)->setView(new JsonView());

    }

    /**
     * Получение группы по url
     *
     * @param HttpRequest $httpRequest
     * @return ModelAndView
     */
    public function getGroupByUrlAction(HttpRequest $httpRequest)
    {
        $model = new Model();
        $form = new Form();

        if (!$this->checkAccess($httpRequest))
            return (new ModelAndView())
                ->setModel(
                    $model
                        ->set('success', false)
                        ->set('error', 'Access denied!!!')
                )
                ->setView(new JsonView());

        $form
            ->add(Primitive::string('url')->required())
            ->addWrongLabel('url', 'The field "url" is not the correct value!')
            ->addMissingLabel('url', 'The field "url" is obligatory for filling!');

        $form->import($httpRequest->getGet());

        if ($form->getErrors()) {
            foreach ($form->getPrimitiveNames() as $name) {
                if (!is_null($error = $form->getTextualErrorFor($name)))
                    $model->set($name, $error);
            }

            return (new ModelAndView())
                ->setModel($model->set('success', false))
                ->setView(new JsonView());
        }

        $url = explode('ok.ru', $form->get('url')->getValue());

        try {
            $group = PlatformBotGroup::dao()->getByUrl($url[1]);
            $model->set('id', $group->getId());
        } catch (Exception $e) {
            return (new ModelAndView())
                ->setModel((new Model())->set('success', false))
                ->setView(new JsonView());
        }

        return (new ModelAndView())
            ->setModel($model->set('success', true))
            ->setView(new JsonView());

    }

    /**
     * Получение группы для пгриглашения в основную группу бота
     *
     * @param HttpRequest $httpRequest
     * @return ModelAndView
     */
    public function getInviteGroupByBotGroupIdAction(HttpRequest $httpRequest)
    {
        $model = new Model();
        $form = new Form();

        if (!$this->checkAccess($httpRequest))
            return (new ModelAndView())
                ->setModel(
                    $model
                        ->set('success', false)
                        ->set('error', 'Access denied!!!')
                )
                ->setView(new JsonView());

        $form
            ->add(Primitive::integer('groupId')->required())
            ->addWrongLabel('groupId', 'The field "groupId" is not the correct value!')
            ->addMissingLabel('groupId', 'The field "groupId" is obligatory for filling!');

        $form->import($httpRequest->getGet());

        if ($form->getErrors()) {
            foreach ($form->getPrimitiveNames() as $name) {
                if (!is_null($error = $form->getTextualErrorFor($name)))
                    $model->set($name, $error);
            }

            return (new ModelAndView())
                ->setModel($model->set('success', false))
                ->setView(new JsonView());
        }

        try {
            $data = [];

            $groups = PlatformInviteGroups::dao()->getRandomGroupByGroup(
                PlatformBotGroup::dao()->getById($form->get('groupId')->getValue())
            );

            /** @var  PlatformInviteGroups $group */
            foreach ($groups as $group)
                $data[] = $group->getUrl();

            $model->set('groups', $data);

        } catch (ObjectNotFoundException $e) {
            $model->set('group', []);
        } catch (Exception $e) {
            return (new ModelAndView())
                ->setModel((new Model())->set('success', false))
                ->setView(new JsonView());
        }

        return (new ModelAndView())
            ->setModel($model->set('success', true))
            ->setView(new JsonView());
    }

    /**
     * Получение ссылок на группы
     *
     * @param HttpRequest $httpRequest
     * @return ModelAndView
     */
    public function getGroupUrlAction(HttpRequest $httpRequest)
    {
        $model = new Model();

        if (!$this->checkAccess($httpRequest))
            return (new ModelAndView())
                ->setModel(
                    $model
                        ->set('success', false)
                        ->set('error', 'Access denied!!!')
                )
                ->setView(new JsonView());

        $url = PlatformBotGroup::dao()->getRandomList();
        $data = [];

        /** @var PlatformBotGroup $group */
        foreach ($url as $group) {
            $data[] = $group->getUrl();
        };

        $model
            ->set('data', $data);

        return (new ModelAndView())
            ->setModel(
                $model
                    ->set('success', true)
            )
            ->setView(new JsonView());

    }

    /**
     * Получение всех групп
     *
     * @param HttpRequest $httpRequest
     * @return ModelAndView
     */
    public function getAllGroupActions(HttpRequest $httpRequest)
    {
        $model = new Model();

        if (!$this->checkAccess($httpRequest))
            return (new ModelAndView())
                ->setModel(
                    $model
                        ->set('success', false)
                        ->set('error', 'Access denied!!!')
                )
                ->setView(new JsonView());

        try {
            $model
                ->set('success', true)
                ->set('groups', PlatformBotGroup::dao()->getAllGroup());

        } catch (Exception $e) {
            $model->set('success', false);
        }

        return (new ModelAndView())
            ->setModel($model)
            ->setView(new JsonView());
    }

    /**
     * Добавить в историю посещение страницы
     *
     * @param HttpRequest $httpRequest
     * @return ModelAndView
     */
    public function addHistoryVisitPageAction(HttpRequest $httpRequest)
    {
        $model = new Model();
        $form = new Form();

        if (!$this->checkAccess($httpRequest))
            return (new ModelAndView())
                ->setModel(
                    $model
                        ->set('success', false)
                        ->set('error', 'Access denied!!!')
                )
                ->setView(new JsonView());

        $form
            ->add(Primitive::integer('botId')->required())
            ->add(Primitive::string('pageUrl')->required())
            ->addWrongLabel('botId', 'The field "botId" is not the correct value!')
            ->addMissingLabel('botId', 'The field "botId" is obligatory for filling!')
            ->addWrongLabel('pageUrl', 'The field "userPageUrl" is not the correct value!')
            ->addMissingLabel('pageUrl', 'The field "userPageUrl" is obligatory for filling!');

        $form->import($httpRequest->getGet());

        if ($form->getErrors()) {
            foreach ($form->getPrimitiveNames() as $name) {
                if (!is_null($error = $form->getTextualErrorFor($name)))
                    $model->set($name, $error);
            }

            return (new ModelAndView())
                ->setModel($model->set('success', false))
                ->setView(new JsonView());
        }

        try {
            PlatformBotHistoryVisitPage::dao()
                ->add(
                    PlatformBotHistoryVisitPage::create()
                        ->setPageUrl($form->get('pageUrl')->getValue())
                        ->setBotId($form->get('botId')->getValue())
                        ->setVisitedAt(TimestampTZ::makeNow())
                );
            $model->set('success', true);
        } catch (Exception $e) {
            $model->set('success', false);
        }

        return
            (new ModelAndView())
                ->setModel($model)
                ->setView(new JsonView());

    }

    /**
     * Список забанненых ботов
     *
     * @param HttpRequest $httpRequest
     * @return ModelAndView
     */
    public function getBotBanAction(HttpRequest $httpRequest)
    {
        $model = new Model();

        if (!$this->checkAccess($httpRequest))
            return (new ModelAndView())
                ->setModel(
                    $model
                        ->set('success', false)
                        ->set('error', 'Access denied!!!')
                )
                ->setView(new JsonView());

        $list = PlatformBotBan::dao()->getList();

        return (new ModelAndView())
            ->setView(new JsonView())
            ->setModel(
                (new Model())
                    ->set('success', false)
                    ->set('list', $list)
                    ->set('count', count($list))
            );
    }

    /**
     * Забанить бота
     *
     * @param HttpRequest $httpRequest
     * @return ModelAndView
     */
    public function banBotAction(HttpRequest $httpRequest)
    {
        $model = new Model();
        $form = new Form();

        if (!$this->checkAccess($httpRequest))
            return (new ModelAndView())
                ->setModel(
                    $model
                        ->set('success', false)
                        ->set('error', 'Access denied!!!')
                )
                ->setView(new JsonView());

        $form
            ->add(Primitive::integer('botId')->required())
            ->addWrongLabel('botId', 'The field "botId" is not the correct value!')
            ->addMissingLabel('botId', 'The field "botId" is obligatory for filling!');

        $form->import($httpRequest->getGet());

        if ($form->getErrors()) {
            foreach ($form->getPrimitiveNames() as $name) {
                if (!is_null($error = $form->getTextualErrorFor($name)))
                    $model->set($name, $error);
            }

            return (new ModelAndView())
                ->setModel($model->set('success', false))
                ->setView(new JsonView());
        }

        try {
            /** @var PlatformBot $bot */
            $bot = PlatformBot::dao()->getById($form->get('botId')->getValue());

            $botBan = PlatformBotBan::dao()->add(
                PlatformBotBan::create()
                    ->setBusy($bot->getBusy())
                    ->setProxy($bot->getProxy())
                    ->setGender($bot->getGender())
                    ->setFreedAt($bot->getFreedAt())
                    ->setLogin($bot->getLogin())
                    ->setPassword($bot->getPassword())
                    ->setSocialNetwork($bot->getSocialNetwork())
                    ->setEmail($bot->getEmail())
                    ->setUserAgent($bot->getUserAgent())
            );

            PlatformBot::dao()->drop($bot);

            return (new ModelAndView())
                ->setModel(
                    $model
                        ->set('success', false)
                        ->set('botBanId', $botBan->getId())
                )
                ->setView(new JsonView());

        } catch (Exception $e) {
            return (new ModelAndView())
                ->setModel(
                    $model
                        ->set('success', false)
                        ->set('error', $e->getMessage())
                )
                ->setView(new JsonView());
        }
    }

    /**
     * Получение бота по логину
     *
     * @param HttpRequest $httpRequest
     * @return ModelAndView
     */
    public function getBotByLoginAction(HttpRequest $httpRequest)
    {
        $model = new Model();
        $form = new Form();

        if (!$this->checkAccess($httpRequest))
            return (new ModelAndView())
                ->setModel(
                    $model
                        ->set('success', false)
                        ->set('error', 'Access denied!!!')
                )
                ->setView(new JsonView());

        $form
            ->add(Primitive::string('login')->required())
            ->addWrongLabel('login', 'The field "login" is not the correct value!')
            ->addMissingLabel('login', 'The field "login" is obligatory for filling!');

        $form->import($httpRequest->getGet());

        if ($form->getErrors()) {
            foreach ($form->getPrimitiveNames() as $name) {
                if (!is_null($error = $form->getTextualErrorFor($name)))
                    $model->set($name, $error);
            }

            return (new ModelAndView())
                ->setModel($model->set('success', false))
                ->setView(new JsonView());
        };
        try {
            $model->set('data', PlatformBot::dao()->getByLogin($form->get('login')->getValue()));
            $model->set('success', true);
        } catch (Exception $e) {
            $model->set('success', false);
        }

        return (new ModelAndView())
            ->setModel($model)->setView(new JsonView());
    }

    /**
     * Добавить в историю приглашения в группу
     *
     * @param HttpRequest $httpRequest
     * @return ModelAndView
     */
    public function addHistoryInvitedGroupAction(HttpRequest $httpRequest)
    {
        $model = new Model();
        $form = new Form();

        if (!$this->checkAccess($httpRequest))
            return (new ModelAndView())
                ->setModel(
                    $model
                        ->set('success', false)
                        ->set('error', 'Access denied!!!')
                )
                ->setView(new JsonView());

        $form
            ->add(Primitive::string('groupUrl')->required())
            ->add(Primitive::string('userPageUrl')->required())
            ->add(Primitive::string('groupDescription')->required())
            ->addWrongLabel('groupUrl', 'The field "groupUrl" is not the correct value!')
            ->addMissingLabel('groupUrl', 'The field "groupUrl" is obligatory for filling!')
            ->addWrongLabel('userPageUrl', 'The field "userPageUrl" is not the correct value!')
            ->addMissingLabel('userPageUrl', 'The field "userPageUrl" is obligatory for filling!')
            ->addWrongLabel('groupDescription', 'The field "groupDescription" is not the correct value!')
            ->addMissingLabel('groupDescription', 'The field "groupDescription" is obligatory for filling!');

        $form->import($httpRequest->getGet());

        if ($form->getErrors()) {
            foreach ($form->getPrimitiveNames() as $name) {
                if (!is_null($error = $form->getTextualErrorFor($name)))
                    $model->set($name, $error);
            }

            return (new ModelAndView())
                ->setModel($model->set('success', false))
                ->setView(new JsonView());
        }

        try {
            $url = explode('ok.ru', $form->get('groupUrl')->getValue());
            $group = PlatformBotGroup::dao()->getByUrl($url[1]);

            PlatformBotHistoryInvited::dao()
                ->add(
                    PlatformBotHistoryInvited::create()
                        ->setGroupId($group->getId())
                        ->setUserPageUrl($form->get('userPageUrl')->getValue())
                        ->setInvitedAt(TimestampTZ::makeNow())
                        ->setGroupDescription($form->get('groupDescription')->getValue())
                );

            $model->set('success', true);
        } catch (Exception $e) {
            $model->set('success', false);
        }

        return (new ModelAndView())
            ->setModel($model)
            ->setView(new JsonView());
    }

    /**
     * Добавить нового бота
     *
     * @param HttpRequest $httpRequest
     * @return ModelAndView
     */
    public function addBotAction(HttpRequest $httpRequest)
    {
        $model = new Model();
        $form = new Form();

        if (!$this->checkAccess($httpRequest))
            return (new ModelAndView())
                ->setModel(
                    $model
                        ->set('success', false)
                        ->set('error', 'Access denied!!!')
                )
                ->setView(new JsonView());

        $form
            ->add(Primitive::string('login')->required())
            ->add(Primitive::string('password')->required())
            ->add(Primitive::string('proxy')->required())
            ->add(Primitive::string('email'))
            ->add(Primitive::integer('sid')->setDefault(PlatformSocialNameEnum::OK))
            ->add(Primitive::integer('uid'))
            ->add(Primitive::string('uAgent')->required())
            ->add(Primitive::enum('gender')->of('PlatformGender')->required())
            ->addWrongLabel('login', 'The field "login" is not the correct value!')
            ->addMissingLabel('login', 'The field "login" is obligatory for filling!')
            ->addWrongLabel('password', 'The field "password" is not the correct value!')
            ->addMissingLabel('password', 'The field "password" is obligatory for filling!')
            ->addWrongLabel('proxy', 'The field "proxy" is not the correct value!')
            ->addMissingLabel('proxy', 'The field "proxy" is obligatory for filling!')
            ->addWrongLabel('sid', 'The field "sid" is not the correct value!')
            ->addMissingLabel('sid', 'The field "sid" is obligatory for filling!')
            ->addWrongLabel('email', 'The field "email" is not the correct value!')
            ->addMissingLabel('email', 'The field "email" is obligatory for filling!')
            ->addWrongLabel('uid', 'The field "uid" is not the correct value!')
            ->addMissingLabel('uid', 'The field "uid" is obligatory for filling!')
            ->addWrongLabel('gender', 'The field "gender" is not the correct value!')
            ->addMissingLabel('gender', 'The field "gender" is obligatory for filling!')
            ->addWrongLabel('uAgent', 'The field "uAgent" is not the correct value!')
            ->addMissingLabel('uAgent', 'The field "uAgent" is obligatory for filling!')

            ;


        $form->import($httpRequest->getGet());

        if ($form->getErrors()) {
            foreach ($form->getPrimitiveNames() as $name) {
                if (!is_null($error = $form->getTextualErrorFor($name)))
                    $model->set($name, $error);
            }

            return (new ModelAndView())
                ->setModel($model->set('success', false))
                ->setView(new JsonView());
        }


        try {
            PlatformBot::dao()->add(
                PlatformBot::create()
                    ->setLogin($form->get('login')->getValue())
                    ->setPassword($form->get('password')->getValue())
                    ->setProxy($form->get('proxy')->getValue())
                    ->setSocialNetworkId(
                        (is_null($sid = $form->get('sid')->getValue())) ? $form->get('sid')->getDefault() : $sid
                    )
                    ->setEmail($form->get('email')->getValue())
                    ->setUserId($form->get('uid')->getValue())
                    ->setGender($form->get('gender')->getValue())
                    ->setUserAgent($form->get('uAgent')->getRawValue())
            );
            $model->set('success', true);
        } catch (Exception $e) {
            $model->set('success', false);
        }

        return (new ModelAndView())
            ->setModel($model)
            ->setView(new JsonView());

    }

    /**
     * Получение id социальных сетей
     *
     * @param HttpRequest $httpRequest
     * @return ModelAndView
     */
    public function getSocialEnumAction(HttpRequest $httpRequest)
    {
        $model = new Model();
        if (!$this->checkAccess($httpRequest))
            return (new ModelAndView())
                ->setModel(
                    $model
                        ->set('success', false)
                        ->set('error', 'Access denied!!!')
                )
                ->setView(new JsonView());
        try {
            $data = [];
            foreach (PlatformSocialNameEnum::getNameList() as $id => $value)
                $data[$value] = [
                    'id' => $id,
                    'default' => ($id === PlatformSocialNameEnum::OK) ? true : false
                ];
            $model
                ->set('success', true)
                ->set('data', $data);
        } catch (Exception $e) {
            $model->set('success', false);
        }
        return (new ModelAndView())
            ->setModel($model)
            ->setView(new JsonView());
    }

    public function getBotNameAction(HttpRequest $httpRequest)
    {
        $model = new Model();
        $form = new Form();
        if (!$this->checkAccess($httpRequest))
            return (new ModelAndView())
                ->setModel(
                    $model
                        ->set('success', false)
                        ->set('error', 'Access denied!!!')
                )
                ->setView(new JsonView());

        $form
            ->add(Primitive::enum('gender')->of('PlatformGender')->setDefault(PlatformGender::female()))
            ->addWrongLabel('gender', 'The field "gender" is not the correct value!')
            ->addMissingLabel('gender', 'The field "gender" is obligatory for filling!');

        $form->import($httpRequest->getGet());

        if ($form->getErrors()) {
            foreach ($form->getPrimitiveNames() as $name) {
                if (!is_null($error = $form->getTextualErrorFor($name)))
                    $model->set($name, $error);
            }

            return (new ModelAndView())
                ->setModel($model->set('success', false))
                ->setView(new JsonView());
        }

        if (is_null($form->get('gender')->getValue()))
            $gender = $form->get('gender')->getDefault();
        else
            $gender = $form->get('gender')->getValue();

        $platformFirstName = PlatformBotFirstName::dao()->getRandom($gender);
        $platformLastName = PlatformBotLastName::dao()->getRandom();

        return (new ModelAndView())
            ->setModel(
                $model
                    ->set('success', true)
                    ->set('lastName', ($gender == 1) ? $platformLastName->getName() : $platformLastName->getNameFamale())
                    ->set('firstName', $platformFirstName->getName())
            )
            ->setView(
                new JsonView()
            );
    }

    /**
     * Добавить в историю использование статуса пользователем
     *
     * @param HttpRequest $httpRequest
     * @return ModelAndView
     */
    public function addHistoryStatusAction(HttpRequest $httpRequest)
    {
        $model = new Model();
        $form = new Form();

        if (!$this->checkAccess($httpRequest))
            return (new ModelAndView())
                ->setModel(
                    $model
                        ->set('success', false)
                        ->set('error', 'Access denied!!!')
                )
                ->setView(new JsonView());

        $form
            ->add(Primitive::integer('statusId')->required())
            ->add(Primitive::integer('botId')->required())
            ->addWrongLabel('statusId', 'The field "statusId" is not the correct value!')
            ->addMissingLabel('statusId', 'The field "statusId" is obligatory for filling!')
            ->addWrongLabel('botId', 'The field "botId" is not the correct value!')
            ->addMissingLabel('botId', 'The field "botId" is obligatory for filling!');

        $form->import($httpRequest->getGet());

        if ($form->getErrors()) {
            foreach ($form->getPrimitiveNames() as $name) {
                if (!is_null($error = $form->getTextualErrorFor($name)))
                    $model->set($name, $error);
            }

            return (new ModelAndView())
                ->setModel($model->set('success', false))
                ->setView(new JsonView());
        }

        try {
            PlatformBotHistoryStatus::dao()->add(
                PlatformBotHistoryStatus::create()
                    ->setBotId($form->get('botId')->getValue())
                    ->setStatusId($form->get('statusId')->getValue())
                    ->setCreatedAt(TimestampTZ::makeNow())
            );
            $model->set('success', true);
        } catch (Exception $exception) {
            $model->set('success', false);
        }

        return (new ModelAndView())
            ->setModel($model)
            ->setView(new JsonView());
    }

    /**
     * Проверка можно ли пользователю добавить статус
     *
     * @param HttpRequest $httpRequest
     * @return ModelAndView
     */
    public function isAvailableStatusAction(HttpRequest $httpRequest)
    {
        $model = new Model();
        $form = new Form();

        if (!$this->checkAccess($httpRequest))
            return (new ModelAndView())
                ->setModel(
                    $model
                        ->set('success', false)
                        ->set('error', 'Access denied!!!')
                )
                ->setView(new JsonView());

        $form
            ->add(Primitive::integer('botId')->required())
            ->addWrongLabel('botId', 'The field "botId" is not the correct value!')
            ->addMissingLabel('botId', 'The field "botId" is obligatory for filling!');

        $form->import($httpRequest->getGet());

        if ($form->getErrors()) {
            foreach ($form->getPrimitiveNames() as $name) {
                if (!is_null($error = $form->getTextualErrorFor($name)))
                    $model->set($name, $error);
            }

            return (new ModelAndView())
                ->setModel($model->set('success', false))
                ->setView(new JsonView());
        }

        if (
        is_null(
            PlatformBotHistoryStatus::dao()
                ->getMore(
                    TimestampTZ::makeNow()->toTimestamp()->modify('-1 day'),
                    $form->get('botId')->getValue()
                )
        )
        )
            $result = true;
        else
            $result = false;

        $model->set('success', true)->set('result', $result);

        return (new ModelAndView())
            ->setModel($model)
            ->setView(new JsonView());
    }

    /**
     * Получение статуса
     *
     * @param HttpRequest $httpRequest
     * @return ModelAndView
     */
    public function getStatusAction(HttpRequest $httpRequest)
    {

        $model = new Model();
        if (!$this->checkAccess($httpRequest))
            return (new ModelAndView())
                ->setModel(
                    $model
                        ->set('success', false)
                        ->set('error', 'Access denied!!!')
                )
                ->setView(new JsonView());

        $status = PlatformBotStatus::dao()->getRandom();

        if (!is_null($status))
            $model->set('data', ['text' => $status->getText(), 'status_id' => $status->getId()]);
        else
            $model
                ->set('data', false)
                ->set('error', 'No data')
                ->set('success', false);

        return (new ModelAndView())
            ->setModel($model)
            ->setView(new JsonView());
    }

    public function getCrawlTimeLinkAction(HttpRequest $httpRequest)
    {
        $model = new Model();
        $form = new Form();
        if (!$this->checkAccess($httpRequest))
            return (new ModelAndView())
                ->setModel(
                    $model
                        ->set('success', false)
                        ->set('error', 'Access denied!!!')
                )
                ->setView(new JsonView());
        $form
            ->add(Primitive::integer('botId')->required())
            ->addWrongLabel('botId', 'The field "botId" is not the correct value!')
            ->addMissingLabel('botId', 'The field "botId" is obligatory for filling!');
        $form->import($httpRequest->getGet());
        if ($form->getErrors()) {
            foreach ($form->getPrimitiveNames() as $name) {
                if (!is_null($error = $form->getTextualErrorFor($name)))
                    $model->set($name, $error);
            }
            return (new ModelAndView())
                ->setModel($model->set('success', false))
                ->setView(new JsonView());
        }
        try {
            $bot = PlatformBot::dao()->getById($form->get('botId')->getValue());
            $time = TimestampTZ::makeNow()->toFormatString('Y-m-d H:00');
            /** @var TimestampTZ $timeBegin */
            $timeBegin = TimestampTZ::create($time);
            $links = [];
            /** @var TimestampTZ $timeTo */
            $timeTo = TimestampTZ::create($time)->modify('+1 hours');
            $crawlTimes = PlatformCrawlTime::dao()
                ->getBySocialNetworkAndTimes(
                    $bot->getSocialNetwork(),
                    $timeBegin,
                    $timeTo
                );
            foreach ($crawlTimes as $crawlTime) {
                $visit = PlatformVisitedPageCrawlTime::dao()->getByCrawlAndBot($crawlTime, $bot);
                if (!is_null($visit))
                    continue;
                if ($crawlTime->getStatus()->getId() == PlatformCrawlTimeStatus::EXPECTS)
                    $crawlTime->dao()->save($crawlTime->setStatus(PlatformCrawlTimeStatus::performed()));
                $visitedPage = (new PlatformVisitedPageCrawlTime())
                    ->dao()
                    ->add(
                        (new PlatformVisitedPageCrawlTime())
                            ->setBot($bot)
                            ->setCrawTime($crawlTime)
                            ->setStatus(PlatformVisitedPageCrawlTimeStatus::notDone())
                    );
                $links[] = [
                    'id' => $visitedPage->getId(),
                    'social_group_url' => $crawlTime->getGroupAndPage()->getSocialLink(),
                    'material_link' => $crawlTime->getReferrersLink()->getUrlLink(),
                    'title' => $crawlTime->getReferrersLink()->getTitle()
                ];
            }
            $model
                ->set('success', true)
                ->set('data', $links)
                ->set('count', count($links));
        } catch (Exception $exception) {
            $model
                ->set('success', 'false')
                ->set('error', $exception->getMessage());
        }
        return (new ModelAndView())
            ->setModel($model)
            ->setView(new JsonView());
    }

    public function getBotByProxyAction(HttpRequest $httpRequest)
    {
        $model = new Model();
        $form = new Form();
        $form
            ->add(Primitive::string('proxy')->required())
            ->addWrongLabel('proxy', 'The field "proxy" is not the correct value!')
            ->addMissingLabel('proxy', 'The field "proxy" is obligatory for filling!');
        $form->import($httpRequest->getGet());
        if (!$this->checkAccess($httpRequest))
            return (new ModelAndView())
                ->setModel(
                    $model
                        ->set('success', false)
                        ->set('error', 'Access denied!!!')
                )
                ->setView(new JsonView());
        if ($form->getErrors()) {
            foreach ($form->getPrimitiveNames() as $name) {
                if (!is_null($error = $form->getTextualErrorFor($name)))
                    $model->set($name, $error);
            }
            return (new ModelAndView())
                ->setModel($model->set('success', false))
                ->setView(new JsonView());
        }
        $bots = (new PlatformBot())->dao()->getByProxy($form->get('proxy')->getValue());
        $botsIDs = [];
        foreach ($bots as $bot)
            $botsIDs[] = $bot->getId();
        $model->set('botIds', $botsIDs);
        $model->set('count', count($botsIDs));
        return (new ModelAndView())
            ->setModel($model)
            ->setView(new JsonView());
    }

    public function getBotByIdAction(HttpRequest $httpRequest)
    {
        $model = new Model();
        $form = new Form();
        $form
            ->add(Primitive::integer('id')->required())
            ->addWrongLabel('id', 'The field "id" is not the correct value!')
            ->addMissingLabel('id', 'The field "id" is obligatory for filling!');
        $form->import($httpRequest->getGet());
        if (!$this->checkAccess($httpRequest))
            return (new ModelAndView())
                ->setModel(
                    $model
                        ->set('success', false)
                        ->set('error', 'Access denied!!!')
                )
                ->setView(new JsonView());
        if ($form->getErrors()) {
            foreach ($form->getPrimitiveNames() as $name) {
                if (!is_null($error = $form->getTextualErrorFor($name)))
                    $model->set($name, $error);
            }
            return (new ModelAndView())
                ->setModel($model->set('success', false))
                ->setView(new JsonView());
        }

        try {
            $data = PlatformBot::dao()->getData($form->get('id')->getValue());
            $model->set('success', true)->set('data', $data);
            return (new ModelAndView())
                ->setModel($model)
                ->setView(new JsonView());
        } catch (Exception $exception) {
            $model->set('success', false)->set('error', $exception->getMessage());
            return (new ModelAndView())
                ->setModel($model)->setView(new JsonView());
        }

    }

    public function takeCrawlTimeLinkAction(HttpRequest $httpRequest)
    {
        $model = new Model();
        $form = new Form();
        if (!$this->checkAccess($httpRequest))
            return (new ModelAndView())
                ->setModel(
                    $model
                        ->set('success', false)
                        ->set('error', 'Access denied!!!')
                )
                ->setView(new JsonView());
        $form
            ->set(Primitive::integer('visitedId')->required())
            ->addWrongLabel('visitedId', 'The field "visitedId" is not the correct value!')
            ->addMissingLabel('visitedId', 'The field "visitedId" is obligatory for filling!');
        $form->import($httpRequest->getGet());
        if ($form->getErrors()) {
            foreach ($form->getPrimitiveNames() as $name) {
                if (!is_null($error = $form->getTextualErrorFor($name)))
                    $model->set($name, $error);
            }
            return (new ModelAndView())
                ->setModel($model->set('success', false))
                ->setView(new JsonView());
        }
        try {
            $visitedPage = (new PlatformVisitedPageCrawlTime())->dao()->getById($form->get('visitedId')->getValue());
            $visitedPage
                ->dao()->save(
                    $visitedPage
                        ->setStatus(PlatformVisitedPageCrawlTimeStatus::done())
                );
            $model->set('success', true);
            return (new ModelAndView())
                ->setModel($model)
                ->setView(new JsonView());
        } catch (Exception $e) {
            $model
                ->set('success', false)
                ->set('error', $e->getMessage());
        }
        return (new ModelAndView())
            ->setModel($model)
            ->setView(new JsonView());

    }

    /**
     * Освободить всех ботов
     *
     * @param HttpRequest $httpRequest
     * @return ModelAndView
     */
    public function resetAllBotsAction(HttpRequest $httpRequest)
    {
        try {
            $model = new Model();
            if (!$this->checkAccess($httpRequest))
                return (new ModelAndView())
                    ->setModel(
                        $model
                            ->set('success', false)
                            ->set('error', 'Access denied!!!')
                    )
                    ->setView(new JsonView());

            $bots = (new PlatformBot())->dao()->getByBusy();

            foreach ($bots as $bot)
                (new PlatformBot())->dao()->save($bot->setBusy(false));

            $model->set('success', true);
        } catch (ObjectNotFoundException $e) {
            $model->set('success', true);
        } catch (Exception $e) {
            $model->set('success', false);
        }

        return (new ModelAndView())->setModel($model)->setView(new JsonView());
    }

    /**
     * Кол-во активных ботов
     *
     * @param HttpRequest $httpRequest
     * @return ModelAndView
     */
    public function getCountActivityBotAction(HttpRequest $httpRequest)
    {
        try {
            $model = new Model();
            if (!$this->checkAccess($httpRequest))
                return (new ModelAndView())
                    ->setModel(
                        $model
                            ->set('success', false)
                            ->set('error', 'Access denied!!!')
                    )
                    ->setView(new JsonView());
            $bots = (new PlatformBot())->dao()->getByBusy();
            $model
                ->set('success', true)
                ->set('count', count($bots));
        } catch (ObjectNotFoundException $e) {
            $model->set('success', false);
        }
        return
            (new ModelAndView())
                ->setModel($model)
                ->setView(new JsonView());
    }


    public function getCountAction(HttpRequest $httpRequest)
    {
        $model = new Model();
        try {
            if (!$this->checkAccess($httpRequest))
                return (new ModelAndView())
                    ->setModel(
                        $model
                            ->set('success', false)
                            ->set('error', 'Access denied!!!')
                    )
                    ->setView(new JsonView());
            $bots = (new PlatformBot())->dao()->getCountRowBot();
            $model->set('success', true)->set('data', $bots);
        }catch (Exception $e)
        {
            $model->set('success', false);
        }

        return (new ModelAndView())
            ->setModel($model)->setView(new JsonView());
    }

    /**
     * Мапинг методов конроллера
     * Можно вернуть пустой массив если брать с учетом
     * что экшен будет тот который прописан в роут конфиге
     *
     * @return array
     */
    protected function getMapping()
    {
        return [
            'getFreeBot' => 'getFreeBotAction',
            'takeBot' => 'takeBotAction',
            'ridBot' => 'ridBotAction',
            'updateBot' => 'updateBotAction',
            'getBotRecentActivity' => 'getRecentActivityActions',
            'createBotAction' => 'createBotAction',
            'stoppedBotAction' => 'stoppedBotAction',
            'getGroupByUrl' => 'getGroupByUrlAction',
            'getInviteGroupByBotGroupId' => 'getInviteGroupByBotGroupIdAction',
            'getGroupUrl' => 'getGroupUrlAction',
            'getAllGroup' => 'getAllGroupActions',
            'addHistoryVisitPage' => 'addHistoryVisitPageAction',
            'addHistoryInvitedGroup' => 'addHistoryInvitedGroupAction',
            'banBot' => 'banBotAction',
            'getBotBan' => 'getBotBanAction',
            'getBotByLogin' => 'getBotByLoginAction',
            'addBot' => 'addBotAction',
            'getSocialEnum' => 'getSocialEnumAction',
            'getBotName' => 'getBotNameAction',
            'isAvailableStatus' => 'isAvailableStatusAction',
            'addHistoryStatus' => 'addHistoryStatusAction',
            'getStatus' => 'getStatusAction',
            'getCrawlTimeLink' => 'getCrawlTimeLinkAction',
            'takeCrawlTimeLink' => 'takeCrawlTimeLinkAction',
            'getBotByProxy' => 'getBotByProxyAction',
            'getBotById' => 'getBotByIdAction',
            'resetAllBots' => 'resetAllBotsAction',
            'getCountActivityBot' => 'getCountActivityBotAction',
            'getCount' => 'getCountAction'
        ];
    }
}