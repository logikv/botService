<?php

/**
 * Created by PhpStorm.
 * User: root
 * Date: 12.04.17
 * Time: 10:40
 */
class ManagingTheTransitionController extends ProjectAuthMappedController
{
    private $imageSrc = false;
    private $title = false;
    private $description = false;

    /**
     * @return ModelAndView
     */
    public function indexAction()
    {
        $data = [];
        $key = 0;
        $list = [];
        /** @var PlatformSocialNameEnum[] $socialList */
        $socialList = PlatformSocialNameEnum::getList();
        foreach ($socialList as $item) {
            $data[$key]['value'] = $item->getId();
            $data[$key]['name'] = $item->getName();
            $data[$key]['data'] = (new PlatformGroupAndPage())->dao()->getBySocialNameEnum($item);
            $key = ++$key;
        }
        try {
            $links = (new PlatformReferrersLink())->dao()->getList(10);
        } catch (Exception $exception) {
            $links = [];
        }
        foreach ($links as $link) {
            $times = PlatformCrawlTime::dao()->getTimeListByLinkId($link['id']);
            $link['times'] = $times;
            $list[] = $link;
        }

        return (new ModelAndView())
            ->setModel(
                (new Model())
                    ->set('pages', $data)
                    ->set('linksCount', (count($list) === 0) ? 0 : $list[0]['total_count'])
                    ->set('links', $list)
            )
            ->setView('managing/transition');
    }

    public function loadDataAction(HttpRequest $httpRequest)
    {
        $socialNetwork = null;
        $groupPage = null;
        $model = new Model();
        $list = [];
        if ($this->isPostVar($httpRequest, 'social_network_id'))
            $socialNetwork = new PlatformSocialNameEnum($httpRequest->getPostVar('social_network_id'));
        if ($this->isPostVar($httpRequest, 'group_page_id'))
            try {
                Assert::isInteger($httpRequest->getPostVar('group_page_id'));
                $groupPage = (new PlatformGroupAndPage())->dao()
                    ->getById($httpRequest->getPostVar('group_page_id'));
            } catch (WrongArgumentException $e) {
                /**  */
            }
        try {
            $links = (new PlatformReferrersLink())->dao()->getList(10, $groupPage, $socialNetwork);
        } catch (Exception $e) {
            $links = [];
        }
        foreach ($links as $link) {
            $times = PlatformCrawlTime::dao()->getTimeListByLinkId($link['id']);
            $link['times'] = $times;
            $list[] = $link;
        }
        $model
            ->set('linksCount', (count($list) === 0) ? 0 : $list[0]['total_count'])
            ->set('links', $list);
        $model->set('success', true);
        return (new ModelAndView())
            ->setModel($model)
            ->setView(new JsonView());
    }

    public function addTimeAction(HttpRequest $httpRequest)
    {
        $model = new Model();
        $form = new Form();
        $form
            ->add(Primitive::integer('link_id')->required())
            ->add(Primitive::integer('group_page_id')->required())
            ->add(Primitive::timestampTZ('start_date')->required());
        $form->import($httpRequest->getPost());
        try {
            (new PlatformCrawlTime())->dao()->add(
                (new PlatformCrawlTime())
                    ->setReferrersLinkId($form->get('link_id')->getValue())
                    ->setGroupAndPageId($form->get('group_page_id')->getValue())
                    ->setBeginAt($form->get('start_date')->getValue())
                    ->setStatus(PlatformCrawlTimeStatus::expects())
            );
            $model
                ->set('success', true)
                ->set('times', PlatformCrawlTime::dao()->getTimeListByLinkId($form->get('link_id')->getValue()));
        } catch (Exception $e) {
            $model->set('success', false);
        }
        return (new ModelAndView())
            ->setModel($model)
            ->setView(new JsonView());
    }

    public function getFreeTimeAction(HttpRequest $httpRequest)
    {
        $form = (new Form())
            ->add(Primitive::string('link')->setAllowedPattern(PrimitiveString::URL_PATTERN)->required())
            ->add(Primitive::timestampTZ('date')->required());
        $form->import($httpRequest->getPost());
        /** @var PlatformReferrersLink $link */
        $link = (new PlatformReferrersLink())->dao()->getByLink($form->get('link')->getValue());
        /** @var TimestampTZ $date */
        $date = $form->get('date')->getValue();
        /** @var  TimestampTZ $dataTo */
        $dataTo = clone $form->get('date')->getValue();
        $dataTo->modify('+1 day');
        $times = [];
        $model = new Model();
        for ($i = 0; $i < 24; $i++)
            if ($i < 10) $times[] = "0$i:00"; else $times[] = "$i:00";
        if (!is_null($link)) {
            if (Timestamp::compare($now = Timestamp::makeNow(), $date) == 1) {
                $copyTimes = $times;
                $key = array_search($now->toFormatString('H:00'), $copyTimes);
                $times = array_slice($copyTimes, ++$key);
            }
            $crawlTimes = (new PlatformCrawlTime())->dao()->getTimesByLink($link, $date, $dataTo);
            foreach ($crawlTimes as $crawlTime) {
                if ($key = array_search($crawlTime['time'], $times))
                    unset($times[$key]);
            }
            $model
                ->set('times', (new PlatformCrawlTime())->dao()->getTimeListByLinkId($link->getId()))
                ->set('allowTimes', array_values($times))
                ->set('link_id', $link->getId());
        } else {
            $model
                ->set('link_id', false)
                ->set('allowTimes', $times)
                ->set('times', []);
        }
        return (new ModelAndView())
            ->setModel($model)
            ->setView(new JsonView());
    }

    public function loadMoreAction(HttpRequest $httpRequest)
    {
        $model = new Model();
        $socialNetwork = null;
        $groupPage = null;
        $offset = 0;
        $list = [];
        if ($this->isPostVar($httpRequest, 'social_network_id'))
            try {
                $socialNetwork = new PlatformSocialNameEnum($httpRequest->getPostVar('social_network_id'));
            } catch (Exception $e) {
                /**  */
            }
        if ($this->isPostVar($httpRequest, 'group_page_id'))
            try {
                Assert::isInteger($httpRequest->getPostVar('group_page_id'));
                if ($httpRequest->getPostVar('group_page_id') != 0)
                    $groupPage =
                        (new PlatformGroupAndPage())->dao()->getById($httpRequest->getPostVar('group_page_id'));
            } catch (WrongArgumentException $e) {
                /**  */
            };
        try {
            Assert::isInteger($httpRequest->getPostVar('visibleCount'));
            $offset = $httpRequest->getPostVar('visibleCount');
        } catch (WrongArgumentException $e) {
            /**  */
        }
        $links = (new PlatformReferrersLink())->dao()->getList(10, $groupPage, $socialNetwork, $offset);

        foreach ($links as $link) {
            $times = PlatformCrawlTime::dao()->getTimeListByLinkId($link['id']);
            $link['times'] = $times;
            $list[] = $link;
        }
        $model
            ->set('linksCount', (count($list) === 0) ? 0 : $list[0]['total_count'])
            ->set('links', $list)
            ->set('success', true);
        return (new ModelAndView())->setModel($model)->setView(new JsonView());
    }

    public function addLinkAction(HttpRequest $httpRequest)
    {
        $model = new Model();
        $form = new Form();
        $form
            ->add(Primitive::string('link')->required())
            ->add(Primitive::enum('social_network')->of('PlatformSocialNameEnum')->required())
            ->add(Primitive::integer('group_page_id')->required())
            ->add(Primitive::timestampTZ('start_date')->required())
            ->add(Primitive::integer('id'));
        $form->import($httpRequest->getPost());
        $this->parseContent(file_get_contents($httpRequest->getPostVar('link')));
        if (is_null($id = $form->get('id')->getValue()))
            $link = (new PlatformReferrersLink())
                ->dao()
                ->add(
                    (new PlatformReferrersLink())
                        ->setUrlLink($form->get('link')->getValue())
                        ->setTitle(html_entity_decode($this->title))
                        ->setDescription(html_entity_decode($this->description))
                        ->setImgSrc($this->imageSrc)
                );
        else
            $link = (new PlatformReferrersLink())->dao()->getById($id);
        /** @var PlatformCrawlTime $time */
        (new PlatformCrawlTime())
            ->dao()
            ->add(
                (new PlatformCrawlTime())
                    ->setReferrersLink($link)
                    ->setGroupAndPageId($form->get('group_page_id')->getValue())
                    ->setBeginAt($form->get('start_date')->getValue())
                    ->setStatus(PlatformCrawlTimeStatus::expects())
            );
        $model
            ->set('success', true)
            ->set('id', $link->getId())
            ->set('times', (new PlatformCrawlTime())->dao()->getTimeListByLinkId($link->getId()))
            ->set('title', html_entity_decode($this->title))
            ->set('description', html_entity_decode($this->description))
            ->set('img_src', $this->imageSrc);
        return (new ModelAndView())
            ->setModel($model)
            ->setView(new JsonView());
    }

    public function addGroupAndPageAction(HttpRequest $httpRequest)
    {
        $model = new Model();
        $form = new Form();
        $form
            ->add(Primitive::enum('social_network')->of('PlatformSocialNameEnum')->required())
            ->add(Primitive::httpUrl('url')->setMin(10)->setMax(512)->required())
            ->add(Primitive::string('name')->setMin(5)->setMax(256)->required());
        $form
            ->addMissingLabel('social_network', 'Поле обязательно для заполнения')
            ->addWrongLabel('social_network', 'Не правильно заполенено поле')
            ->addMissingLabel('url', 'Поле "URL" обязательно для заполнения')
            ->addWrongLabel('url', 'Не правильно заполенено поле "URL"')
            ->addMissingLabel('name', 'Поле "Название" обязательно для заполнения')
            ->addWrongLabel('name', 'Не правильно заполенено поле "Название"');
        $form->import($httpRequest->getPost());
        if ($form->getErrors()) {
            foreach ($form->getPrimitiveNames() as $name) {
                if (!is_null($error = $form->getTextualErrorFor($name)))
                    $model->set($name, $error);
            }
            return (new ModelAndView())
                ->setModel(
                    $model
                        ->set('code', 0x3ED)
                        ->set('success', false)
                )
                ->setView(new JsonView());
        }
        try {
            /** @var  HttpUrl $url */
            $groupOrPage = (new PlatformGroupAndPage())->dao()->getByUrl($url = $form->get('url')->getValue());
            /** @var  PlatformSocialNameEnum $socialNetwork */
            $socialNetwork = $form->getValue('social_network');
            if ($url->getHost() != $socialNetwork->getHost())
                return (new ModelAndView())
                    ->setModel(
                        $model
                            ->set('success', false)
                            ->set('code', 0x3EC)
                    )
                    ->setView(new JsonView());
            if (!$groupOrPage) {
                $groupOrPage = (new PlatformGroupAndPage())->dao()->add(
                    (new PlatformGroupAndPage())
                        ->setName($form->get('name')->getValue())
                        ->setSocialNetwork($form->getValue('social_network'))
                        ->setSocialLink($form->get('url')->getValue())
                );
                $model
                    ->set('id', $groupOrPage->getId())
                    ->set('success', true);
            } else {
                $model
                    ->set('success', false)
                    ->set('code', 0x3E9)
                    ->set('error', 'This page or group already exists');
            }
        } catch (Exception $exception) {
            return (new ModelAndView())
                ->setModel(
                    $model
                        ->set('success', false)
                        ->set('code', 0x3EA)
                        ->set('error', $exception->getMessage())
                )
                ->setView(new JsonView());
        } catch (Error $error) {
            return (new ModelAndView())
                ->setModel(
                    $model
                        ->set('success', false)
                        ->set('code', 0x3EB)
                        ->set('error', $error->getMessage())
                )
                ->setView(new JsonView());
        }
        return (new ModelAndView())->setModel($model)->setView(new JsonView());
    }

    public function dropLinkAction(HttpRequest $httpRequest)
    {
        $form = new Form();
        $model = new Model();
        $form->add(Primitive::integer('id')->required());
        $form->import($httpRequest->getPost());
        try {
            $times = (new PlatformCrawlTime())->dao()->getByLinkId($ID = $form->get('id')->getValue());
            foreach ($times as $time) {
                $visits = (new PlatformVisitedPageCrawlTime())->dao()->getByCrawl($time);
                foreach ($visits as $visit)
                    (new PlatformVisitedPageCrawlTime())->dao()->drop($visit);
                (new PlatformCrawlTime())->dao()->drop($time);
            };
            (new PlatformReferrersLink())->dao()->dropById($ID);

            $model->set('success', true);
        } catch (Exception $e) {
            $model->set('success', false);
        }
        return (new ModelAndView())
            ->setModel($model)
            ->setView(new JsonView());
    }

    public function rejectAction(HttpRequest $httpRequest)
    {
        $form = new Form();
        $model = new Model();
        $form
            ->add(
                Primitive::integer('id')->required()
            );
        $form->import($httpRequest->getPost());
        try {
            (new PlatformCrawlTime())->dao()->save(
                (new PlatformCrawlTime())
                    ->dao()->getById($form->get('id')->getValue())->setStatus(PlatformCrawlTimeStatus::rejected())
            );
            $model->set('success', true);
        } catch (Exception $e) {
            $model->set('success', false);
        }
        return (new ModelAndView())
            ->setModel($model)
            ->setView(new JsonView());
    }

    public function removeAction(HttpRequest $httpRequest)
    {
        $form = new Form();
        $model = new Model();
        $form
            ->add(
                Primitive::integer('id')->required()
            )
            ->add(
                Primitive::integer('link_id')->required()
            );
        $form->import($httpRequest->getPost());
        try {
            (new PlatformCrawlTime())->dao()->dropById($form->get('id')->getValue());
            $model
                ->set('success', true)
                ->set(
                    'times',
                    (new PlatformCrawlTime())->dao()->getTimeListByLinkId($form->get('link_id')->getValue())
                );
        } catch (Exception $e) {
            $model->set('success', false);
        }
        return (new ModelAndView())
            ->setModel($model)
            ->setView(new JsonView());
    }

    private function parseContent($content)
    {
        if (
            $this->imageSrc === false &&
            preg_match('#<link rel="image_src" href="(.+?)"#is', $content, $img) === 1
        )
            $this->imageSrc = $img[1];
        if (
            $this->imageSrc === false &&
            preg_match('#<meta name="twitter:image" content="(.+?)"#is', $content, $img) === 1
        )
            $this->imageSrc = $img[1];
        if (
            $this->imageSrc === false &&
            preg_match('#<meta name="image_src" content="(.+?)"#is', $content, $img) === 1
        )
            $this->imageSrc = $img[2];
        if (
            $this->imageSrc === false &&
            preg_match('#<meta name="og:image" content="(.+?)"#is', $content, $img) === 1
        )
            $this->imageSrc = $img[2];
        if (
            $this->title === false &&
            preg_match('#<title>(.+?)</title>#is', $content, $title) === 1
        )
            $this->title = $title[1];
        if ($this->title === false &&
            preg_match('#<meta name="title" content="(.+?)"#is', $content, $title) === 1
        )
            $this->title = $title[1];
        if (
            $this->title === false &&
            preg_match('#<meta name="twitter:title" content="(.+?)"#is', $content, $title) === 1
        )
            $this->title = $title[1];
        if (
            $this->title === false &&
            preg_match('#<meta name="og:title" content="(.+?)"#is', $content, $title) === 1
        )
            $this->title = $title[1];
        if (
            $this->description === false &&
            preg_match('#<meta name="twitter:description" content="(.+?)"#is', $content, $description) === 1
        )
            $this->description = $description[1];
        if (
            $this->description === false &&
            preg_match('#<meta name="og:description" content="(.+?)"#is', $content, $description) === 1
        )
            $this->description = $description[1];
        if (
            $this->description === false &&
            preg_match('#<meta name="description" content="(.+?)"#is', $content, $description) === 1
        )
            $this->description = $description[1];

    }


    public function addLink(PlatformSocialPublishedLink $link)
    {
        try {
            $group = PlatformGroupAndPage::dao()->getByName(
                $link->getAppAdminGroup()->getName(),
                $link->getAppAdminGroup()->getAppAdmin()->getApp()->getSocialNetwork()
            );
            $this->parseContent(file_get_contents($link->getLinkUrl()));
            $ref = PlatformReferrersLink::dao()->getByLink($link->getLinkUrl());
            if (is_null($ref))
                $ref = (new PlatformReferrersLink())->dao()
                    ->add(
                        (new PlatformReferrersLink())
                            ->setUrlLink($link->getLinkUrl())
                            ->setTitle(html_entity_decode($this->title))
                            ->setDescription(html_entity_decode($this->description))
                            ->setImgSrc($this->imageSrc)
                    );
            /** @var TimestampTZ $time */
            $time = $link->getDeferredTime();
            if (is_null($time))
                $time = TimestampTZ::makeNow();
            for ($i = 0; $i < 8; $i++) {
                PlatformCrawlTime::dao()->add(
                    (new PlatformCrawlTime())
                        ->setStatus(PlatformCrawlTimeStatus::expects())
                        ->setBeginAt($time->modify('+2 hour'))
                        ->setGroupAndPage($group)
                        ->setReferrersLink($ref)
                );
            }
        }catch (ObjectNotFoundException $e)
        {
            return false;
        }catch (Exception $e)
        {
            return false;
        }
    }

    /**
     * Мапинг методов конроллера
     * Можно вернуть пустой массив если брать с учетом
     * что экшен будет тот который прописан в роут конфиге
     *
     * @return array
     */
    protected function getMapping()
    {
        return [
            'index' => 'indexAction',
            'addGroupAndPage' => 'addGroupAndPageAction',
            'getFreeTime' => 'getFreeTimeAction',
            'addLink' => 'addLinkAction',
            'loadMore' => 'loadMoreAction',
            'loadData' => 'loadDataAction',
            'dropLink' => 'dropLinkAction',
            'reject' => 'rejectAction',
            'remove' => 'removeAction',
            'addTime' => 'addTimeAction'
        ];
    }
}