<?php

/**
 * Контроллер Сервиса
 *
 * Created by PhpStorm.
 * User: root
 * Date: 30.12.15
 * Time: 15:09
 */
class ServicesController extends ProjectServiceController
{
    /** @var  Form */
    private $form;

    /**
     * главная вьюха
     *
     * @return ModelAndView
     */
    public function indexView()
    {

        return
            (new ModelAndView())
                ->setModel(
                    (new Model())
                        ->set('tplName', 'services/home')
                )
                ->setView('services/index');
    }

    /**
     * вьха документации
     *
     * @return ModelAndView
     */
    public function docView()
    {
        return
            (new ModelAndView())
                ->setModel(
                    (new Model())
                        ->set('tplName', 'services/doc'))
                ->setView('services/index');

    }

    /**
     * Метод сервиса добавления в очередь ссылки
     *
     * @param HttpRequest $httpRequest
     * @return ModelAndView
     */
    public function linkAction(HttpRequest $httpRequest)
    {

        $this->header('Access-Control-Allow-Origin: *');
        $this->header("Access-Control-Allow-Credentials: true");
        $this->header('Access-Control-Allow-Methods: GET, PUT, POST, DELETE, OPTIONS');
        $this->header('Access-Control-Max-Age: 1000');
        $this->header(
            'Access-Control-Allow-Headers: Content-Type, Content-Range, Content-Disposition, Content-Description'
        );


        if (count(PlatformSocialPublishedLink::dao()->getByLink(urldecode($httpRequest->getGetVar('link')))) != 0) {
            return $this->mavJson(
                (new Model())
                    ->set('success', false)
                    ->set('errorCode', 201)
                    ->set('error', 'link exists')

            );
        }

        $link = (new PlatformSocialPublishedLink())
            ->setFlow($this->getFlow())
            ->setDescription($httpRequest->getGetVar('description'))
            ->setLinkUrl(urldecode($httpRequest->getGetVar('link')))
            ->setImgUrl(urldecode($httpRequest->getGetVar('imgUrl')))
            ->setEverywhere(true)
            ->setCreatedAt(TimestampTZ::makeNow());


        /** @var PlatformSocialPublishedLink $link */
        $link->dao()->add($link);

        return $this->mavJson(
            (new Model())->set('success', true)->set('message', 'Added to queue')
        );
    }

    /**
     * Получение групп и страниц в данном канале
     *
     * @return ModelAndView
     */
    public function getChannelAction()
    {
        $this->header('Access-Control-Allow-Origin: *');
        $this->header("Access-Control-Allow-Credentials: true");
        $this->header('Access-Control-Allow-Methods: GET, PUT, POST, DELETE, OPTIONS');
        $this->header('Access-Control-Max-Age: 1000');
        $this->header(
            'Access-Control-Allow-Headers: Content-Type, Content-Range, Content-Disposition, Content-Description'
        );

        return
            $this->mavJson(
                (new Model())
                    ->set('success', true)
                    ->set('groups', $this->getFlow()->getGroupsArray())
                    ->set('pages', $this->getFlow()->getPagesArray())
            );
    }

    /**
     * Проверка на доступность сылки
     *
     * @param HttpRequest $httpRequest
     * @return ModelAndView
     */
    public function checkLinkAction(HttpRequest $httpRequest)
    {
        $this->header('Access-Control-Allow-Origin: *');
        $this->header("Access-Control-Allow-Credentials: true");
        $this->header('Access-Control-Allow-Methods: GET, PUT, POST, DELETE, OPTIONS');
        $this->header('Access-Control-Max-Age: 1000');
        $this->header(
            'Access-Control-Allow-Headers: Content-Type, Content-Range, Content-Disposition, Content-Description'
        );

        try {

            $response = [];

            if ($this->isGetVar($httpRequest, 'materialId')) {
                $materialId = $httpRequest->getGetVar('materialId');
            } else {
                $materialId = null;
            }


            $links = $this->getFlow()->getDataLinkByLink($httpRequest->getGetVar('link'), $materialId);

            $i = 0;
            foreach ($links as $link) {
                if (!is_null($link->getAppAdminPageId())) {

                    $response['pages'][$i]['id'] = $link->getAppAdminPageId();
                    $response['pages'][$i]['status'] = $link->getStatus()->getId();

                    if ($link->isPublished())
                        $response['pages'][$i]['time'] = $link->getPublishedAt()->toFormatString('d.m.Y H:i');

                    if ($link->isPostponed())
                        $response['pages'][$i]['time'] = $link->getDeferredTime()->toFormatString('d.m.Y H:i');

                    $response['pages'][$i]['description'] = $link->getDescription();

                    $i = ++$i;
                }
            }

            $i = 0;
            foreach ($links as $link) {
                if (!is_null($link->getAppAdminGroupId())) {

                    $response['groups'][$i]['id'] = $link->getAppAdminGroupId();
                    $response['groups'][$i]['status'] = $link->getStatus()->getId();

                    if ($link->isPublished())
                        $response['groups'][$i]['time'] = $link->getPublishedAt()->toFormatString('d.m.Y H:i');

                    if ($link->isPostponed())
                        $response['groups'][$i]['time'] = $link->getDeferredTime()->toFormatString('d.m.Y H:i');

                    $response['groups'][$i]['description'] = $link->getDescription();
                    $i++;
                }
            }


            return
                $this->mavJson(
                    (new Model())
                        ->set('success', true)
                        ->set('access', 'limited')
                        ->set('data', $response)
                );

        } catch (ObjectNotFoundException $e) {
            return
                $this->mavJson(
                    (new Model())
                        ->set('success', true)
                        ->set('access', 'all')
                );
        }
    }

    /**
     * Публикация в группу в фейсбуке
     *
     * @param HttpRequest $httpRequest
     * @return ModelAndView
     * @throws MissingElementException
     */
    public function groupPublicationFacebookAction(HttpRequest $httpRequest)
    {
        $model = new Model();

        $this->header('Access-Control-Allow-Origin: *');
        $this->header("Access-Control-Allow-Credentials: true");
        $this->header('Access-Control-Allow-Methods: GET, PUT, POST, DELETE, OPTIONS');
        $this->header('Access-Control-Max-Age: 1000');
        $this->header(
            'Access-Control-Allow-Headers: Content-Type, Content-Range, Content-Disposition, Content-Description'
        );

        $this->form = $this->getLinkForms()->import($httpRequest->getGet());

        if ($this->form->getErrors()) {
            foreach ($this->form->getPrimitiveNames() as $name) {
                if (!is_null($error = $this->form->getTextualErrorFor($name)))
                    $model->set($name, $error);
            }
            return $this->mavJson($model->set('success', false)->set('errorCode', 202));
        }

        $publishedLink = (new PlatformSocialPublishedLink())
            ->setAppAdminGroupId($this->form->get('id')->getValue());


        if (!is_null($materialType = MaterialTypeEnum::getByNames($this->form->get('materialType')->getValue())))
            $publishedLink->setMaterialType($materialType);

        if ((integer)$this->form->get('everywhere')->getValue() == 1)
            $publishedLink->setEverywhere(true);
        else
            $publishedLink->setEverywhere(false);

        if (!is_null($this->form->get('description')->getValue()))
            $publishedLink->setDescription($this->form->get('description')->getValue());

        $publishedLink
            ->setFlow($this->getFlow())
            ->setLinkUrl($this->form->get('link')->getValue())
            ->setMaterialId($this->form->get('materialId')->getValue())
            ->setCreatedAt(TimestampTZ::makeNow())
            ->setDeferredTime($this->form->get('publishedAt')->getValue());

        if ((integer)$this->form->get('postponed')->getValue() == 1)
            $publishedLink->setPostponed(true);
        else
            $publishedLink->setPostponed(false);

        $publishedLink->dao()->add($publishedLink);

        if ($publishedLink->isPostponed()) {
            $publishedLink->dao()->save($publishedLink->setStatus(LinkStatusEnum::excepts()));
            return
                $this->mavJson(
                    (new Model())
                        ->set('success', true)
                        ->set('groupId', $this->form->get('id')->getValue())
                        ->set('status', LinkStatusEnum::excepts()->getId())
                );
        }

        try {
            (new PlatformSocialProcessor())->groupPublicationLinkFacebook($publishedLink);

            return
                $this->mavJson(
                    (new Model())
                        ->set('success', true)
                        ->set('groupId', $this->form->get('id')->getValue())
                        ->set('status', LinkStatusEnum::published()->getId())
                );

        } catch (ServiceException $e) {
            return
                $this->mavJson(
                    (new Model())
                        ->set('success', true)
                        ->set('groupId', $this->form->get('id')->getValue())
                        ->set('status', LinkStatusEnum::publishedError()->getId())
                        ->set('error', $e->getMessage())
                );
        }
    }

    /**
     * Публикация на страницу в фейсбуке
     *
     * @param HttpRequest $httpRequest
     * @return ModelAndView
     * @throws MissingElementException
     */
    public function pagePublicationFacebookAction(HttpRequest $httpRequest)
    {
        $model = new Model();

        $this->header('Access-Control-Allow-Origin: *');
        $this->header("Access-Control-Allow-Credentials: true");
        $this->header('Access-Control-Allow-Methods: GET, PUT, POST, DELETE, OPTIONS');
        $this->header('Access-Control-Max-Age: 1000');
        $this->header(
            'Access-Control-Allow-Headers: Content-Type, Content-Range, Content-Disposition, Content-Description'
        );

        $this->form = $this->getLinkForms()->import($httpRequest->getGet());

        if ($this->form->getErrors()) {
            foreach ($this->form->getPrimitiveNames() as $name) {
                if (!is_null($error = $this->form->getTextualErrorFor($name)))
                    $model->set($name, $error);
            }
            return $this->mavJson($model->set('success', false)->set('errorCode', 202));
        }

        $publishedLink = (new PlatformSocialPublishedLink())
            ->setAppAdminPageId($this->form->get('id')->getValue());


        if (!is_null($materialType = MaterialTypeEnum::getByNames($this->form->get('materialType')->getValue())))
            $publishedLink->setMaterialType($materialType);

        if ((integer)$this->form->get('everywhere')->getValue() == 1)
            $publishedLink->setEverywhere(true);
        else
            $publishedLink->setEverywhere(false);

        if (!is_null($this->form->get('description')->getValue()))
            $publishedLink->setDescription($this->form->get('description')->getValue());


        $publishedLink
            ->setFlow($this->getFlow())
            ->setLinkUrl($this->form->get('link')->getValue())
            ->setMaterialId($this->form->get('materialId')->getValue())
            ->setCreatedAt(TimestampTZ::makeNow())
            ->setDeferredTime($this->form->get('publishedAt')->getValue());

        if ((integer)$this->form->get('postponed')->getValue() == 1)
            $publishedLink->setPostponed(true);
        else
            $publishedLink->setPostponed(false);

        $publishedLink->dao()->add($publishedLink);

        if ($publishedLink->isPostponed()) {
            $publishedLink->dao()->save($publishedLink->setStatus(LinkStatusEnum::excepts()));
            return
                $this->mavJson(
                    (new Model())
                        ->set('success', true)
                        ->set('pageId', $this->form->get('id')->getValue())
                        ->set('status', LinkStatusEnum::excepts()->getId())
                        ->set(
                            'message', 'published in ' . $publishedLink->getDeferredTime()->toFormatString('d.m.Y H:i')
                        )
                );
        }

        try {
            (new PlatformSocialProcessor())->pagePublicationLinkFacebook($publishedLink);

            return
                $this->mavJson(
                    (new Model())
                        ->set('success', true)
                        ->set('pageId', $this->form->get('id')->getValue())
                        ->set('status', LinkStatusEnum::published()->getId())
                );

        } catch (ServiceException $e) {
            return
                $this->mavJson(
                    (new Model())
                        ->set('success', false)
                        ->set('pageId', $this->form->get('id')->getValue())
                        ->set('status', LinkStatusEnum::publishedError()->getId())
                        ->set('error', $e->getMessage())
                );
        }

    }

    /**
     * Публикация в группу в вконтакте
     *
     * @param HttpRequest $httpRequest
     * @return ModelAndView
     * @throws MissingElementException
     */
    public function groupPublicationVkontakteAction(HttpRequest $httpRequest)
    {
        $model = new Model();

        $this->header('Access-Control-Allow-Origin: *');
        $this->header("Access-Control-Allow-Credentials: true");
        $this->header('Access-Control-Allow-Methods: GET, PUT, POST, DELETE, OPTIONS');
        $this->header('Access-Control-Max-Age: 1000');
        $this->header(
            'Access-Control-Allow-Headers: Content-Type, Content-Range, Content-Disposition, Content-Description'
        );

        $this->form = $this->getLinkForms()->import($httpRequest->getGet());

        if ($this->form->getErrors()) {
            foreach ($this->form->getPrimitiveNames() as $name) {
                if (!is_null($error = $this->form->getTextualErrorFor($name)))
                    $model->set($name, $error);
            }
            return $this->mavJson($model->set('success', false)->set('errorCode', 202));
        }

        $publishedLink = (new PlatformSocialPublishedLink())
            ->setAppAdminGroupId($this->form->get('id')->getValue());


        if (!is_null($materialType = MaterialTypeEnum::getByNames($this->form->get('materialType')->getValue())))
            $publishedLink->setMaterialType($materialType);

        if ((integer)$this->form->get('everywhere')->getValue() == 1)
            $publishedLink->setEverywhere(true);
        else
            $publishedLink->setEverywhere(false);

        if (!is_null($this->form->get('imgUrl')->getValue()))
            $publishedLink->setImgUrl($this->form->get('imgUrl')->getValue());

        if (!is_null($this->form->get('description')->getValue()))
            $publishedLink->setDescription($this->form->get('description')->getValue());

        $publishedLink
            ->setFlow($this->getFlow())
            ->setLinkUrl($this->form->get('link')->getValue())
            ->setMaterialId($this->form->get('materialId')->getValue())
            ->setCreatedAt(TimestampTZ::makeNow())
            ->setDeferredTime($this->form->get('publishedAt')->getValue());

        if ((integer)$this->form->get('postponed')->getValue() == 1)
            $publishedLink->setPostponed(true);
        else
            $publishedLink->setPostponed(false);

        $publishedLink->dao()->add($publishedLink);

        if ($publishedLink->isPostponed()) {
            $publishedLink->dao()->save($publishedLink->setStatus(LinkStatusEnum::excepts()));
            return
                $this->mavJson(
                    (new Model())
                        ->set('success', true)
                        ->set('groupId', $this->form->get('id')->getValue())
                        ->set('status', LinkStatusEnum::excepts()->getId())
                );
        }

        try {
            (new PlatformSocialProcessor())->groupPublicationLinkVkontakte($publishedLink);

            return
                $this->mavJson(
                    (new Model())
                        ->set('success', true)
                        ->set('groupId', $this->form->get('id')->getValue())
                        ->set('status', LinkStatusEnum::published()->getId())
                );

        } catch (ServiceException $e) {
            return
                $this->mavJson(
                    (new Model())
                        ->set('success', true)
                        ->set('groupId', $this->form->get('id')->getValue())
                        ->set('status', LinkStatusEnum::publishedError()->getId())
                        ->set('error', $e->getMessage())
                );
        }
    }

    /**
     * Отправка сообщений на телефон
     *
     * @param HttpRequest $httpRequest
     * @return ModelAndView
     */
    public function sendSmsAction(HttpRequest $httpRequest)
    {
        $sendSms = (new PlatformSendSms());

        try {

            $config =
                PlatformConfig::create()
                    ->setConfig(ENVIRONMENT_PROJECT);


            $sendSms
                ->setHost($config->getItemConfig('smsService')->getValueByKey('host'))
                ->setLogin($config->getItemConfig('smsService')->getValueByKey('login'))
                ->setPassword($config->getItemConfig('smsService')->getValueByKey('password'))
                ->setPath($config->getItemConfig('smsService')->getValueByKey('path'))
                ->setPort($config->getItemConfig('smsService')->getValueByKey('port'))
                ->setPhone($httpRequest->getGetVar('phone'))
                ->setMessage($httpRequest->getGetVar('message'))
                ->send();

            return
                $this->mavJson(
                    (new Model())
                        ->set('success', true)
                );
        }catch (ServiceException $e)
        {
            return
                $this->mavJson(
                    (new Model())
                        ->set('success', false)
                );
        }
    }

    /**
     * Публикация в группу в одноклассники
     *
     * @param HttpRequest $httpRequest
     * @return ModelAndView
     * @throws MissingElementException
     */
    public function groupPublicationOkAction(HttpRequest $httpRequest)
    {
        $model = new Model();

        $this->header('Access-Control-Allow-Origin: *');
        $this->header("Access-Control-Allow-Credentials: true");
        $this->header('Access-Control-Allow-Methods: GET, PUT, POST, DELETE, OPTIONS');
        $this->header('Access-Control-Max-Age: 1000');
        $this->header(
            'Access-Control-Allow-Headers: Content-Type, Content-Range, Content-Disposition, Content-Description'
        );

        $this->form = $this->getLinkForms()->import($httpRequest->getGet());

        if ($this->form->getErrors()) {
            foreach ($this->form->getPrimitiveNames() as $name) {
                if (!is_null($error = $this->form->getTextualErrorFor($name)))
                    $model->set($name, $error);
            }
            return $this->mavJson($model->set('success', false)->set('errorCode', 202));
        }

        $publishedLink = (new PlatformSocialPublishedLink())
            ->setAppAdminGroupId($this->form->get('id')->getValue());


        if (!is_null($materialType = MaterialTypeEnum::getByNames($this->form->get('materialType')->getValue())))
            $publishedLink->setMaterialType($materialType);

        if ((integer)$this->form->get('everywhere')->getValue() == 1)
            $publishedLink->setEverywhere(true);
        else
            $publishedLink->setEverywhere(false);

        if (!is_null($this->form->get('imgUrl')->getValue()))
            $publishedLink->setImgUrl($this->form->get('imgUrl')->getValue());

        if (!is_null($this->form->get('description')->getValue()))
            $publishedLink->setDescription(addslashes($this->form->get('description')->getValue()));

        $publishedLink
            ->setFlow($this->getFlow())
            ->setLinkUrl($this->form->get('link')->getValue())
            ->setMaterialId($this->form->get('materialId')->getValue())
            ->setCreatedAt(TimestampTZ::makeNow())
            ->setDeferredTime($this->form->get('publishedAt')->getValue());

        if ((integer)$this->form->get('postponed')->getValue() == 1)
            $publishedLink->setPostponed(true);
        else
            $publishedLink->setPostponed(false);

        $publishedLink->dao()->add($publishedLink);

        (new ManagingTheTransitionController())->addLink($publishedLink);

        if ($publishedLink->isPostponed()) {
            $publishedLink->dao()->save($publishedLink->setStatus(LinkStatusEnum::excepts()));
            return
                $this->mavJson(
                    (new Model())
                        ->set('success', true)
                        ->set('groupId', $this->form->get('id')->getValue())
                        ->set('status', LinkStatusEnum::excepts()->getId())
                );
        }

        try {
            (new PlatformSocialProcessor())->groupPublicationLinkOk($publishedLink);

            return
                $this->mavJson(
                    (new Model())
                        ->set('success', true)
                        ->set('groupId', $this->form->get('id')->getValue())
                        ->set('status', LinkStatusEnum::published()->getId())
                );

        } catch (ServiceException $e) {
            return
                $this->mavJson(
                    (new Model())
                        ->set('success', true)
                        ->set('groupId', $this->form->get('id')->getValue())
                        ->set('status', LinkStatusEnum::publishedError()->getId())
                        ->set('error', $e->getMessage())
                );
        }
    }

    /**
     * Форма валидации запроса
     *
     * @return Form
     */
    public function getLinkForms()
    {
        return (new Form())
            ->set(
                Primitive::integer('id')->required()
            )
            ->addMissingLabel('id', 'Not transferred to the \'id\' field')
            ->addCustomLabel('id', Form::WRONG, 'Field \'id\' going to be the type of integer')
            ->set(
                Primitive::string('link')->required()
            )
            ->addMissingLabel('link', 'Not transferred to the \'link\' field')
            ->addCustomLabel('link', Form::WRONG, 'Field \'link\' not url')
            ->set(
                Primitive::string('materialType')->required()
            )
            ->addMissingLabel('materialType', 'Not transferred to the \'materialType\' field')
            ->addCustomLabel('materialType', Form::WRONG, 'Field \'materialType\' going to be the type of string')
            ->set(
                Primitive::integer('everywhere')->required()
            )
            ->addMissingLabel('everywhere', 'Not transferred to the \'everywhere\' field')
            ->addCustomLabel('everywhere', Form::WRONG, 'Field \'everywhere\' going to be the type of integer ')
            ->set(
                Primitive::timestampTZ('publishedAt')
            )
            ->addCustomLabel('publishedAt', Form::WRONG, 'Field \'publishedAt\' going to be the type of timestamp')
            ->set(
                Primitive::integer('materialId')->required()
            )
            ->addMissingLabel('materialId', 'Not transferred to the \'materialId\' field')
            ->addCustomLabel('materialId', Form::WRONG, 'Field \'materialId\' going to be the type of integer')
            ->set(
                Primitive::integer('postponed')->required()
            )
            ->addMissingLabel('postponed', 'Not transferred to the \'postponed\' field')
            ->addCustomLabel('postponed', Form::WRONG, 'Field \'postponed\' going to be the type of integer')
            ->set(
                Primitive::string('imgUrl')
            )
            ->addCustomLabel('imgUrl', Form::WRONG, 'Field \'imgUrl\' going to be the type of integer')
            ->set(
                Primitive::string('description')
            )
            ->addCustomLabel('description', Form::WRONG, 'Field \'description\' going to be the type of integer');
    }


    /**
     * Список видео
     *
     * @param HttpRequest $httpRequest
     * @return ModelAndView
     */
    public function getVideoListAction(HttpRequest $httpRequest)
    {
        $model = new Model();

        $this->header('Access-Control-Allow-Origin: *');
        $this->header("Access-Control-Allow-Credentials: true");
        $this->header('Access-Control-Allow-Methods: GET, PUT, POST, DELETE, OPTIONS');
        $this->header('Access-Control-Max-Age: 1000');
        $this->header(
            'Access-Control-Allow-Headers: Content-Type, Content-Range, Content-Disposition, Content-Description'
        );

        $request = new PlatformSocialVideoRequest();
        $form = $request->proto()->makeForm()->import($httpRequest->getGet());

        FormUtils::form2object($form, $request);

        try {
            $model->set(
                'data',
                SocialPublishedVideo::dao()->getListByPlatformSocialVideoRequest($request, $this->getFlow())
            );
        } catch (ObjectNotFoundException $e) {
            $model->set('data', []);
        }

        return $this->mavJson($model);

    }

    /**
     * Загрузка видео
     *
     *
     * @param HttpRequest $httpRequest
     * @return ModelAndView
     */
    public function uploadVideoFileAction(HttpRequest $httpRequest)
    {
        $model = new Model();

        $this->header('Access-Control-Allow-Origin: *');
        $this->header("Access-Control-Allow-Credentials: true");
        $this->header('Access-Control-Allow-Methods: GET');
        $this->header('Access-Control-Allow-Methods: PUT');
        $this->header('Access-Control-Allow-Methods: POST');
        $this->header('Access-Control-Allow-Methods: DELETE');
        $this->header('Access-Control-Allow-Methods: OPTIONS');
        $this->header('Access-Control-Max-Age: 10000');
        $this->header(
            'Access-Control-Allow-Headers: Content-Type, Content-Range, Content-Disposition, Content-Description'
        );

        $uploadAt = TimestampTZ::makeNow();

        $this->form = $this
            ->getFormUploadVideoFile()
            ->importMore($httpRequest->getGet())
            ->importMore($httpRequest->getFiles());

        if ($this->form->getErrors()) {
            foreach ($this->form->getPrimitiveNames() as $name) {
                if (!is_null($error = $this->form->getTextualErrorFor($name)))
                    $model->set($name, $error);
            }
            return $this
                ->mavJson(
                    $model
                        ->set('success', false)
                        ->set('errorCode', 202)
                );
        }

        try {

            $mimeType = MimeType::getByMimeType($this->form->get('file')->getRawValue()['type']);

            $fileName =
                \Helpers\Uploader\TmpStore::create()->getPath() . 'video/' .
                $uploadAt->toStamp() . '.' . $mimeType->getExtension();

            $publishedVideo = (new SocialPublishedVideo());
            $publishedVideoData = (new SocialPublishedVideoData());


            FileUtils::upload(
                $this->form->get('file')->getRawValue()['tmp_name'],
                $fileName
            );

            $publishedVideo
                ->setTitle($this->form->get('title')->getValue())
                ->setDescription($this->form->get('description')->getValue())
                ->setPublishedAt($this->form->get('publishedAt')->getValue());

            if (
                $httpRequest->getGetVar('type') == 'page'
            )
                $publishedVideo
                    ->setAppAdminPageId($httpRequest->getGetVar('id'));
            else
                $publishedVideo
                    ->setAppAdminGroupId($httpRequest->getGetVar('id'));

            $publishedVideo
                ->setCreatedAt($createdAt = TimestampTZ::makeNow())
                ->setStatus($status = LinkStatusEnum::excepts())
                ->setPathToTheFile($fileName);


            (new SocialPublishedVideo())
                ->dao()
                ->add($publishedVideo);

            $publishedVideoData
                ->setStatus($status)
                ->setCreatedAt($createdAt)
                ->setPublishedVideo($publishedVideo);

            (new SocialPublishedVideoData())->dao()->add($publishedVideoData);

        } catch (Exception $e) {
            return $this->mavJson(
                $model->set('success', false)
            );
        }

        return $this->mavJson(
            $model->set(
                'success', true
            )
        );
    }

    /**
     * @return Form
     */
    public function getFormUploadVideoFile()
    {
        return (new Form())
            ->set(
                Primitive::string('title')->required()
            )
            ->addMissingLabel('title', 'Not transferred to the \'title\' field')
            ->addCustomLabel('title', Form::WRONG, 'Field \'title\' going to be the type of string')
            ->set(
                Primitive::string('description')->required()
            )
            ->addMissingLabel('description', 'Not transferred to the \'description\' field')
            ->addCustomLabel('description', Form::WRONG, 'Field \'description\' going to be the type of integer')
            ->set(
                Primitive::timestampTZ('publishedAt')->required()
            )
            ->addMissingLabel('publishedAt', 'Not transferred to the \'publishedAt\' field')
            ->addCustomLabel('publishedAt', Form::WRONG, 'Field \'publishedAt\' going to be the type of timestamp')
            ->set(
                Primitive::file('file')
                    ->addAllowedMimeType('video/mp4')
                    ->required()
            )
            ->addMissingLabel('file', 'Not transferred to the \'file\' field')
            ->addCustomLabel('file', Form::WRONG, 'File no type *.mp4');
    }

    /**
     * Маппинг
     *
     * @return array
     */
    protected function getMapping()
    {
        return [
            'index' => 'indexView',
            'doc' => 'docView',

            'link' => 'linkAction',
            'getChannel' => 'getChannelAction',
            'checkLink' => 'checkLinkAction',
            'groupPublicationFacebook' => 'groupPublicationFacebookAction',
            'pagePublicationFacebook' => 'pagePublicationFacebookAction',
            'groupPublicationVkontakte' => 'groupPublicationVkontakteAction',
            'groupPublicationOk' => 'groupPublicationOkAction',
            'uploadVideoFile' => 'uploadVideoFileAction',
            'getVideoList' => 'getVideoListAction',
            'sendSms'=> 'sendSmsAction'
        ];
    }

    /**
     * Доступные методы без авторизации
     *
     * @return array
     */
    public function getFreeAction()
    {
        return [
            'index', 'doc','sendSms'
        ];
    }
}