<?php

/**
 * Created by PhpStorm.
 * User: root
 * Date: 12.05.17
 * Time: 11:28
 */
class ChartsController extends ProjectServiceController
{
    public function visitedView()
    {
        $from = TimestampTZ::makeNow()->getDateTime()->modify('-7 day')->format('Y-m-d');
        $to = TimestampTZ::makeNow()->getDateTime()->format('Y-m-d');
        $model = new Model();
        $model
            ->set('fromDate', $from)
            ->set('toDate', $to);
        return (new ModelAndView())
            ->setModel($model)
            ->setView('charts/index');
    }

    public function getLinkAction(HttpRequest $httpRequest)
    {
        $form = new Form();
        $model = new Model();
        $data = [];
        $form
            ->set(Primitive::timestampTZ('f')->required())
            ->addMissingLabel('f', 'The field "f" is obligatory for filling!')
            ->addWrongLabel('f', 'The field "f" is not the correct value!');

        $form->import($httpRequest->getGet());

        if ($form->getErrors()) {
            foreach ($form->getPrimitiveNames() as $name) {
                if (!is_null($error = $form->getTextualErrorFor($name)))
                    $model->set($name, $error);
            }

            return (new ModelAndView())
                ->setModel($model->set('success', false))
                ->setView(new JsonView());
        }
        /** @var TimestampTZ $date */
        $date = $form->get('f')->getValue();
        $from = $date->getDateTime()->format('Y-m-d H:i:s');
        $to = $date->getDateTime()->modify('+1 day')->format('Y-m-d H:i:s');
        $visitSql = <<<SQL
SELECT 
	"bhvp"."page_url" AS "url",
	 count("bhvp"."page_url") AS "visit_count"
FROM 
	"bots"."history_visit_page" AS "bhvp" 
WHERE 
	"bhvp"."visited_at" > TIMESTAMP WITH TIME ZONE '$from' AND
	"bhvp"."visited_at" <  TIMESTAMP WITH TIME ZONE '$to'
GROUP BY "bhvp"."page_url"
ORDER BY "visit_count" DESC;
SQL;
        $link = DBPool::me()->getLink();
        $resourceVisit = $link->queryRaw($visitSql);

        while ($row = pg_fetch_assoc($resourceVisit)) {
            $data[] = ['url' => $row['url'], 'count' => (integer)$row['visit_count']];
        };

        return
            (new ModelAndView())
                ->setModel((new Model())->set('data', $data))
                ->setView(new JsonView());

    }

    public function getVisitedAction(HttpRequest $httpRequest)
    {
        $form = new Form();
        $model = new Model();

        $form
            ->set(Primitive::timestampTZ('f')->required())
            ->set(Primitive::timestampTZ('t')->required())
            ->addMissingLabel('f', 'The field "f" is obligatory for filling!')
            ->addWrongLabel('f', 'The field "f" is not the correct value!')
            ->addMissingLabel('t', 'The field "t" is obligatory for filling!')
            ->addWrongLabel('t', 'The field "t" is not the correct value!');

        $form->import($httpRequest->getGet());

        if ($form->getErrors()) {
            foreach ($form->getPrimitiveNames() as $name) {
                if (!is_null($error = $form->getTextualErrorFor($name)))
                    $model->set($name, $error);
            }

            return (new ModelAndView())
                ->setModel($model->set('success', false))
                ->setView(new JsonView());
        }
        $data = [];
        $data[] = ['Дата', 'Переходы'];
        /** @var TimestampTZ $fromDate */
        $fromDate = $form->get('f')->getValue();
        $f = $fromDate->toFormatString('Y-m-d H:i:s');
        /** @var TimestampTZ $toDate */
        $toDate = $form->get('t')->getValue();
        $t = $toDate->toFormatString('Y-m-d H:i:s');

        $visitSql = <<<SQL
SELECT 
	count(*),
	to_char("bhvp"."visited_at", 'YYYY-MM-DD') as "to_char",
	date_trunc('day', "bhvp"."visited_at") as "date"
FROM 
	"bots"."history_visit_page" AS "bhvp"
WHERE 
	"bhvp"."visited_at" > TIMESTAMP WITH TIME ZONE '$f' AND
	"bhvp"."visited_at" <  TIMESTAMP WITH TIME ZONE '$t'
GROUP BY 
	"date", "to_char"
ORDER BY 
	"date" ASC;
SQL;

        $link = DBPool::me()->getLink();
        $resourceVisit = $link->queryRaw($visitSql);

        while ($row = pg_fetch_assoc($resourceVisit)) {
            $data[] = [$row['to_char'], (integer)$row['count']];
        };

        return (new ModelAndView())
            ->setModel((new Model())->set('data', $data))
            ->setView(new JsonView());
    }

    /**
     * Маппинг
     *
     * @return array
     */
    protected function getMapping()
    {
        return [
            'visited' => 'visitedView',
            'getVisited' => 'getVisitedAction',
            'getLink' => 'getLinkAction'
        ];
    }

    /**
     * Доступные методы без авторизации
     *
     * @return array
     */
    public function getFreeAction()
    {
        return ['visited', 'getVisited', 'getLink'];
    }
}