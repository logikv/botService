/**
 * Created by root on 12.04.17.
 */
function Store (props) {
    let wrap = function (element) {
        let arg = [];
        if ('text' === element.type)
            return element.props;
        arg.push(element.type);
        if (undefined !== element['props'])
            arg.push(element.props);
        else
            arg.push({});

        if (undefined !== element['childs'])
            element['childs'].map(function (child) {
                arg.push(wrap(child));
            });
        if (undefined !== element['child'])
            arg.push(wrap(element['child']));
        return React.createElement(...arg);
    };
    return wrap(props)
}

const makeID = function () {
    let text = "";
    let possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
    for (let i = 0; i < 10; i++)
        text += possible.charAt(Math.floor(Math.random() * possible.length));
    return text;
};