/**
 * Created by root on 12.04.17.
 */

"use strict";

class ManagingTransitionController extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            dataGroupOrPage: dataGroupOrPage,
            referrersLinks: links,
            referrersLinksCount: linksCount,
            selectGroupOrPage: false,
            selectedSocialNetwork: 10,
            loadMore: false,
            controlTimeLinkId: false,
            formControlTime: SettingsTimes
        };
        this.increment = this.increment.bind(this);
        this.initDropDown = this.initDropDown.bind(this);
    }

    increment() {
        this.setState({
            referrersLinksCount: ++this.state.referrersLinksCount
        })
    }

    initDropDown() {
        Array.from(document.getElementsByClassName('dropdown-managing')).forEach(function (dropdown) {
            dropdown.setAttribute('data-toggle', 'dropdown');
            dropdown.setAttribute('aria-expanded', 'false');
        });
        $('.tips').tooltip();
    }

    componentDidMount() {
        this.initDropDown();

    }

    componentDidUpdate() {
        this.initDropDown();
        let that = this, options = {};
        if (this.state.loadMore)
            Services.execute('/managing/transition/loadData', options, function () {
                if (this.response.success === true) {
                    that.setState({
                        referrersLinksCount: parseInt(this.response.linksCount),
                        referrersLinks: this.response.links,
                        loadMore: false
                    })
                }
            })
    }

    loadMore(event) {
        let options = {}, that = this, i = event.target;
        if ('I' !== event.target.tagName)
            Array.from(event.target.childNodes).forEach(function (elements) {
                if ('I' === elements.tagName) i = elements
            });
        i.classList.add('fa-spin');
        this.btnLoadMore.setAttribute('disabled', 'true');
        options.visibleCount = this.state.referrersLinks.length;
        options.social_network_id = this.state.selectedSocialNetwork;
        options.group_page_id = this.state.selectGroupOrPage;
        Services.execute('/managing/transition/loadMore', options, function () {
            if (this.response.success === true) {
                let list = that.state.referrersLinks;
                that.btnLoadMore.removeAttribute('disabled');
                i.classList.remove('fa-spin');
                this.response.links.forEach(function (link) {
                    list.push(link)
                });
                that.setState({
                    referrersLinks: list,
                    referrersLinksCount: parseInt(this.response.linksCount),
                    loadMore: false
                });
            }
        })
    }

    loadData(event) {
        let options = {}, that = this, i, button = document.getElementsByClassName('sorting-social-icon');
        options.visibleCount = 0;
        options.social_network_id = this.state.selectedSocialNetwork;
        options.group_page_id = this.state.selectGroupOrPage;
        i = event.target;
        if (event.target.tagName === 'DIV')
            event.target.childNodes.forEach(function (el) {
                if (el.tagName === 'I') i = el
            });
        if (i.classList.contains('fa-search')) {
            i.classList.remove('fa-search');
            i.classList.add('fa-spinner');
            i.classList.add('fa-pulse');
        }
        Array.from(button).forEach(function (btnSocialIcon) {
            if (btnSocialIcon.classList.contains('sorting-pointer')) {
                btnSocialIcon.classList.remove('sorting-pointer');
                btnSocialIcon.classList.add('not-allowed');
            }
        });
        this.btnLoadData.setAttribute('disabled', 'true');
        this.selectGroupOrPage.setAttribute('disabled', 'true');
        Services.execute('/managing/transition/loadData', options, function () {
            if (this.response.success === true) {
                that.btnLoadData.removeAttribute('disabled');
                that.selectGroupOrPage.removeAttribute('disabled');
                Array.from(button).forEach(function (btnSocialIcon) {
                    if (btnSocialIcon.classList.contains('not-allowed')) {
                        btnSocialIcon.classList.remove('not-allowed');
                        btnSocialIcon.classList.add('sorting-pointer');
                    }
                });
                if (i.classList.contains('fa-spinner')) {
                    i.classList.remove('fa-spinner');
                    i.classList.remove('fa-pulse');
                    i.classList.add('fa-search');
                }
                that.setState({
                    referrersLinksCount: parseInt(this.response.linksCount),
                    referrersLinks: this.response.links
                })
            }
        })
    }

    dropLink(event) {
        event.preventDefault();
        let that = this, options = {}, links = that.state.referrersLinks;
        options.id = event.target.getAttribute('id');
        Services.execute('/managing/transition/dropLink', options, function () {
            if (this.response.success) {
                that.state.referrersLinks.forEach(function (item, key) {
                    if (parseInt(item.id) === parseInt(options.id))
                        delete links[key]
                });
                that.setState({referrersLinks: links})
            }
        })
    }

    changeGroupOrPage(event) {
        this.setState({
            selectGroupOrPage: (event.target.value === 'none') ? false : parseInt(event.target.value)
        })
    }

    selectSocialNetwork(event) {
        let list = document.querySelectorAll('.sorting-social-icon');
        list.forEach(function (element) {
            if (element.classList.contains('active'))
                element.classList.remove('active');
        });
        if (parseInt(event.target.getAttribute('value')) !== this.state.selectedSocialNetwork) {
            event.target.classList.add('active');
            this.setState({
                selectedSocialNetwork: parseInt(event.target.getAttribute('value')),
                loadMore: false,
                selectGroupOrPage: false
            });
        } else {
            this.setState({selectedSocialNetwork: 10, loadMore: true, selectGroupOrPage: false})
        }
    }

    handlerControlTimes(event) {
        event.preventDefault();
        this.setState({controlTimeLinkId: event.target.getAttribute('id'), formControlTime: SettingsTimes});
        this.controlTime.form.setState({addTime: false});
        $(this.controlTime.modal).modal('show');
    }

    render() {
        let links = [], options = [], that = this;

        if (this.state.referrersLinks.length === 0)
            links.push(
                {
                    type: 'div',
                    props: {
                        className: 'panel panel-default',
                    },
                    child: {
                        type: 'div',
                        props: {
                            className: 'panel-body'
                        },
                        child: {
                            type: 'p',
                            props: {
                                className: 'text-center'
                            },
                            child: {
                                type: 'text',
                                props: 'Нет ссылок в очереди!!!'
                            }
                        }
                    }
                }
            );

        this.state.referrersLinks.map(function (link) {
            let times = [], rowCunt = 0;
            Array.from(link.times).forEach(function (time) {
                let socialName, tooltip, statusClass;
                switch (parseInt(time.social_network_id)) {
                    case 0:
                        socialName = 'facebook.com';
                        break;
                    case 1:
                        socialName = 'vk.com';
                        break;
                    case 2:
                        socialName = 'ok.ru';
                }
                tooltip = 'Социальная сеть: ' + socialName + ' Группа\\Старница: ' + time.social_name;
                switch (parseInt(time.status)) {
                    case 1:
                        statusClass = 'label-success';
                        break;
                    case 2:
                        statusClass = 'label-warning';
                        break;
                    default:
                        statusClass = 'label-danger';
                        break;
                }

                times.push({
                    type: 'span',
                    props: {
                        className: 'label ' + statusClass + ' tips',
                        title: tooltip,
                        style: {
                            marginRight: '.3em'
                        },
                        key: makeID()
                    },
                    childs: [
                        {
                            type: 'i',
                            props: {
                                className: 'glyphicon glyphicon-time',
                                style: {
                                    marginRight: '.3em'
                                }

                            }
                        },
                        {
                            type: 'text',
                            props: time.begin_at
                        }
                    ]
                });
                if (rowCunt === 4) {
                    times.push({
                        type: 'hr',
                        props: {
                            style: {
                                marginBottom: '0px',
                                marginTop: '0px',
                                borderTop: '3px solid #fff'
                            }
                        }
                    });
                    rowCunt = 0;
                } else {
                    rowCunt = ++rowCunt
                }
            });
            links.push({
                type: 'div',
                props: {
                    className: 'panel panel-info',
                    style: {
                        boxShadow: '0px 0px 7px #BCE8F1'
                    }
                },
                child: {
                    type: 'div',
                    props: {
                        className: 'panel-body'
                    },
                    childs: [
                        {
                            type: 'div',
                            childs: [
                                {
                                    type: 'img',
                                    props: {
                                        src: link.img_src,
                                        className: 'img-thumbnail pull-left',
                                        style: {
                                            width: '9%',
                                            height: '9%',
                                            marginRight: '1em',
                                            padding: '2px'
                                        }
                                    }
                                },
                                {
                                    type: 'a',
                                    props: {
                                        href: link.url_link,
                                        target: '_blank'
                                    },
                                    child: {
                                        type: 'text',
                                        props: link.title
                                    }
                                },
                                {
                                    type: 'div',
                                    props: {
                                        className: 'btn-group pull-right'
                                    },
                                    childs: [
                                        {
                                            type: 'button',
                                            props: {
                                                className: 'btn bnt-primary btn-xs dropdown-toggle dropdown-managing',
                                                style: {
                                                    marginLeft: '10px'
                                                }
                                            },
                                            child: {
                                                type: 'i',
                                                props: {
                                                    className: 'fa fa-cog'
                                                }
                                            }
                                        },
                                        {
                                            type: 'ul',
                                            props: {
                                                className: 'dropdown-menu pull-right',
                                                role: 'menu'
                                            },
                                            childs: [
                                                {
                                                    type: 'li',
                                                    child: {
                                                        type: 'a',
                                                        props: {
                                                            href: '#',
                                                            id: link.id,
                                                            onClick: that.handlerControlTimes.bind(that)
                                                        },
                                                        childs: [
                                                            {
                                                                type: 'i',
                                                                props: {
                                                                    className: 'fa fa-clock-o',
                                                                    style: {
                                                                        marginRight: '.3em'
                                                                    }
                                                                }
                                                            },
                                                            {
                                                                type: 'text',
                                                                props: 'Управление временем'
                                                            }
                                                        ]
                                                    }
                                                },
                                                {
                                                    type: 'li',
                                                    props: {
                                                        className: 'divider'
                                                    }
                                                },
                                                {
                                                    type: 'li',
                                                    child: {
                                                        type: 'a',
                                                        props: {
                                                            href: '#',
                                                            id: link.id,
                                                            onClick: that.dropLink.bind(that)
                                                        },
                                                        childs: [
                                                            {
                                                                type: 'i',
                                                                props: {
                                                                    className: 'fa fa-trash-o text-danger',
                                                                    style: {
                                                                        marginRight: '.3em'
                                                                    }
                                                                }
                                                            },
                                                            {
                                                                type: 'text',
                                                                props: 'Удалить из очереди'
                                                            }
                                                        ]
                                                    }
                                                }
                                            ]
                                        }
                                    ]
                                }
                            ]
                        },
                        {
                            type: 'text',
                            props: link.description
                        },
                        {
                            type: 'hr',
                            props: {
                                className: 'separator',
                                style: {
                                    borderTop: '1px solid #BCE8F1',
                                    marginBottom: '5px'
                                }
                            }
                        },
                        {
                            type: 'div',
                            props: {
                                className: 'pull-left'
                            },
                            childs: times
                        }
                    ]
                }
            });
        });
        if (this.state.selectedSocialNetwork < 10) {
            this.state.dataGroupOrPage.forEach(function (socialNetworkData) {
                if (socialNetworkData.value === that.state.selectedSocialNetwork) {
                    options.push({
                        type: 'option',
                        props: {
                            value: 'none'
                        },
                        child: {
                            type: 'text',
                            props: "Не выбранна страница\\группа"
                        }
                    });
                    socialNetworkData.data.forEach(function (item) {
                        options.push({
                            type: 'option',
                            props: {
                                value: item.value
                            },
                            child: {
                                type: 'text',
                                props: item.name
                            }
                        })
                    })
                }
            });
        } else {
            options.push(
                {
                    type: 'option',
                    props: {
                        value: 'none'
                    },
                    child: {
                        type: 'text',
                        props: 'Выберете социальную сеть'
                    }
                }
            )
        }
        return React.createElement(
            Store,
            {
                type: 'div',
                childs: [
                    {
                        type: 'div',
                        props: {
                            className: 'row'
                        },
                        child: {
                            type: 'div',
                            props: {
                                className: 'col-lg-12'
                            },
                            child: {
                                type: 'h1',
                                props: {
                                    className: 'page-header'
                                },
                                child: {
                                    type: 'text',
                                    props: 'Настройка ссылок для ZENO'
                                }
                            }
                        }
                    },
                    {
                        type: 'div',
                        props: {
                            className: 'row'
                        },
                        child: {
                            type: Actions,
                            props: {
                                managing: this
                            }
                        }
                    },
                    {
                        type: 'div',
                        props: {
                            className: 'row',
                            style: {
                                paddingBottom: '10px'
                            }
                        },
                        childs: [
                            {
                                type: 'div',
                                props: {
                                    className: 'form-inline'
                                },
                                childs: [
                                    {
                                        type: 'div',
                                        props: {
                                            className: 'form-group'
                                        },
                                        child: {
                                            type: 'img',
                                            props: {
                                                value: 0,
                                                src: getBaseUrl() + 'images/social/fb.png',
                                                className: 'img-thumbnail sorting-social-icon sorting-pointer tips',
                                                title: 'www.facebook.com',
                                                onClick: this.selectSocialNetwork.bind(this)
                                            }
                                        }

                                    },
                                    {
                                        type: 'div',
                                        props: {
                                            className: 'form-group'
                                        },
                                        child: {
                                            type: 'img',
                                            props: {
                                                value: 1,
                                                src: getBaseUrl() + 'images/social/network.png',
                                                className: 'img-thumbnail sorting-social-icon sorting-pointer tips',
                                                title: 'vk.com',
                                                onClick: this.selectSocialNetwork.bind(this)
                                            }
                                        }
                                    },
                                    {
                                        type: 'div',
                                        props: {
                                            className: 'form-group'
                                        },
                                        child: {
                                            type: 'img',
                                            props: {
                                                value: 2,
                                                src: getBaseUrl() + 'images/social/odnoklassniki.png',
                                                className: 'img-thumbnail sorting-social-icon sorting-pointer tips',
                                                title: 'ok.ru',
                                                onClick: this.selectSocialNetwork.bind(this)
                                            }
                                        }
                                    },
                                    {
                                        type: 'div',
                                        props: {
                                            className: 'form-group',
                                            style: {
                                                marginLeft: '5px'
                                            }
                                        },
                                        child: {
                                            type: 'select',
                                            props: {
                                                disabled: this.state.selectedSocialNetwork === 10,
                                                className: 'form-control',
                                                style: {
                                                    width: '300px'
                                                },
                                                ref: (selectGroupOrPage) => {
                                                    this.selectGroupOrPage = selectGroupOrPage
                                                },
                                                onChange: this.changeGroupOrPage.bind(this)
                                            },
                                            childs: options
                                        },
                                    },
                                    {
                                        type: 'div',
                                        props: {
                                            className: 'btn btn-link',
                                            disabled: this.state.selectedSocialNetwork === 10,
                                            onClick: this.loadData.bind(this),
                                            ref: (btnLoadData) => {
                                                this.btnLoadData = btnLoadData
                                            },
                                            style: {
                                                marginLeft: '5px',
                                            }
                                        },
                                        childs: [
                                            {
                                                type: 'i',
                                                props: {
                                                    className: 'fa fa-search',
                                                    style: {
                                                        marginRight: '.3em'
                                                    }
                                                }
                                            },
                                            {
                                                type: 'text',
                                                props: 'Показать'
                                            }
                                        ]
                                    }
                                ]
                            },
                        ]
                    },
                    {
                        type: 'div',
                        props: {
                            className: 'row'
                        },
                        childs: links
                    },
                    (this.state.referrersLinksCount > this.state.referrersLinks.length) ?
                        {
                            type: 'p',
                            props: {
                                className: 'text-center'
                            },
                            child: {
                                type: 'button',
                                props: {
                                    className: 'btn bnt-default',
                                    ref: (btnLoadMore) => {
                                        this.btnLoadMore = btnLoadMore
                                    },
                                    onClick: this.loadMore.bind(this)
                                },
                                childs: [
                                    {
                                        type: 'i',
                                        props: {
                                            className: 'fa fa-refresh fa-fw',
                                            style: {
                                                marginRight: '.3em'
                                            }
                                        }
                                    },
                                    {
                                        type: 'text',
                                        props: 'Загрузить еще'
                                    }
                                ]
                            }
                        }
                        :
                        {
                            type: 'div',
                        }
                    ,
                    {
                        type: BaseModalWindow,
                        props: {
                            title: 'Добавить ссылку на Группу\\Страницу',
                            managing: this,
                            bodyForm: AddGroupOrPage,
                            buttonAction: ButtonSaveGroupOrPage,
                            ref: (addGroupOrPageWindow) => {
                                this.addGroupOrPageWindow = addGroupOrPageWindow
                            },
                        }
                    },
                    {
                        type: BaseModalWindow,
                        props: {
                            title: 'Добавить ссылку в очередь',
                            managing: this,
                            bodyForm: AddLinks,
                            buttonAction: ButtonAddLinks,
                            ref: (addLinks) => {
                                this.addLinks = addLinks
                            },
                        }
                    },
                    {
                        type: BaseModalWindow,
                        props: {
                            title: "Управление временем",
                            managing: this,
                            bodyForm: ControlTime,
                            buttonAction: ButtonControlTime,
                            ref: (controlTime) => {
                                this.controlTime = controlTime
                            }
                        }
                    }
                ]
            }
        )
    }
}

class ControlTime extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            addTime: false,
            allowSave: false
        };
    }

    createAddForm() {
        this.props.modal.props.managing.setState({
            formControlTime: AddLinks
        });
        this.setState({addTime: true})
    }

    backToSettingTime() {
        this.props.modal.props.managing.setState({
            formControlTime: SettingsTimes
        });
        this.setState({addTime: false})
    }

    addNewTime() {
        let
            options = {},
            that = this,
            links = that.props.modal.props.managing.state.referrersLinks;
        options.link_id = this.props.modal.props.managing.state.controlTimeLinkId;
        options.group_page_id = this.addTimesForm.state.fields.group_page_id;
        options.start_date = this.addTimesForm.state.fields.startDate + ' ' + this.addTimesForm.state.fields.startTime;
        Services.execute('/managing/transition/addTime', options, function () {
            let response = this.response;
            if (response.success === true) {
                links.map(function (link) {
                    if (parseInt(link.id) === parseInt(options.link_id)) {
                        link.times = response.times
                    }
                });
                that.props.modal.props.managing.setState({
                    referrersLinks: links
                });
                that.backToSettingTime();
            }
        });

    }

    render() {
        let times = [], that = this, linkTimes = [], url_link = undefined;
        this.props.modal.props.managing.state.referrersLinks.map(function (link) {
            if (link.id === that.props.modal.props.managing.state.controlTimeLinkId) {
                linkTimes = link.times;
                url_link = link.url_link
            }
        });
        linkTimes.map(function (time) {
            let props = time;
            props.link_id = that.props.modal.props.managing.state.controlTimeLinkId;
            props.controlTime = that;
            props.count = linkTimes.length;
            times.push({
                type: ControlLinkTime,
                props: time
            })
        });
        return React.createElement(Store, {
            type: 'div',
            childs: [
                {
                    type: this.props.modal.props.managing.state.formControlTime,
                    props: {
                        times: times,
                        modal: this.props.modal,
                        disabledLink: true,
                        link: url_link,
                        controlTime: this,
                        ref: (addTimesForm) => {
                            this.addTimesForm = addTimesForm
                        }
                    },
                },
                (!this.state.addTime) ?
                    {
                        type: 'div',
                        props: {
                            className: 'btn',
                            title: 'Добавить время для этой ссылки',
                            onClick: this.createAddForm.bind(this)
                        },
                        childs: [
                            {
                                type: 'i',
                                props: {
                                    className: 'fa fa-plus-square',
                                    style: {
                                        marginRight: '.3em'
                                    }
                                }
                            },
                            {
                                type: 'text',
                                props: 'Добавить'
                            }
                        ]
                    }
                    :
                    {
                        type: 'div',
                        childs: [
                            {
                                type: 'div',
                                props: {
                                    className: 'btn',
                                    onClick: this.backToSettingTime.bind(this),
                                    title: 'Назад к списку',
                                    style: {
                                        marginRight: '.3em'
                                    }
                                },
                                childs: [
                                    {
                                        type: 'i',
                                        props: {
                                            className: 'fa fa-chevron-left',
                                            style: {
                                                marginRight: '.3em'
                                            }
                                        }
                                    },
                                    {
                                        type: 'text',
                                        props: 'Назад'
                                    }
                                ]
                            },
                            {
                                type: 'div',
                                props: {
                                    className: 'btn',
                                    onClick: this.addNewTime.bind(this),
                                    title: 'Сохранить',
                                    disabled: !this.state.allowSave

                                },
                                childs: [
                                    {
                                        type: 'i',
                                        props: {
                                            className: 'fa fa-save',
                                            style: {
                                                marginRight: '.3em'
                                            }
                                        }
                                    },
                                    {
                                        type: 'text',
                                        props: 'Сохранить'
                                    }
                                ]
                            }
                        ]

                    }
            ],
        })
    }
}

class SettingsTimes extends React.Component {
    render() {
        return React.createElement(Store, {
            type: 'div',
            child: {
                type: 'form',
                props: {
                    role: 'form'
                },
                childs: this.props.times
            }
        })
    }
}

class ControlLinkTime extends React.Component {
    reject() {
        let
            links = this.props.controlTime.props.modal.props.managing.state.referrersLinks,
            that = this,
            options = {};
        options.id = this.props.id;
        this.rejectBtn.disabled = true;
        Services.execute('/managing/transition/reject', options, function () {
                if (this.response.success === true) {
                    links.map(function (link) {
                        if (parseInt(link.id) === parseInt(that.props.link_id)) {
                            link.times.map(function (time) {
                                if (parseInt(time.id) === parseInt(that.props.id))
                                    time.status = 4;
                            });
                        }
                    });
                    that.deleteBtn.disabled = false;
                    that.props.controlTime.props.modal.props.managing.setState({referrersLinks: links});
                }
            }
        );
    }

    remove() {
        let
            that = this,
            options = {},
            links = this.props.controlTime.props.modal.props.managing.state.referrersLinks;
        options.id = this.props.id;
        options.link_id = this.props.link_id;

        that.props.controlTime.props.modal.props.managing.setState({referrersLinks: links});
        Services.execute('/managing/transition/remove', options, function () {
            let response = this.response;
            if (response.success === true) {
                links.map(function (link) {
                    if (parseInt(link.id) === parseInt(that.props.link_id)) {
                        link.times = response.times
                    }
                });
                that.props.controlTime.props.modal.props.managing.setState({referrersLinks: links});
            }
        })

    }

    render() {
        let
            classStatus = '',
            socialIcon = [
                {
                    className: 'fa fa-facebook-official',
                    color: '#3b5998'
                },
                {
                    className: 'fa fa-vk',
                    color: '#507299'
                },
                {
                    className: 'fa fa fa-odnoklassniki-square',
                    color: '#ee8208'
                }
            ];
        switch (parseInt(this.props.status)) {
            case 1 :
                classStatus = 'has-success';
                break;
            case 4:
                classStatus = 'has-error';
                break;
            default:
                classStatus = 'has-warning';
        }
        return React.createElement(Store, {
            type: 'div',
            childs: [
                {
                    type: 'div',
                    props: {
                        className: 'form-group ' + classStatus + ' input-group',
                        style: {
                            marginBottom: '5px'
                        }
                    },
                    childs: [
                        {
                            type: 'input',
                            props: {
                                type: 'text',
                                className: 'form-control',
                                value: this.props.begin_at,
                                disabled: true,
                                style: {
                                    height: '28px'
                                }
                            }
                        },
                        {
                            type: 'span',
                            props: {
                                className: 'input-group-btn'
                            },
                            childs: [
                                {
                                    type: 'div',
                                    props: {
                                        className: 'btn btn-default tips',
                                        disabled: parseInt(this.props.status) !== 1,
                                        title: 'Остановить выполнение задания',
                                        ref: (rejectBtm) => {
                                            this.rejectBtn = rejectBtm
                                        },
                                        onClick: this.reject.bind(this)
                                    },
                                    child: {
                                        type: 'i',
                                        props: {
                                            className: 'fa fa-stop text-danger'
                                        }
                                    }
                                },
                                {
                                    type: 'div',
                                    props: {
                                        className: 'btn btn-default tips',
                                        title: 'Удаление текущего задания',
                                        disabled: 1 >= this.props.count,
                                        ref: (deleteBtn) => {
                                            this.deleteBtn = deleteBtn
                                        },
                                        onClick: this.remove.bind(this)
                                    },
                                    child: {
                                        type: 'i',
                                        props: {
                                            className: 'fa fa-trash-o'
                                        }
                                    }
                                }
                            ]
                        },
                    ]
                },
                {
                    type: 'p',
                    props: {
                        className: 'help-block'
                    },
                    childs: [
                        {
                            type: 'text',
                            props: 'Социальная сеть:'
                        },
                        {
                            type: 'i',
                            props: {
                                className: socialIcon[parseInt(this.props.social_network_id)].className,
                                style: {
                                    marginRight: '.3em',
                                    marginLeft: '.3em',
                                    color: socialIcon[parseInt(this.props.social_network_id)].color
                                }
                            }
                        },
                        {
                            type: 'text',
                            props: 'Группа\\Страница:'
                        },
                        {
                            type: 'a',
                            props: {
                                href: this.props.group_url,
                                target: '_blank',
                                style: {
                                    marginLeft: '.3em'
                                }
                            },
                            child: {
                                type: 'text',
                                props: this.props.social_name
                            }
                        }
                    ]
                }
            ]
        })
    }
}

class ButtonControlTime extends React.Component {
    constructor(props) {
        super(props);
    }

    handlerClose() {
        $(this.props.modal.modal).modal('hide');
    }

    render() {
        return React.createElement(Store, {
            type: 'div',
            childs: [
                {
                    type: 'div',
                    props: {
                        className: 'btn btn-default',
                        onClick: this.handlerClose.bind(this)
                    },
                    child: {
                        type: 'text',
                        props: 'Закрыть'
                    }
                }
            ]
        })
    }
}

class ButtonAddLinks extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            disabled: true,
        };
        this.isPushLink = this.isPushLink.bind(this);
    }

    isPushLink(id) {
        let
            links = this.props.modal.props.managing.state.referrersLinks,
            push = true;
        if (links.length === 0)
            return true;
        links.map(function (link) {
            if (parseInt(link.id) === parseInt(id))
                push = false;
        });
        return push;
    }

    addLink() {
        let options = {}, that = this;
        options.start_date = this.props.modal.form.state.fields.startDate + ' ' + this.props.modal.form.state.fields.startTime;
        options.social_network = this.props.modal.form.state.fields.social_network;
        options.link = this.props.modal.form.state.fields.link;
        options.group_page_id = this.props.modal.form.state.fields.group_page_id;
        options.id = this.props.modal.form.state.checkData.link_id;

        Services.execute('/managing/transition/addLink', options, function () {
            if (this.response.success === true) {
                let
                    response = this.response,
                    links = that.props.modal.props.managing.state.referrersLinks,
                    fields = that.props.modal.form.state.fields,
                    checkData = that.props.modal.form.state.checkData,
                    times = response.times;

                if (that.isPushLink(options.id)) {
                    that.props.modal.props.managing.increment();
                    if (that.props.modal.props.managing.state.referrersLinksCount > 10)
                        links.pop();
                    links.unshift({
                        id: response.id,
                        group_page_id: options.group_page_id,
                        social_network: options.social_network,
                        title: response.title,
                        description: response.description,
                        url_link: options.link,
                        img_src: this.response.img_src,
                        times: times
                    });
                } else {
                    links.map(function (link) {
                        if (parseInt(link.id) === parseInt(options.id))
                            link.times = times;
                    });
                }
                that.props.modal.props.managing.setState({
                    referrersLinks: links,
                    loadMore: false
                });
                fields.link = '';
                fields.startDate = '';
                fields.startTime = '';
                checkData.times = [];
                checkData.link_id = false;
                that.props.modal.form.setState({fields: fields, checkData: checkData});
                $(that.props.modal.modal).modal('hide');
            }
        })
    }

    render() {
        return React.createElement(Store, {
            type: 'div',
            props: {
                className: 'btn btn-primary',
                disabled: this.state.disabled,
                onClick: this.addLink.bind(this)
            },
            childs: [
                {
                    type: 'i',
                    props: {
                        className: 'fa fa-save',
                        style: {
                            marginRight: '.3em'
                        }
                    }
                },
                {
                    type: 'text',
                    props: 'Сохранить'
                }
            ]

        })
    }
}

class AddLinks extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            checkData: {
                times: [],
                link_id: false
            },
            fields: {
                social_network: 0,
                group_page_id: '',
                link: '',
                startDate: '',
                startTime: ''
            },
            errors: {
                social_network: false,
            },
            groupAndPageData: [],
            socialData: [
                {
                    text: 'facebook.com',
                    value: 0,
                    selected: false,
                    description: "www.facebook.com",
                    imageSrc: getBaseUrl() + "images/social/facebook.png"
                },
                {
                    text: "vk.com",
                    value: 1,
                    selected: false,
                    description: "vk.com",
                    imageSrc: getBaseUrl() + "images/social/vkontakte.png"
                },
                {
                    text: 'ok.ru',
                    value: 2,
                    selected: false,
                    description: "ok.ru",
                    imageSrc: getBaseUrl() + "images/social/odnoklassniki.png"
                }
            ],
            error: {
                type: 'div'
            }
        };
    }

    componentDidMount() {
        let
            that = this,
            groupAndPageData = this.props.modal.props.managing.state.dataGroupOrPage,
            date = new Date(),
            fields = this.state.fields;

        if (this.props.link !== undefined) {
            fields.link = this.props.link;
            this.setState({fields: fields});
        }

        $(this.startDate).datetimepicker({
            timepicker: false,
            format: 'Y-m-d',
            minDate: date.technomediaFormat1(),
            onSelectDate: function (current_time, input) {
                let fields = that.state.fields, options = {}, checkData = that.state.checkData;
                fields.startDate = input.val();
                that.setState({fields: fields});
                $(that.loader).show();
                this.hide();
                that.startDate.disabled = true;
                that.startTime.disabled = true;
                options.link = that.state.fields.link;
                options.date = input.val();
                Services.execute('/managing/transition/getFreeTime', options, function () {
                    that.startDate.disabled = false;
                    that.startTime.disabled = false;
                    checkData.link_id = (false === this.response.link_id) ? false : parseInt(this.response.link_id);
                    checkData.times = Array.from(this.response.times);
                    that.setState({checkData: checkData});
                    $(that.loader).hide();
                    $(that.startTime).datetimepicker({
                        format: 'H:00',
                        startDate: this.response.allowTimes[0],
                        formatTime: "H:i",
                        datepicker: false,
                        allowTimes: this.response.allowTimes,
                        onSelectTime: function (current_time, input) {
                            fields.startTime = input.val();
                            that.setState({fields: fields});
                            that.props.modal.props.managing.addLinks.action.setState({disabled: false});
                            if (that.props.controlTime !== undefined)
                                that.props.controlTime.setState({allowSave: true});
                        },
                        onChangeDateTime: function () {
                            $(this).hide()
                        }
                    });
                });
            }
        });
        $(this.loader).hide();
        $(this.socialSelect).ddslick({
            data: that.state.socialData,
            imagePosition: "left",
            selectText: "Выберите социальную сеть",
            defaultSelectedIndex: that.state.fields.social_network,
            onSelected: function (data) {
                let fields = that.state.fields;
                groupAndPageData.forEach(function (item) {
                    fields.group_page_id = '';
                    if (parseInt(data.selectedData.value) === parseInt(item.value))
                        that.setState({groupAndPageData: item.data});
                    that.link.disabled = true;
                });
                fields.social_network = data.selectedData.value;
                that.setState({fields: fields})
            }
        });
        groupAndPageData.forEach(function (item) {
            if (parseInt(item.value) === parseInt(that.state.fields.social_network))
                that.setState({groupAndPageData: item.data});
        });
        this.link.disabled = true;
    }

    changeLink(event) {
        let fields = this.state.fields;
        fields.link = event.target.value;
        this.setState({fields: fields})
    }

    changeGroupAndLink(event) {
        let fields = this.state.fields;
        fields.group_page_id = event.target.value;
        this.link.disabled = false;
        this.setState({fields: fields});
    }

    componentDidUpdate(prevProps, prevState) {
        $('.tips').tooltip();
    }

    render() {
        let options = [], times = [];
        if (this.state.groupAndPageData.length > 0) {
            options.push({
                type: 'option',
                props: {
                    value: 'none',
                    disabled: true
                },
                child: {
                    type: 'text',
                    props: 'Выберите группу'
                }
            });
            this.state.groupAndPageData.map(function (item) {
                options.push({
                    type: 'option',
                    props: {
                        value: item.value
                    },
                    child: {
                        type: 'text',
                        props: item.name
                    }
                })
            });
        }
        else {
            this.state.fields.group_page_id = '';
            options.push({
                type: 'option',
                props: {
                    value: 'none'
                },
                child: {
                    type: 'text',
                    props: 'Нет доступных групп'
                }
            });
        }
        if (this.state.checkData.times.length > 0) {
            let rowCunt = 1;
            this.state.checkData.times.map(function (time) {
                    let socialName, tooltip, statusClass;
                    switch (parseInt(time.social_network_id)) {
                        case 0:
                            socialName = 'facebook.com';
                            break;
                        case 1:
                            socialName = 'vk.com';
                            break;
                        case 2:
                            socialName = 'ok.ru';
                    }
                    tooltip = 'Социальная сеть: ' + socialName + ' Группа\\Старница: ' + time.social_name;
                    switch (parseInt(time.status)) {
                        case 1:
                            statusClass = 'label-success';
                            break;
                        case 2:
                            statusClass = 'label-warning';
                            break;
                        default:
                            statusClass = 'label-danger';
                            break;
                    }

                    times.push({
                        type: 'span',
                        props: {
                            className: 'label ' + statusClass + ' tips',
                            title: tooltip,
                            style: {
                                marginRight: '.3em'
                            },
                            key: makeID()
                        },
                        childs: [
                            {
                                type: 'i',
                                props: {
                                    className: 'glyphicon glyphicon-time',
                                    style: {
                                        marginRight: '.3em'
                                    }

                                }
                            },
                            {
                                type: 'text',
                                props: time.begin_at
                            }
                        ]
                    });
                    if (rowCunt === 4) {
                        times.push({
                            type: 'hr',
                            props: {
                                style: {
                                    marginBottom: '0px',
                                    marginTop: '0px',
                                    borderTop: '3px solid #fff'
                                }
                            }
                        });
                        rowCunt = 1;
                    } else {
                        rowCunt = ++rowCunt
                    }
                }
            );
            times.push({
                type: 'div',
                props: {
                    className: 'alert alert-info',
                    style: {
                        marginTop: '1em',
                        padding: '5px'
                    }
                },
                child: {
                    type: 'p',
                    props: {
                        className: 'text-center'
                    },
                    child: {
                        type: 'text',
                        props: 'Добавиться еще задача на новое время!!!'
                    }
                }
            });
        }
        return React.createElement(Store, {
            type: 'form',
            props: {
                role: 'form'
            },
            childs: [
                {
                    type: 'div',
                    props: {
                        className: 'form-group'
                    },
                    childs: [
                        {
                            type: 'label',
                            child: {
                                type: 'text',
                                props: 'Социальная сеть',
                            }
                        },
                        {
                            type: 'select',
                            props: {
                                ref: (socialSelect) => {
                                    this.socialSelect = socialSelect
                                },
                                className: 'form-control',
                            }
                        }
                    ]
                },
                {
                    type: 'div',
                    props: {
                        className: 'form-group'
                    },
                    childs: [
                        {
                            type: 'label',
                            child: {
                                type: 'text',
                                props: 'Группы'
                            }
                        },
                        {
                            type: 'select',
                            props: {
                                value: this.state.fields.group_page_id,
                                className: 'form-control',
                                disabled: this.state.groupAndPageData.length === 0,
                                onChange: this.changeGroupAndLink.bind(this)
                            },
                            childs: options
                        },
                        {
                            type: 'p',
                            props: {
                                className: 'help-block'
                            },
                            child: {
                                type: 'text',
                                props: 'Группа в которой будет опубликован материал'
                            }
                        }

                    ]
                },
                (this.props.link === undefined) ?
                    {
                        type: 'div',
                        props: {
                            className: 'form-group'
                        },
                        childs: [
                            {
                                type: 'label',
                                child: {
                                    type: 'text',
                                    props: 'Ссылка на материал'
                                }
                            },
                            {
                                type: 'input',
                                props: {
                                    onChange: this.changeLink.bind(this),
                                    value: this.state.fields.link,
                                    className: 'form-control',
                                    ref: (link) => {
                                        this.link = link
                                    }
                                }
                            },
                            {
                                type: 'p',
                                props: {
                                    className: 'help-block'
                                },
                                child: {
                                    type: 'text',
                                    props: 'На которую будут переходить боты из социальных сетей'
                                }
                            }
                        ]
                    }
                    :
                    {
                        type: 'div',
                        props: {
                            ref: (link) => {
                                this.link = link
                            }
                        }
                    }
                ,
                {
                    type: 'hr',
                    props: {
                        className: 'separator'
                    }
                },
                {
                    type: 'p',
                    props: {
                        className: 'text-center',
                        ref: (loader) => {
                            this.loader = loader
                        }
                    },
                    childs: [
                        {
                            type: 'i',
                            props: {
                                className: 'fa fa-spinner fa-pulse fa-fw',
                                style: {
                                    marginRight: '.3em'
                                }
                            }
                        },
                        {
                            type: 'text',
                            props: 'Проверка ссылки'
                        }
                    ]
                },
                {
                    type: 'div',
                    props: {
                        className: 'form-group'
                    },
                    childs: (this.state.checkData.times.length > 0) ? times : [{type: 'p'}]
                },
                {
                    type: 'div',
                    props: {
                        className: 'form-group'
                    },
                    childs: [
                        {
                            type: 'label',
                            child: {
                                type: 'text',
                                props: 'Дата'
                            }
                        },
                        {
                            type: 'input',
                            props: {
                                className: 'form-control',
                                value: this.state.fields.startDate,
                                disabled: this.state.fields.link === '' || this.state.fields.group_page_id === '',
                                ref: (startDate) => {
                                    this.startDate = startDate
                                }
                            }
                        },
                        {
                            type: 'p',
                            props: {
                                className: 'help-block'
                            },
                            child: {
                                type: 'text',
                                props: 'Дата когда нужно нужно пройти по ссылке'
                            }
                        }
                    ]
                },
                {
                    type: 'div',
                    props: {
                        className: 'form-group'
                    },
                    childs: [
                        {
                            type: 'label',
                            child: {
                                type: 'text',
                                props: 'Время'
                            }
                        },
                        {
                            type: 'input',
                            props: {
                                value: this.state.fields.startTime,
                                className: 'form-control',
                                disabled: this.state.fields.startDate === '',
                                ref: (startTime) => {
                                    this.startTime = startTime
                                }
                            }
                        },
                        {
                            type: 'p',
                            props: {
                                className: 'help-block'
                            },
                            child: {
                                type: 'text',
                                props: 'Время когда нужно начать проходить по ссылке'
                            }
                        }
                    ]
                }
            ]
        })
    }
}

class AddGroupOrPage extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            fields: {
                url: '',
                social_network: 0,
                name: ''
            },
            errors: {
                url: false,
                social_network: false,
                name: false
            },
            socialData: [
                {
                    text: 'facebook.com',
                    value: 0,
                    selected: false,
                    description: "Группа/Страница из facebook",
                    imageSrc: getBaseUrl() + "images/social/facebook.png"
                },
                {
                    text: "vk.com",
                    value: 1,
                    selected: false,
                    description: "Группа/Страница из vk.com",
                    imageSrc: getBaseUrl() + "images/social/vkontakte.png"
                },
                {
                    text: 'ok.ru',
                    value: 2,
                    selected: false,
                    description: "Группа в ok.ru",
                    imageSrc: getBaseUrl() + "images/social/odnoklassniki.png"
                }
            ],
            error: {
                type: 'div'
            }
        };
    }

    changeUrl(event) {
        let fields = this.state.fields, errors = this.state.errors;
        fields.url = event.target.value;
        errors.url = false;
        this.setState(
            {
                fields: fields,
                errors: errors
            }
        )
    }

    componentDidMount() {
        let that = this;
        $(this.socialSelect).ddslick({
            data: that.state.socialData,
            imagePosition: "left",
            selectText: "Выберите социальную сеть",
            defaultSelectedIndex: that.state.fields.social_network,
            onSelected: function (data) {
                let fields = that.state.fields;
                fields.social_network = data.selectedData.value;
                that.setState({fields: fields})
            }
        })
    }


    changeName(event) {
        let fields = this.state.fields, errors = this.state.errors;
        fields.name = event.target.value;
        errors.name = false;
        this.setState(
            {
                fields: fields,
                errors: errors
            }
        )
    }

    render() {
        return React.createElement(Store, {
            type: 'form',
            props: {
                role: 'form'
            },
            childs: [
                {
                    type: 'div',
                    props: {
                        className: 'form-group'
                    },
                    childs: [
                        {
                            type: 'label',
                            child: {
                                type: 'text',
                                props: 'Социальная сеть',
                            }
                        },
                        {
                            type: 'select',
                            props: {
                                ref: (socialSelect) => {
                                    this.socialSelect = socialSelect
                                },
                                className: 'form-control',
                            }
                        }
                    ]
                },
                {
                    type: 'div',
                    props: {
                        className: (this.state.errors.url) ? 'form-group has-error' : 'form-group'
                    },
                    childs: [
                        {
                            type: 'label',
                            child: {
                                type: 'text',
                                props: 'URL группы\\страницы'
                            }
                        },
                        {
                            type: 'input',
                            props: {
                                className: 'form-control',
                                value: this.state.fields.url,
                                onChange: this.changeUrl.bind(this)
                            }
                        },
                        (this.state.errors.url) ?
                            {
                                type: 'p',
                                props: {
                                    className: 'help-block'
                                },
                                child: {
                                    type: 'text',
                                    props: this.state.errors.url
                                }
                            }
                            :
                            {
                                type: 'div'
                            }

                    ]
                },
                {
                    type: 'div',
                    props: {
                        className: (this.state.errors.name) ? 'form-group has-error' : 'form-group'
                    },
                    childs: [
                        {
                            type: 'label',
                            child: {
                                type: 'text',
                                props: 'Название группы\\страницы'
                            }
                        },
                        {
                            type: 'input',
                            props: {
                                className: 'form-control',
                                value: this.state.fields.name,
                                onChange: this.changeName.bind(this)
                            }
                        },
                        (this.state.errors.name) ?
                            {
                                type: 'p',
                                props: {
                                    className: 'help-block'
                                },
                                child: {
                                    type: 'text',
                                    props: this.state.errors.name
                                }
                            }
                            :
                            {
                                type: 'div'
                            }
                    ]
                },
                this.state.error
            ]
        })
    }
}

class ButtonSaveGroupOrPage extends React.Component {
    save() {
        let that = this;
        Services.execute('/managing/transition/addGroupOrPage', that.props.modal.form.state.fields,
            function () {
                if (this.response.success) {
                    let
                        dataGroupOrPage = that.props.modal.props.managing.state.dataGroupOrPage,
                        data = [], id = this.response.id;

                    dataGroupOrPage.forEach(function (item) {
                        if (parseInt(item.value) === parseInt(that.props.modal.form.state.fields.social_network))
                            item.data.push({
                                value: id,
                                name: that.props.modal.form.state.fields.name
                            });
                        data.push(item)
                    });
                    that.props.modal.props.managing.setState({dataGroupOrPage: data});
                    $(that.props.modal.modal).modal('hide');
                    that.props.modal.form.setState({
                        fields: {url: '', social_network: that.props.modal.form.state.fields.social_network ,name: ''},
                        errors: {url: false, social_network: false, name: false},
                        error: {type: 'div'}
                    });
                } else {
                    switch (parseInt(this.response.code)) {
                        case 1005:
                            let errors = that.props.modal.form.state.errors;
                            for (let error in errors)
                                if (this.response.hasOwnProperty(error))
                                    errors[error] = this.response[error];
                            that.props.modal.form.setState({errors: errors});
                            break;
                        case 1001:
                        case 1002:
                        case 1003:
                        case 1004:
                            that.props.modal.form.setState({
                                error: {
                                    type: ErrorManaging,
                                    props: {
                                        code: this.response.code,
                                        form: that.props.modal.form
                                    }
                                }
                            });
                            break;
                    }
                }
            })
    }

    render() {
        return React.createElement(Store, {
            type: 'div',
            props: {
                className: 'btn btn-primary',
                onClick: this.save.bind(this)
            },
            childs: [
                {
                    type: 'i',
                    props: {
                        className: 'fa fa-save',
                        style: {
                            marginRight: '.3em'
                        }
                    }
                },
                {
                    type: 'text',
                    props: 'Сохранить'
                }
            ]

        })
    }
}

class ErrorManaging extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            1001: {
                error: 'Группа уже существует!!!'
            },
            1002: {
                error: 'ОШИБКА!!! Обратитесь к разработчикам'
            },
            1003: {
                error: 'ОШИБКА!!! Обратитесь к разработчикам.'
            },
            1004: {
                error: 'ОШИБКА!!! URL не соответствует выбранной социальной сети'
            }
        }
    }

    close() {
        this.props.form.setState({
            error: {
                type: 'div'
            }
        })
    }

    render() {
        return React.createElement(
            Store,
            {
                type: 'div',
                props: {
                    className: 'alert alert-danger alert-dismissable'
                },
                childs: [
                    {
                        type: 'button',
                        props: {
                            className: 'close',
                            onClick: this.close.bind(this)
                        },
                        child: {
                            type: 'text',
                            props: '×'
                        }
                    },
                    {
                        type: 'text',
                        props: this.state[this.props.code].error
                    }
                ]
            }
        )
    }
}

class BaseModalWindow extends React.Component {
    close() {
        $(this.modal).modal('hide');
    }

    render() {
        return React.createElement(Store, {
                type: 'div',
                props: {
                    ref: (modal) => {
                        this.modal = modal
                    },
                    className: 'modal fade',
                    role: 'dialog',
                    style: {
                        display: 'none'
                    }
                },
                child: {
                    type: 'div',
                    props: {
                        className: 'modal-dialog'
                    },
                    child: {
                        type: 'div',
                        props: {
                            className: 'modal-content'
                        },
                        childs: [
                            {
                                type: 'div',
                                props: {
                                    className: 'modal-header'
                                },
                                childs: [
                                    {
                                        type: 'button',
                                        props: {
                                            className: 'close',
                                            onClick: this.close.bind(this)
                                        },
                                        child: {
                                            type: 'text',
                                            props: '×'
                                        }
                                    },
                                    {
                                        type: 'h4',
                                        props: {
                                            className: 'modal-title'
                                        },
                                        child: {
                                            type: 'text',
                                            props: this.props.title
                                        }
                                    }
                                ]
                            },
                            {
                                type: 'div',
                                props: {
                                    className: 'modal-body'
                                },
                                child: {
                                    type: this.props.bodyForm,
                                    props: {
                                        ref: (form) => {
                                            this.form = form
                                        },
                                        modal: this
                                    }
                                }
                            },
                            {
                                type: 'div',
                                props: {
                                    className: 'modal-footer'
                                },
                                child: {
                                    type: this.props.buttonAction,
                                    props: {
                                        modal: this,
                                        ref: (action) => {
                                            this.action = action
                                        }
                                    }
                                }
                            }
                        ]

                    }
                }
            }
        )
    }
}

class Actions extends React.Component {
    openAddGroupOrPage() {
        $(this.props.managing.addGroupOrPageWindow.modal).modal('show');
    }

    openAddLinks() {
        $(this.props.managing.addLinks.modal).modal('show');
    }

    render() {
        return React.createElement(Store, {
            type: 'div',
            childs: [
                {
                    type: 'div',
                    props: {
                        className: 'form-group'
                    },
                    childs: [
                        {
                            type: 'button',
                            props: {
                                type: 'button',
                                className: 'btn btn-primary',
                                onClick: this.openAddGroupOrPage.bind(this)
                            },
                            child: {
                                type: 'text',
                                props: 'Добавить сылку на группу\\страницу'
                            }
                        },
                        {
                            type: 'button',
                            props: {
                                type: 'button',
                                className: 'btn btn-primary',
                                style: {
                                    marginLeft: '.3em'
                                },
                                onClick: this.openAddLinks.bind(this)
                            },
                            child: {
                                type: 'text',
                                props: 'Добавить ссылку материала'
                            }
                        }
                    ]
                }
            ]
        })
    }
}