const Services = {
    url : getBaseUrl(),
    token: null,
    secret: null,
    sessionID: null,
    json: null,
    init: function (connect) {
        this.url = connect.url;
        this.token = connect.token;
        this.secret = connect.secret;
        this.sessionID = connect.sessionID;
        return this;
    },
    execute: function (methodApi, options, complete, beforeSend) {
        this.json = null;
        let form = new FormData();
        let url = Services.url + methodApi + '?';
        if (typeof options == "object") {
            for (let variable in options) {
                if (options.hasOwnProperty(variable)) {
                    form.append(variable, options[variable]);
                }
            }
        }
        form.append('secret', Services.secret);
        form.append('token', Services.token);
        form.append('sessionID', Services.sessionID);
        let request = {
            type: 'post',
            url: url,
            data: form,
            dataType: "json",
            processData: false,
            contentType: false,
            success: function (json) {
                Services.json = json;
                this.response = json;
                this.options = options;
                return json;
            }
        };
        if (typeof beforeSend == 'function')
            request.beforeSend = beforeSend;
        if (typeof complete == 'function')
            request.complete = complete;
        $.ajax(request);
    }
};




