<?php

/**
 * Created by PhpStorm.
 * User: root
 * Date: 10.02.17
 * Time: 13:53
 */
final class PosixExpression extends StaticFactory
{
    const SENSITIVE = '~';
    const INSENSITIVE = '~*';
    const NOT_SENSITIVE = '!~';
    const NOT_INSENSITIVE = '!~*';

    public static function sensitive($left, $right)
    {
        return new BinaryExpression($left, $right, self::SENSITIVE);
    }

    public static function inSensitive($left, $right)
    {
        return new BinaryExpression($left, $right, self::INSENSITIVE);
    }

    public static function notSensitive($left, $right)
    {
        return new BinaryExpression($left, $right, self::NOT_SENSITIVE);
    }

    public static function notInSensitive($left, $right)
    {
        return new BinaryExpression($left, $right, self::NOT_INSENSITIVE);
    }
}